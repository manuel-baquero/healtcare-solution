public with sharing class VC_CreateRoomButton_Controller {
    @AuraEnabled
    public static CreateRoomButtonResponse createRoom(String recordId){
        System.debug('recordId: ' + recordId);
        try{
            Event evt = [SELECT Id, ActivityDateTime, EndDateTime, OwnerId
                         FROM Event
                         WHERE Id = :recordId];
                         System.debug('creo la sala');
            //Si la fecha de la cita es inferior a la fecha actual no dejo crear una sala ya que da error la sala.
            if(evt.ActivityDateTime < System.now()) {
                return new CreateRoomButtonResponse(false, Label.VideoCall_Error_Valid_Date);
            }

            System.debug('creo la sala');
            
            //Creo la sala.
            evt = VC_WS_Dailyco.createRoomOnDailyWS(evt);

            //Inserto logs Servicios.
            VC_WS_Utils.insertLogs();

            //Si da error devuelvo mensaje de error.
            if(evt == null) return new CreateRoomButtonResponse(false, Label.VideoCall_Error_Create_Room);
            
            //Si va bien actualizo y devuelvo mensaje.
            Database.update(evt);            
            return new CreateRoomButtonResponse(true, Label.VideoCall_Succes_Create_Room);
            
        }catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    //Wrapper class para guardar la respuesta y el mensaje de error de la creacion de salas desde el lwc.
    public class CreateRoomButtonResponse{
        @AuraEnabled public Boolean isSucced;
        @AuraEnabled public String errorMessage;

        public CreateRoomButtonResponse(Boolean isSucced, String errorMessage){
            this.isSucced = isSucced;
            this.errorMessage = errorMessage;
        }
    }
}