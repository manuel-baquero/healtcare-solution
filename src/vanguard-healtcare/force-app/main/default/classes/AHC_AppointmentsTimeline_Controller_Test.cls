@isTest
public class AHC_AppointmentsTimeline_Controller_Test {

    @isTest
    public static void AHC_AppointmentsTimelineTest() {
        UtilTestClass utils = new UtilTestClass();
		Account account = utils.createAccount();
        insert account;
        
        List<Event> eventList = new List<Event>();
        Event event1 = utils.creatEevent(account);
        event1.StartDateTime = Datetime.now().addDays(1).addMinutes(60);
        event1.EndDateTime = Datetime.now().addDays(1).addMinutes(90);
        event1.ActivityDate = Date.today()+1;
        eventList.add(event1);
        
        Event event2 = utils.creatEevent(account);
        event2.StartDateTime = Datetime.now().addDays(2).addMinutes(10);
        event2.EndDateTime = Datetime.now().addDays(2).addMinutes(40);
        event2.ActivityDate = Date.today()+2;
        eventList.add(event2);
        insert eventList;

        List<AHC_AppointmentsTimeline_Controller.ScheduledAppointment> appointmentsList = AHC_AppointmentsTimeline_Controller.getAppointmentsByDate(1, 0, 2);
        Integer totalRecords = AHC_AppointmentsTimeline_Controller.TotalRecords(event1.ActivityDate);
        Integer next = AHC_AppointmentsTimeline_Controller.getNext(0, 2);
        Integer previous = AHC_AppointmentsTimeline_Controller.getPrevious(0, 2);
    }
}