@isTest
global with sharing class VC_MockCallOutSuccess_Dailyco implements HttpCalloutMock{
    global Integer HTTP_STATUS_OK = 200;
    global Integer HTTP_STATUS_KO = 401;
    global String HTTP_VERB_POST = 'POST';
    global String HTTP_VERB_GET = 'GET';
    global String HEADER_CONTENT_TYPE = 'Content-Type';
    global String HEADER_CONTENT_TYPE_VALUE_JSON = 'application/json';


    global HTTPResponse respond(HTTPRequest req) {
        HttpResponse res = new HttpResponse();
        //Request for token
        // Create a fake response
        System.debug('REQUEST: ' + req.getBody());
        if(req.getEndpoint().equals('https://adesso.daily.co/api/v1/rooms/')){
            
            res.setHeader(HEADER_CONTENT_TYPE, HEADER_CONTENT_TYPE_VALUE_JSON);
            res.setStatusCode(Integer.valueOf(HTTP_STATUS_OK));
            res.setBody('{"id": "5104e83c-0ed4-4e73-b7e6-dcd69be938f2","name": "adessoDemoRoom","api_created": true,"privacy": "public","url": "https://adesso.daily.co/adessoDemoRoom","created_at": "2021-02-15T09:07:37.371Z","config": {"max_participants": 2,"start_video_off": true,"start_audio_off": true}}');
        } else {
            
            res.setHeader(HEADER_CONTENT_TYPE, HEADER_CONTENT_TYPE_VALUE_JSON);
            res.setStatusCode(Integer.valueOf(HTTP_STATUS_OK));
            res.setBody('{"id": "78497ad7-59f9-4c46-a7af-c367c5821b97","name": "sndx-00U3O000002vI9HUAU","api_created": true,"privacy": "public","url": "https://adesso.daily.co/sndx-00U3O000002vI9HUAU","created_at": "2021-02-17T10:26:00.000Z","config": {"nbf": 1613559600,"exp": 1613563200}}');
        
        }
            
        return res;
    }
}