public with sharing class VC_WS_Dailyco {

    public static final String INT_CONFIG_CREATE = 'Create_Room';
    public static final String INT_CONFIG_UPDATE = 'Update_Room';
    public static final String CUSTOM_NOTIFICATION_CREATE_ROOM = 'VC_Advice_Create_Room_Error';



    public static Event createRoomOnDailyWS(Event evt){

        //Cargo la configuracion para el WS.
        Integration_Configuration__mdt config= Integration_Configuration__mdt.getAll().get(INT_CONFIG_CREATE);
        
        //Relleno las wrapper classes con info que voy a enviar al WS.
        Boolean isSb = VC_WS_Utils.runningInASandbox();
        String prod = isSb ? 'sndx' : 'prod'; 
        Create_Room_Properties propRequest = new Create_Room_Properties(true, true, 2, (evt.ActivityDateTime.getTime()/1000), (evt.EndDateTime.getTime()/1000));
        Create_Room_Request createRoomRequest = new Create_Room_Request(prod + '-' + String.valueOf(evt.Id), 'public', propRequest);
        
        //Hago la llamada HTTP.
        HttpRequest req = VC_WS_Utils.getHttpRequestwithJSON(config, JSON.serialize(createRoomRequest), new Map<String,String>());
        Http http = new Http();
        HTTPResponse result = http.send(req);

        //Añado log.
        VC_WS_Utils.addLog(result, req, INT_CONFIG_CREATE, evt);
        System.debug(LoggingLevel.INFO, 'Resultado llamada ws: ' + result);

        //Si funciona bien parseo la respues ya actualizo el evento con la informacion.
        if(result.getStatusCode() == 200){
            System.debug('');
            Create_Room_Response crResponse = (Create_Room_Response)JSON.deserialize(result.getBody(), Create_Room_Response.Class);
            evt.EVT_ckb_Sala_creada__c = true;
            evt.EVT_txt_Url_Video_Call__c = crResponse.url;
            evt.EVT_dat_Old_EndDateTime__c = evt.EndDateTime;
            evt.EVT_dat_Old_ActivityDateTime__c = evt.ActivityDateTime;
            return evt;
        } else {
            System.debug('');
            //Error en la llamada
            CustomNotificationType notificationType = [SELECT Id, DeveloperName 
                                                       FROM CustomNotificationType 
                                                       WHERE DeveloperName = :CUSTOM_NOTIFICATION_CREATE_ROOM];
            
            //Mando la notificación
            String title = Label.VideoCall_Title_Custom_Notification_Create_Room;
            String body = Label.VideoCall_Body_Custom_Notification_Create_Room;
            VC_WS_Utils.createApexNotification(notificationType, evt.Id, new Set<String>{evt.OwnerId}, title, body);
            
            return null;
        }

        //Si no funciona bien de momento devuelvo esto
        // return null;
    }

    public static Update_Room_Response_Wrapper updateRoomOnDailyWS(Event evt){

        //Cargo la configuracion para el WS.
        Integration_Configuration__mdt config= Integration_Configuration__mdt.getAll().get(INT_CONFIG_UPDATE);

        //Añado el id de la sala a la url
        Boolean isSb = VC_WS_Utils.runningInASandbox();
        String prod = isSb ? 'sndx' : 'prod'; 
        
        //Creo el mapa para reemplazar todos parametros de la url
        Map<String,String> placeHolder = new Map<String,String>();
        placeHolder.put('##room_id##', prod + '-' + evt.Id);

        //Relleno las wrapper classes con info que voy a enviar al WS.
        Create_Room_Properties propRequest = new Create_Room_Properties((evt.ActivityDateTime.getTime()/1000), (evt.EndDateTime.getTime()/1000));
        Update_Room_Request updateRoomRequest = new Update_Room_Request(propRequest);
        
        //Hago la llamada HTTP.
        HttpRequest req = VC_WS_Utils.getHttpRequestwithJSON(config, JSON.serialize(updateRoomRequest), placeHolder);
        Http http = new Http();
        HTTPResponse result = http.send(req);

        //Añado log.
        VC_WS_Utils.addLog(result, req, INT_CONFIG_UPDATE, evt);
        System.debug(LoggingLevel.INFO, 'Resultado llamada ws: ' + result);

        //Si funciona bien parseo la respues ya actualizo el evento con la informacion.
        if(result.getStatusCode() == 200){

            Create_Room_Response crResponse = (Create_Room_Response)JSON.deserialize(result.getBody(), Create_Room_Response.Class);
            evt.EVT_ckb_Sala_creada__c = true;
            evt.EVT_txt_Url_Video_Call__c = crResponse.url;
            evt.EVT_dat_Old_EndDateTime__c = evt.EndDateTime;
            evt.EVT_dat_Old_ActivityDateTime__c = evt.ActivityDateTime;
            return new Update_Room_Response_Wrapper(false, evt);

        } else {

            CustomNotificationType notificationType = [SELECT Id, DeveloperName 
                                                       FROM CustomNotificationType 
                                                       WHERE DeveloperName = :CUSTOM_NOTIFICATION_CREATE_ROOM];

            //Mando la notificación
            String title = Label.VideoCall_Title_Custom_Notification_Update_Room;
            String body = Label.VideoCall_Body_Custom_Notification_Update_Room;
            VC_WS_Utils.createApexNotification(notificationType, evt.Id, new Set<String>{evt.OwnerId}, title, body);
            
            //Actualizo con la fecha antigua
            evt.EndDateTime = evt.EVT_dat_Old_EndDateTime__c;
            evt.ActivityDateTime = evt.EVT_dat_Old_ActivityDateTime__c;
            return new Update_Room_Response_Wrapper(true, evt);
        }

        //Si no funciona bien de momento devuelvo esto
        // return null;
    }

    //Wrapper con la respuesta para guardar para saber si hay que mandar email o no
    public class Update_Room_Response_Wrapper{
        public Boolean isError;
        public Event myEvent;

        public Update_Room_Response_Wrapper(Boolean isError, Event myEvent){
            this.isError = isError;
            this.myEvent = myEvent;
        }
    }

    //Wrapper class de la informacion de la sala a crear
    public class Create_Room_Request{
        public String name;
        public String privacy;
        public Create_Room_Properties properties;

        public Create_Room_Request(String name, String privacy, Create_Room_Properties properties){
            this.name = name;
            this.privacy = privacy;
            this.properties = properties;
        }

    }

    public class Update_Room_Request{
        public Create_Room_Properties properties;

        public Update_Room_Request(Create_Room_Properties properties){
            this.properties = properties;
        }

    }
    
    public class Create_Room_Properties{
        public Boolean start_audio_off;
        public Boolean start_video_off;
        public Long nbf;
        public Long exp;
        public Integer max_participants;
        public String lang;

        public Create_Room_Properties(Boolean start_audio_off, Boolean start_video_off, Integer max_participants, Long nbf, Long exp){
            this.start_video_off = start_video_off;
            this.start_audio_off = start_audio_off;
            this.max_participants = max_participants;
            this.nbf = nbf;
            this.exp = exp;
            this.lang = 'user';
        }

        public Create_Room_Properties(Long nbf, Long exp){
            this.nbf = nbf;
            this.exp = exp;
        }
    }

    //Wrapper class de la respuesta de la creacion de la sala
    public class Create_Room_Response {

        public String id;
        public String name;
        public Boolean api_created;
        public String privacy;
        public String url;
        public String created_at;
        public Config config;        
    }

    public class Config {
        public Integer max_participants;
        public Boolean start_video_off;
        public Boolean start_audio_off;
    }
}