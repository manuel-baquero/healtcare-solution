@isTest
global with sharing class VC_MockCallOutError_Dailyco implements HttpCalloutMock{
    global Integer HTTP_STATUS_OK = 200;
    global Integer HTTP_STATUS_KO = 401;
    global String HTTP_VERB_POST = 'POST';
    global String HTTP_VERB_GET = 'GET';
    global String HEADER_CONTENT_TYPE = 'Content-Type';
    global String HEADER_CONTENT_TYPE_VALUE_JSON = 'application/json';


    global HTTPResponse respond(HTTPRequest req) {
        HttpResponse res = new HttpResponse();
        //Request for token
        // Create a fake response
        System.debug('REQUEST: ' + req.getBody());
        if(req.getEndpoint().equals('https://adesso.daily.co/api/v1/rooms/')){
            
            res.setHeader(HEADER_CONTENT_TYPE, HEADER_CONTENT_TYPE_VALUE_JSON);
            res.setStatusCode(Integer.valueOf(HTTP_STATUS_KO));
            res.setBody('');
        } 
            
        return res;
    }
}