public with sharing class VC_VideoCall_Controller {
    public Id recordId {get;set;}    
    public String url {get;set;}   
    public String activityDateTimeString {get;set;} 
    public DateTime endDateTime {get;set;} 
    public DateTime activityDateTime {get;set;} 

    public Boolean isNow{get{return (DateTime.valueOf(this.activityDateTime) < System.now());}set;}
    public Boolean isOver{get{return (DateTime.valueOf(this.EndDateTime) < System.now());}set;}

        



    public VC_VideoCall_Controller(){

        //Saco el id y los campos que necesito del evento
        this.recordId  = ApexPages.currentPage().getParameters().get('id');
        Event evt = [SELECT ActivityDateTime, EndDateTime, EVT_txt_Url_Video_Call__c FROM Event where id = :this.recordId];
        this.url = evt.EVT_txt_Url_Video_Call__c;
        this.activityDateTime = evt.ActivityDateTime;
        this.endDateTime = evt.EndDateTime;

        //Formateo el mensaje a mostrar con la fecha de la cita
        this.activityDateTimeString = 'el ' + evt.ActivityDateTime.format('dd/MM/YYYY') + 
                                      ' a las ' + evt.ActivityDateTime.format('HH:mm');
    }
}