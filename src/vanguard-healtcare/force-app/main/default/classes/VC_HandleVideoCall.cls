public with sharing class VC_HandleVideoCall {
    public static final string RT_CITA_ONLINE = 'OnlineEvent';
    public static final string TEMPLATE_EDIT_CITA = 'vc_edit_cita_online';
    public static final string TEMPLATE_NEW_CITA = 'vc_nueva_cita_online';


    public static void createRoom(Map<Id, Event> newMap){
        Set<Id> eventSet = new Set<Id>();
        Id rtCitaOnlineId = Schema.SObjectType.Event.getRecordTypeInfosByDeveloperName().get(RT_CITA_ONLINE).getRecordTypeId();

        //Recorro todos los eventos y recojo los que son de tipo cita para crear las salas en daily co
        for (Event evt : newMap.values()) {
            if(evt.recordTypeId.equals(rtCitaOnlineId)) {eventSet.add(evt.Id);}
        }

        //Si hay citas creo las salas en daily co
        if(eventSet.size() > 0) {createRoomOnDaily(eventSet);}
        
    }


    public static void updateRoom(Map<Id, Event> newMap, Map<Id, Event> oldMap){
        Set<Id> eventSet = new Set<Id>();
        Id rtCitaOnlineId = Schema.SObjectType.Event.getRecordTypeInfosByDeveloperName().get(RT_CITA_ONLINE).getRecordTypeId();

        //Recorro todos los eventos y recojo los que son de tipo cita y 
        //se ha modificado su horario para actualizar la hora de las salas y tiene la sala creada
        for (Event evt : newMap.values()) {
            if( evt.EVT_ckb_Sala_creada__c == true &&
                evt.recordTypeId.equals(rtCitaOnlineId) && 
                    (evt.ActivityDateTime != oldMap.get(evt.Id).ActivityDateTime || 
                    evt.EndDateTime != oldMap.get(evt.Id).EndDateTime)) {
                eventSet.add(evt.Id);
            }
        }
        
        //Si hay citas actualizo las salas en daily co
        if(eventSet.size() > 0) {updateRoomOnDaily(eventSet);}
    }



    
    @future(callout=true)
    private static void updateRoomOnDaily(Set<Id> eventSet){
        List<Event> evntList = new List<Event>();
        Map<Id,Event> EvntMapSuccess = new Map<Id,Event>();
        Map<Id,Event> EvntMapError = new Map<Id,Event>();


        Map<Id,Event> eventMap = new Map<Id,Event>([
            SELECT  Id, ActivityDateTime, EndDateTime, OwnerId, EVT_dat_Old_EndDateTime__c, EVT_dat_Old_ActivityDateTime__c, EVT_txt_Url_Video_Call__c,
                    typeof What when Account then PersonEmail, PersonContactId, IsPersonAccount end,
                    typeof Who when Lead then Email end
            FROM Event
            WHERE Id IN :eventSet]);
        
        //Recorro los eventos, actualizo el horario en daily co y la url en sf
        for (Event evt : eventMap.values()) {
            VC_WS_Dailyco.Update_Room_Response_Wrapper uprw = VC_WS_Dailyco.updateRoomOnDailyWS(evt);
            if(uprw.isError) EvntMapError.put(evt.Id, evt);
            else EvntMapSuccess.put(evt.Id, evt);
        }

        //Actualizo los eventos que han fallado
        if(EvntMapError.values() != null && EvntMapError.values().size() > 0) {
            VC_TR_Event_Handler.runTrigger = false;
            Database.SaveResult[] srList = Database.update(EvntMapError.values());
            VC_TR_Event_Handler.runTrigger = true;
        }


        //Actualizo los eventos que han ido bien y envío emails de actualizado de fecha de cita
        if(EvntMapSuccess.values() != null && EvntMapSuccess.values().size() > 0) {
            VC_TR_Event_Handler.runTrigger = false;
            Database.SaveResult[] srList = Database.update(EvntMapSuccess.values());
            VC_TR_Event_Handler.runTrigger = true;

             EmailTemplate et = [
                 SELECT Id, Subject, Body, HtmlValue 
                 FROM EmailTemplate 
                 WHERE DeveloperName = :TEMPLATE_EDIT_CITA
             ];

             for (Database.SaveResult sr : srList) {
                 if(sr.isSuccess()){
                     String email;
                    Id ctc;
                     if(eventMap.get(sr.getId()).who != null){
                          Lead ld = eventMap.get(sr.getId()).who;
                          ctc = ld.Id;
                          email = ld.email;
                      }
                     if(eventMap.get(sr.getId()).what != null){
                         Account acc = eventMap.get(sr.getId()).what;
                         ctc = acc.PersonContactId;
                         email = acc.PersonEmail;
                     }
                    //  et.HtmlValue = et.HtmlValue.replace('{!Event.StartDateTime}', eventMap.get(sr.getId()).ActivityDateTime.format('dd/MM/yyyy HH:mm'));
                    //  et.HtmlValue = et.HtmlValue.replace('{!Event.EVT_txt_Url_Video_Call__c}', '<a href=" ' + eventMap.get(sr.getId()).EVT_txt_Url_Video_Call__c + ' ">pulse aqui</a>');
                    VC_WS_Utils.sendEmail(email, et, ctc, sr.getId());
                 }
             }
        }

        //inserto logs generados en los servicios
        VC_WS_Utils.insertLogs();
    }

    @future(callout=true)
    private static void createRoomOnDaily(Set<Id> eventSet){
        List<Event> evntList = new List<Event>();
        Map<Id,Event> newEvntMap = new Map<Id,Event>();

        Map<Id,Event> eventMap = new Map<Id,Event>([
            SELECT  Id, ActivityDateTime, EndDateTime, OwnerId, EVT_dat_Old_EndDateTime__c, EVT_dat_Old_ActivityDateTime__c,
                    typeof What when Account then PersonEmail, PersonContactId end,
                    typeof Who when Lead then Email end
            FROM Event
            WHERE Id IN :eventSet]);
     
        //Recorro los eventos, creo las salas en daily co y actualizo la url en sf
        for (Event evt : eventMap.values()) {
            evt = VC_WS_Dailyco.createRoomOnDailyWS(evt);
            if(evt != null){
                newEvntMap.put(evt.Id, evt);
            } 
            // evntList.add(evt);
            
        }
        system.debug('newEvntMap: ' + newEvntMap);
        //Si hay eventos para actualizar actualizo los eventos forzando
        if(newEvntMap.values() != null && newEvntMap.values().size() > 0) {
            VC_TR_Event_Handler.runTrigger = false;
            Database.SaveResult[] srList = Database.update(newEvntMap.values());
            system.debug('srList: ' + srList);
            VC_TR_Event_Handler.runTrigger = true;

             EmailTemplate et = [
                 SELECT Id, Subject, Body, HtmlValue 
                 FROM EmailTemplate 
                 WHERE DeveloperName = :TEMPLATE_NEW_CITA
             ];  
            
            //envio el email al correo del account o el lead de la cita
             for (Database.SaveResult sr : srList) {
                 if(sr.isSuccess()){
                      String email;
                     Id ctc;
                      if(eventMap.get(sr.getId()).who != null){
                          Lead ld = eventMap.get(sr.getId()).who;
                          ctc = ld.Id;
                          email = ld.email;
                      }
					String fechaHoraFormateada;
                     if(eventMap.get(sr.getId()).what != null){
                         Account acc = eventMap.get(sr.getId()).what;
                         ctc = acc.PersonContactId;
                         email = acc.PersonEmail;
                         //DateTime dt = eventMap.get(sr.getId()).StartDateTime;
						 //fechaHoraFormateada = dt.format('dd-MM-yyyy hh:mm:ss');
                     }
                     System.debug('et.HtmlValue: '+ et.HtmlValue);
                      //et.HtmlValue = et.HtmlValue.replace('Appointment details','LAJSLDJSD');
                    //  et.HtmlValue = et.HtmlValue.replace('{!relatedTo.EVT_txt_Url_Video_Call__c}', '<a href=" ' + eventMap.get(sr.getId()).EVT_txt_Url_Video_Call__c + ' ">pulse aqui</a>');
System.debug('et.HtmlValue: '+ et.HtmlValue);
                    VC_WS_Utils.sendEmail(email, et, ctc, sr.getId());
                 }
            }
        }

        //inserto logs generados en los servicios
        VC_WS_Utils.insertLogs();
    }
}