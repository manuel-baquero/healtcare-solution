public class AutoconvertLeadHandler {

    @future
    public static void changeLeadOwner(String jsonString){
        Map<Id, String> leadOwnerMap = (Map<Id, String>)Json.deserialize(jsonString,Map<Id, String>.class);
        List<Lead> leadsToUpdate = new List<Lead>();
        for(Id idAux : leadOwnerMap.keySet()) {
            Lead lead = new Lead(
                Id = idAux,
                OwnerId = leadOwnerMap.get(idAux)
            );
            leadsToUpdate.add(lead);
        }
        update leadsToUpdate;
    }
    
    /*@future
    public static void undeleteEvents(String jsonStringEvent){
        List<Event> eventList = (List<Event>)Json.deserialize(jsonStringEvent,List<Event>.class);
        undelete eventList;
    }*/
}