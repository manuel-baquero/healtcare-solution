/**
 * All methods defined for appointmentsTimeline LWC
 *
 * @author		Manuel Baquero <manuel.baquero@adesso.es>
 */
public with sharing class AHC_AppointmentsTimeline_Controller {
    /**
     * This method gets appointments based in filters input
     *
     * @param		<selectedDate> - Date from which to make the query
     * @param		<appointmentType> - Appointment Type parameter (online or onsite)
     * @param		<numberOfRecords> - Number of records to query
     *
     * @return		List of events in wrapper class
     */

    @AuraEnabled(cacheable = false)
	public static List<AHC_AppointmentsTimeline_Controller.ScheduledAppointment> getAppointmentsByDate(Integer selectedDate, Integer v_Offset, Integer v_pagesize){

        Date selectedDt = Date.today().addDays(selectedDate);

        List<Event> appointmentList = [SELECT Id, Subject, WhatId, What.Name, WhoId, Who.Name, StartDateTime, RecordTypeId, RecordType.Name, ALS_EventLocation__c, ALS_EventStatus__c,
                                        ALS_MainAgenda__c, ALS_MainAgenda__r.Name, ALS_RelatedPayment__c, ALS_RelatedPayment__r.Status__c, ActivityDate, ActivityDateTime
                                       FROM Event
                                       WHERE ActivityDate =: selectedDt AND ActivityDateTime >=: Datetime.now()
                                       ORDER BY ActivityDateTime ASC
                                       LIMIT :v_pagesize OFFSET :v_Offset];

        List<ScheduledAppointment> appList = new List<ScheduledAppointment>();
        Datetime dt = Datetime.now();
        String pendingTime;
        Integer pendingTotalTime;
        Integer pendingHoursTime;
        for(Event evt:appointmentList){
            ScheduledAppointment app = new ScheduledAppointment();
            app.appointment = evt;
            app.appointmentLink = '/lightning/r/Event/' + evt.Id + '/view';
            app.patientLink = '/lightning/r/Account/' + evt.WhatId + '/view';
            app.doctorLink = '/lightning/r/ALS_Agenda__c/' + evt.ALS_MainAgenda__c + '/view';
            pendingTotalTime = Integer.valueOf((evt.StartDateTime.getTime() - dt.getTime()))/1000/60;
            if(pendingTotalTime > 60){
                pendingHoursTime = pendingTotalTime / 60;
                pendingTotalTime = pendingTotalTime - pendingHoursTime * 60;
                app.pendingTime = pendingHoursTime + ' h ' + pendingTotalTime + ' min';
            }else{
                app.pendingTime = String.valueOf(pendingTotalTime) + ' min';
            }
            if(evt.ALS_RelatedPayment__c != null){
                if(evt.ALS_RelatedPayment__r.Status__c == '01'){
                    app.paidAppointment = false;
                }else{
                    app.paidAppointment = true;
                }
            }else{
                app.paidAppointment = false;
            }
            app.virtualAppointment = false;
            appList.add(app);
        }

        return appList;
    }

    @AuraEnabled(cacheable=true)
    public static Integer TotalRecords(Date selectedDate){
        return [SELECT count() 
                FROM Event
                WHERE ActivityDate =:selectedDate AND ActivityDateTime >=: Datetime.now()];
    }

    @AuraEnabled(cacheable=true)
    public static Integer getNext(Integer v_Offset, Integer v_pagesize){
        v_Offset += v_pagesize;
        return v_Offset;
    }

    @AuraEnabled(cacheable=true)
    public static Integer getPrevious(Integer v_Offset, Integer v_pagesize){
        v_Offset -= v_pagesize;
        return v_Offset;
    }

    public class ScheduledAppointment{
        @AuraEnabled
        public Event appointment{get;set;}
        @AuraEnabled
        public Boolean paidAppointment{get;set;}
        @AuraEnabled
        public String appointmentLink{get;set;}
        @AuraEnabled
        public String patientLink{get;set;}
        @AuraEnabled
        public String doctorLink{get;set;}
        @AuraEnabled
        public Boolean virtualAppointment{get;set;}
        @AuraEnabled
        public String pendingTime{get;set;}
    }
}