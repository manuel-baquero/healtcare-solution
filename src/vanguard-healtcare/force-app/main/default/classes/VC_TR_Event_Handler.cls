public without sharing class VC_TR_Event_Handler {
  public static Boolean runTrigger = true;

  public static void handleTrigger(
    System.TriggerOperation operationType,
    Map<Id, Event> newMap,
    Map<Id, Event> oldMap
  ) {
    if (runTrigger) {
      switch on (operationType) {
        when AFTER_INSERT {
          VC_HandleVideoCall.createRoom(newMap);
        }
        when AFTER_Update {
          // VC_EventService.changeEventOnPast(newMap, oldMap);
          VC_HandleVideoCall.updateRoom(newMap, oldMap);
          // VC_EventService.updateAssociateEvents(newMap);
        }
        when BEFORE_DELETE {
          // VC_EventService.removeAssociateEvents(oldMap);
        }
      }
    }
  }
}