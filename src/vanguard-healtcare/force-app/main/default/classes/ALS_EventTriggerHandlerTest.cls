/**
 * In this class I have all the methods for testing ALS_EventTriggerHandler class
 *
 * @author		Jesús Martínez  <jesus.martinez@adesso.es>
 */
@isTest
public with sharing class ALS_EventTriggerHandlerTest {
    
    @TestSetup
    static void makeData() {
        Event ev1 = new Event(StartDateTime = DateTime.now(), DurationInMinutes = 60, Subject='Test');
        insert ev1;
        Event ev2 = new Event(StartDateTime = DateTime.now(), DurationInMinutes = 60, Subject='Test', ALS_ParentId__c = ev1.id);
        insert ev2;
    }

    @isTest
    static void beforeUpdate_Test() {
        Event ev = [Select id, Subject FROM Event Where ALS_ParentId__c = null Limit 1];
        ev.Subject = 'Mod Test';
        update ev;

        System.assertEquals(ev.Subject, [Select Subject FROM Event Where ALS_ParentId__c =: ev.id].Subject);
    }

    @isTest
    static void beforeDelete_Test() {
        Event ev = [Select id FROM Event Where ALS_ParentId__c = null Limit 1];
        delete ev;

        List<Event> eventList = new List<Event>();
        eventList = [Select id FROM Event Where ALS_ParentId__c =: ev.id];

        System.assertEquals(eventList.size(), 0);
    }
}