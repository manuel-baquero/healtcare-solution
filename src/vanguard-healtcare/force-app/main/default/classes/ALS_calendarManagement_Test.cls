/**
 * In this class I have all the methods for testing ALS_calendarManagement class
 *
 * @author		Jesús Martínez  <jesus.martinez@adesso.es>
 */
@isTest
public with sharing class ALS_calendarManagement_Test {

    @TestSetup
    static void makeData() {
        Id rtPatient = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName()
            .get('Patient').getRecordTypeId();
        insert new Account(
            FirstName = 'Test',
            LastName = 'Account',
            RecordTypeId = rtPatient,
            PersonEmail = 'email@test.com'
        );
        ALS_Agenda__c agenda = new ALS_Agenda__c(Name = 'Test');
        insert agenda;
        ALS_AgendaEntry__c agendaEntry = new ALS_AgendaEntry__c(StartDate__c = Date.Today(), EndDate__c = Date.Today() + 30, ActivityStart__c='08:00',
            ActivityEnd__c = '18:00', Days__c = '1', Agenda__c = agenda.id);
        insert agendaEntry;

        Event ev1 = new Event(StartDateTime = DateTime.now(), DurationInMinutes = 60, Subject='Test', ALS_MainAgenda__c = agenda.id);
        insert ev1;
        Event ev2 = new Event(StartDateTime = DateTime.now(), DurationInMinutes = 60, Subject='Test', ALS_ParentId__c = ev1.id);
        insert ev2;

        ALS_CalendarConfiguration__c calendar = new ALS_CalendarConfiguration__c(Type__C='Grid', EndHour__c = '20:00', SlotDuration__c= '00:15:00', StartHour__c ='08:00',  NumberLegendItems__c = '2');      
        insert calendar;
        ALS_CalendarLegend__c calendarLegend = new ALS_CalendarLegend__c(Label__c = 'Prueba', Color__c='#8cc082', Calendar__c = calendar.id);
        insert calendarLegend;

        insert new ALS_CalendarCondition__c(Legend__c = calendarLegend.id, Value__C= 'Email', Field_ApiName__c='Type', Operation__C='Equal');

        String pickListRT = Schema.SObjectType.ALS_CalendarField__c.getRecordTypeInfosByDeveloperName().get('Picklist').getRecordTypeId();
        String textRT = Schema.SObjectType.ALS_CalendarField__c.getRecordTypeInfosByDeveloperName().get('Text').getRecordTypeId();
        ALS_CalendarField__c calendarField = new ALS_CalendarField__c(RecordTypeId = pickListRT, ApiName__c = 'Name', FieldName__c='Name', Label__c = 'Test', Calendar__c = calendar.id, Order__c = 1, Size__c = 12);
        ALS_CalendarField__c textField = new ALS_CalendarField__c(RecordTypeId = textRT, ApiName__c = 'Text', FieldName__c='Name', Label__c = 'Test', Calendar__c = calendar.id, Order__c = 2, Size__c = 12);
        insert new List<ALS_CalendarField__c>{calendarField, textField};

        ALS_CalendarOptions__c calendarOption = new ALS_CalendarOptions__c(Field__c = calendarField.id, Label__c = 'Test', Value__c = 'Test', Order__c = 1);
        ALS_CalendarOptions__c calendarOption2 = new ALS_CalendarOptions__c(Field__c = calendarField.id, Label__c = 'Test2', Value__c = 'Test2', Order__c = 2);
        insert new List<ALS_CalendarOptions__c>{calendarOption, calendarOption2};

    }

    @isTest
    static void getAppointmentByDate_Test() { 
        List<ALS_calendarManagement_ctrl.SelectableWrapper> lsw = new List<ALS_calendarManagement_ctrl.SelectableWrapper>();
        ALS_Agenda__c agenda = [Select id FROM ALS_Agenda__c Limit 1];
        Event ev = [Select id, ActivityDate FROM Event Where ALS_MainAgenda__c =: agenda.id Limit 1];
        List<Event> eventList = ALS_calendarManagement_ctrl.getAppointmentByDate(Date.today(), new List<String>{agenda.id}, 'timeGridWeek', lsw);
        system.assertEquals(ev.Id, eventList[0].id);

        eventList = ALS_calendarManagement_ctrl.getAppointmentByDate(Date.today(), new List<String>{agenda.id}, 'dayGridMonth', lsw);
        eventList = ALS_calendarManagement_ctrl.getAppointmentByDate(Date.today(), null, 'dayGridMonth', lsw);
        system.assertEquals(2, eventList.size());
    }

    @isTest
    static void getMainAgenda_Test() {
        ALS_Agenda__c agenda = [Select id, Name FROM ALS_Agenda__c Limit 1];
        List<ALS_calendarManagement_ctrl.AgendaData> result = ALS_calendarManagement_ctrl.getMainAgenda('Name = \'Test\'');
        system.assertEquals(agenda.id, result[0].Id);
        system.assertEquals(agenda.Name, result[0].Name);
        system.assertEquals(1, result[0].businessHour.size());
    }

    @isTest
    static void getOtherAgendas_Test() {
        ALS_Agenda__c agenda = [Select id, Name FROM ALS_Agenda__c Limit 1];
        List<ALS_calendarManagement_ctrl.AgendaData> result = ALS_calendarManagement_ctrl.getOtherAgendas(null);
        system.assertEquals(agenda.id, result[0].Id);
        system.assertEquals(agenda.Name, result[0].Name);
        system.assertEquals(1, result[0].businessHour.size());
    }   

    @isTest
    static void getCalendarConfiguration_Test() {
        ALS_CalendarConfiguration__c calendar = [Select id, Name FROM ALS_CalendarConfiguration__c Limit 1];
        ALS_calendarManagement_ctrl.CalendarConfiguration result = ALS_calendarManagement_ctrl.getCalendarConfiguration(calendar.Name);
        System.debug(result);
        system.assertEquals(calendar.Id, result.Id);
        system.assertEquals(2, result.Fields.size());
        system.assertEquals(1, result.Legends.size());
        system.assertEquals(2, result.Fields[0].Options.size());
    }

    @isTest
    static void createEvent_Test() {
        ALS_Agenda__c mainAgenda = [Select id FROM ALS_Agenda__c Limit 1];
        TEST.startTest();
        // Id rtId = Schema.SObjectType.Event.getRecordTypeInfosByDeveloperName().get('Event').getRecordTypeId();
        Id accId = [SELECT Id FROM Account LIMIT 1].Id;
        Id result = ALS_calendarManagement_ctrl.createEvent('Subject', 'Description', null, mainAgenda.id, accId, '2021-01-01', new List<String>{mainAgenda.id}, '08:00', false, 60,  null);
        system.assertEquals(result, [Select id FROM Event WHERE ALS_ParentId__c = null Order by CreatedDate DESC Limit 1].id);
        Test.stopTest();
    }

    @isTest
    static void getEventRecordType_Test() {
        ALS_calendarManagement_ctrl.getEventRecordType();
    }

    @isTest
    static void createCalendarLegend_Test() {
        ALS_CalendarConfiguration__c calendar = [Select id FROM ALS_CalendarConfiguration__c Limit 1];
        ALS_calendarManagement_ctrl.createCalendarLegend('label', '#FFFFFF', calendar.id);
    }    

    @isTest
    static void isAvailableField_Test() {
        ALS_calendarManagement_ctrl.isAvailableField('Event', 'Subject');
    }

    @isTest
    static void updateCita_Test(){
        Event e = [Select id FROM Event Limit 1];
        ALS_Agenda__c mainAgenda = [Select id FROM ALS_Agenda__c Limit 1];
        ALS_calendarManagement_ctrl.updateCita(e.id, '2021-01-01 08:00:00:00', '2021-01-01 08:30:00:00', null, mainAgenda.id, false);
    }

    @isTest
    static void updateCita_Clone_Test(){
        Event e = [Select id FROM Event Limit 1];
        ALS_Agenda__c mainAgenda = [Select id FROM ALS_Agenda__c Limit 1];
        ALS_calendarManagement_ctrl.updateCita(e.id, '2021-01-01 08:00:00:00', '2021-01-01 08:30:00:00', null, mainAgenda.id, true);
    }
}