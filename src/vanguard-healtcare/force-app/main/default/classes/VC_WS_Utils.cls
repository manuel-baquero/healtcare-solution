public with sharing class VC_WS_Utils {
    public static List <VC_WS_Log__c> LOG_LIST = new List <VC_WS_Log__c>();

    public static HttpRequest getHttpRequestwithJSON(Integration_Configuration__mdt config, String requestBody, Map<String,String> placeHolderMap){
        //Relleno toda la url con placeholders
        String endpoint = config.End_Point__c;
        for (String str : placeHolderMap.keySet()) {
            endpoint = endpoint.replace(str, placeHolderMap.get(str));
        }

        //Creo la http request
        HttpRequest req = new HttpRequest();
        req.setEndpoint(endpoint);
        req.setMethod(config.Method__c);
        if(String.isNotBlank(requestBody)) req.setBody(requestBody);
        if(String.isNotBlank(config.Token__c) && config.Bearer_Auth__c) req.setHeader('Authorization', 'Bearer ' + config.Token__c);
        req.setHeader('Content-Type', 'application/json');
        req.setHeader('Accept', 'application/json');
        return req;
    }

    public static void addLog(HTTPResponse result, HttpRequest req, String name, SObject obj){
        String sObjType = String.valueOf(obj.Id.getSObjectType());
        Boolean hasName = obj.getSobjectType().getDescribe().fields.getMap().keySet().contains('Name'.toLowerCase());

        VC_WS_Log__c wsLog = new VC_WS_Log__c();
        wsLog.WSL_txt_Record_Id__c = String.valueOf(obj.get('Id'));
        wsLog.WSL_txt_Record_Name__c = hasName ? String.valueOf(obj.get('Name')) : '';
        wsLog.WSL_txt_Object_Type__c = sObjType;
        wsLog.Name = name;
        wsLog.WSL_txt_Body_Request__c = req.getBody();
        wsLog.WSL_txt_Body_Response__c = result.getBody();
        wsLog.WSL_txt_Response_Code__c = result.getStatusCode();
        wsLog.WSL_txt_Method__c = req.getMethod();
        wsLog.WSL_txt_End_Point__c = req.getEndpoint();
        for(String key: result.getHeaderKeys()){
            wsLog.WSL_txt_Headers__c += result.getHeader(key) + '\n';
        }

        LOG_LIST.add(wsLog);
    }

    public static void insertLogs(){
        if(LOG_LIST != null && LOG_LIST.size() > 0) Database.insert(LOG_LIST);
    }

    public static Boolean runningInASandbox() {
        boolean resultado = false;
        resultado = [SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox;
        return resultado;
    }

    public static void createApexNotification(CustomNotificationType notificationType, String targetId, Set<String> setUsers, String title, String body){
        
        // Create a new custom notification
        Messaging.CustomNotification notification = new Messaging.CustomNotification();

        // Set the contents for the notification
        notification.setTitle(title);
        notification.setBody(body);

        // Set the notification type and target
        notification.setNotificationTypeId(notificationType.Id);
        notification.setTargetId(targetId);

        // Actually send the notification
        try {
            notification.send(setUsers);
        }
        catch (Exception e) {
            System.debug('Problem sending notification: ' + e.getMessage());
        }
    }

    public static void sendEmail(String email, EmailTemplate et, Id ctc, Id sobj){
        Messaging.reserveSingleEmailCapacity(2);

        Messaging.SingleEmailMessage mail = Messaging.renderStoredEmailTemplate(et.Id, ctc, sobj);
        
        mail.setBccSender(false);
        mail.setUseSignature(false);  
        mail.setSaveAsActivity(false);      
        SYSTEM.debug('ctc: ' + ctc);
        if(String.isNotBlank(ctc)){
            mail.setSenderDisplayName('adesso Salud'); 
        } else {
            String[] toAddresses = new String[] {email}; 
            List<OrgWideEmailAddress> owea = [select id, Address, DisplayName from OrgWideEmailAddress];
            mail.setOrgWideEmailAddressId(owea[0].Id);
            mail.setToAddresses(toAddresses);
            mail.setSubject(et.Subject);
            mail.setHtmlBody(et.HtmlValue);
        }
        // Send the email you have created.
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
}