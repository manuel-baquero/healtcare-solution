/**
 * In this class I have all the configuration for Event trigger
 *
 * @author		Jesús Martínez  <jesus.martinez@adesso.es>
 */
public with sharing class ALS_EventTriggerHandler {

    /**
     * This method have the code for Before Delete flows.
     *
     * @param		<eventList>  List of events
     *
     */
    public static void beforeDelete(List<Event> eventList){
        List<String> listIds = new List<String>();
        for(Event event : eventList){
            listIds.add(event.id);
        }
        List<Event> deleteList = new List<Event>();
        for(Event event :[Select id From Event Where ALS_ParentId__c in :listIds]){
            deleteList.add(event);
        }

        if(!deleteList.isEmpty()) delete deleteList;
    }

    /**
     * This method have the code for After Update flows.
     *
     * @param		<eventList>  List of events
     * @param		<oldMap>  Trigger old map event
     *
     */
    public static void afterUpdate(List<Event> eventList, Map<Id, Event> oldMap){
        editAsociatedEvents(eventList, oldMap);
    }

    /**
     * This method checks if an event has been modified and if it has an associated event. If it does, it modifies the event as well.
     *
     * @param		<eventList>  List of events
     * @param		<oldMap>  Trigger old map event
     *
     */
    public static void editAsociatedEvents(List<Event> eventList, Map<Id, Event> oldMap){
        Map<String, Map<String,Object>> mapChanges = new Map<String, Map<String,Object>>(); 
        Set<String> eventSetIds = new Set<String>();
        
        //Fields changed
        String queryFields = '';
        Set<String> setChangedFields = new Set<String>();
        for(Event event : eventList){ 
            //Skip changes from associated Events
            if(String.isBlank(event.ALS_ParentId__c)){
                //Initialized the map
                if(mapChanges.get(event.id) == null) mapChanges.put(event.id, new Map<String, Object>());

                Schema.SObjectType objectType = event.getSObjectType();
                for (String field : objectType.getDescribe().fields.getMap().keySet()){
                    //Field changed
                    if(field.tolowerCase() != 'ownerid' && field.toLowerCase() != 'lastmodifieddate' &&  field.toLowerCase() != 'systemmodstamp' &&  
                    field != 'ALS_MainAgenda__c' && field.toLowerCase() != 'groupeventtype' && field.toLowerCase() != 'hora_de_la_cita__c' && event.get(field) != oldMap.get(event.id).get(field)){
                        //System.debug('Field > ' + field + ' -- New Event > ' + event.get(field) + ' -- Old Event > ' + oldMap.get(event.id).get(field));
                        if(!setChangedFields.contains(field)) {
                            setChangedFields.add(field);
                        	queryFields += field + ',';
                        }
                        Map<String, Object> aux = mapChanges.get(event.id); 
                        aux.put(field, event.get(field));
                        mapChanges.put(event.id, aux);
                    }
                }
                if(!mapChanges.get(event.id).isEmpty()) eventSetIds.add(event.id);
            }            
        }

        if(!eventSetIds.isEmpty()){
            queryFields = queryFields.substring(0, queryFields.length()-1);
            String whereSentence = '(';
            for(String id : eventSetIds){
                whereSentence += '\'' + id + '\',';
            }
            whereSentence = whereSentence.substring(0, whereSentence.length()-1) + ')';
            //System.debug('queryFields > ' + queryFields);
            //System.debug('whereSentence > ' + whereSentence);
            List<Event> updateList = new List<Event>();
            for(Event event : database.query('SELECT ALS_ParentId__c, ' + queryFields + ' FROM Event WHERE ALS_ParentId__c in ' + whereSentence)){
                sObject sEvent = new Event(Id = event.id);
                for(String field : mapChanges.get(event.ALS_ParentId__c).keySet()){
                    //System.debug('Field > ' + field + ' -- Associated Event value > ' + event.get(field) + ' -- new Value > ' + mapChanges.get(event.ALS_ParentId__c).get(field));
                    sEvent.put(field, mapChanges.get(event.ALS_ParentId__c).get(field));
                }
                updateList.add((Event) sEvent);
            }

            if(!updateList.isEmpty()) update updateList;
        }      
    }
}