@IsTest
public with sharing class VC_VideoCall_Test {
    @TestSetup
    static void initData(){
        Test.setMock(HttpCalloutMock.class, new VC_MockCallOutSuccess_Dailyco());
        Id rtCitaOnlineId = Schema.SObjectType.Event.getRecordTypeInfosByDeveloperName().get(VC_HandleVideoCall.RT_CITA_ONLINE).getRecordTypeId();
        Id rtPersonAccountId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Patient').getRecordTypeId();
        
        Account acc = new Account();
        acc.recordTypeId = rtPersonAccountId;
        acc.FirstName = 'test1';
        acc.LastName = 'test2';
        acc.PersonEmail = 'test@test.com';
        insert acc;
        Event evt = new Event();
        evt.whatId = acc.Id;
        evt.ActivityDateTime = System.now().addMinutes(30);
        evt.EndDateTime = System.now().addMinutes(60);
        evt.Subject = 'Actividad grupal';
        evt.RecordTypeId = rtCitaOnlineId;
        // evt.EVT_dat_Old_ActivityDateTime__c = System.now().addMinutes(30);
        // evt.EVT_dat_Old_EndDateTime__c = System.now().addMinutes(60);
        insert evt;
    }

    @isTest
    public static void createRoomSuccess(){
        Id rtCitaOnlineId = Schema.SObjectType.Event.getRecordTypeInfosByDeveloperName().get(VC_HandleVideoCall.RT_CITA_ONLINE).getRecordTypeId();
        Test.setMock(HttpCalloutMock.class, new VC_MockCallOutSuccess_Dailyco());
        
        Test.startTest();
        Account acc = new Account();
        acc.FirstName = 'test1';
        acc.LastName = 'test2';
        acc.PersonEmail = 'test@test.com';
        insert acc;
        Event evt = new Event();
        evt.WhatId = acc.Id;
        evt.ActivityDateTime = System.now().addMinutes(30);
        evt.EndDateTime = System.now().addMinutes(60);
        evt.Subject = 'Actividad grupal';
        evt.RecordTypeId = rtCitaOnlineId;
        // evt.EVT_dat_Old_ActivityDateTime__c = System.now().addMinutes(30);
        // evt.EVT_dat_Old_EndDateTime__c = System.now().addMinutes(60);
        insert evt;
        Test.stopTest();
    }

    @isTest
    public static void createRoomError(){
        Id rtCitaOnlineId = Schema.SObjectType.Event.getRecordTypeInfosByDeveloperName().get(VC_HandleVideoCall.RT_CITA_ONLINE).getRecordTypeId();
        Test.setMock(HttpCalloutMock.class, new VC_MockCallOutError_Dailyco());
        
        Test.startTest();
        Account acc = new Account();
        acc.FirstName = 'test1';
        acc.LastName = 'test2';
        acc.PersonEmail = 'test@test.com';
        insert acc;
        Event evt = new Event();
        evt.WhatId = acc.Id;
        evt.ActivityDateTime = System.now().addMinutes(30);
        evt.EndDateTime = System.now().addMinutes(60);
        evt.Subject = 'Actividad grupal';
        evt.RecordTypeId = rtCitaOnlineId;
        // evt.EVT_dat_Old_ActivityDateTime__c = System.now().addMinutes(30);
        // evt.EVT_dat_Old_EndDateTime__c = System.now().addMinutes(60);
        insert evt;
        Test.stopTest();
    }

    @isTest
    public static void updateRoom(){
        Id rtCitaOnlineId = Schema.SObjectType.Event.getRecordTypeInfosByDeveloperName().get(VC_HandleVideoCall.RT_CITA_ONLINE).getRecordTypeId();
        Test.setMock(HttpCalloutMock.class, new VC_MockCallOutSuccess_Dailyco());
        Event evt = [SELECT Id FROM Event LIMIT 1];

        Test.startTest();
        evt.ActivityDateTime = System.now().addMinutes(60);
        evt.EndDateTime = System.now().addMinutes(90);
        update evt;
        Test.stopTest();
    }

    @isTest
    public static void updateRoomError(){
        Id rtCitaOnlineId = Schema.SObjectType.Event.getRecordTypeInfosByDeveloperName().get(VC_HandleVideoCall.RT_CITA_ONLINE).getRecordTypeId();
        Test.setMock(HttpCalloutMock.class, new VC_MockCallOutError_Dailyco());
        Event evt = [SELECT Id FROM Event LIMIT 1];

        Test.startTest();
        evt.ActivityDateTime = System.now().addMinutes(60);
        evt.EndDateTime = System.now().addMinutes(90);
        update evt;
        Test.stopTest();
    }

    @isTest
    public static void createRoomFromButton(){
        
        Test.setMock(HttpCalloutMock.class, new VC_MockCallOutSuccess_Dailyco());
        Event evt = [SELECT Id FROM Event LIMIT 1];

        Test.startTest();
        VC_CreateRoomButton_Controller.createRoom(evt.Id);
        Test.stopTest();
    }

    @isTest
    public static void vfController(){
        
        Test.setMock(HttpCalloutMock.class, new VC_MockCallOutSuccess_Dailyco());
        Event evt = [SELECT Id FROM Event LIMIT 1];

        // Add parameters to page URL
        ApexPages.currentPage().getParameters().put('id', evt.Id);
        
        Test.startTest();
        VC_VideoCall_Controller evc = new VC_VideoCall_Controller();
        Test.stopTest();
    }
}