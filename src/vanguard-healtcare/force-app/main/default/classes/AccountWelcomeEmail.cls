public class AccountWelcomeEmail {

    @InvocableMethod(label='Account welcome email')
    public static void sendEmails(List<Account> accountList) {
		System.debug('accountList: ' + accountList);
		Id templateId = [SELECT Id, DeveloperName FROM EmailTemplate WHERE DeveloperName = 'WelcomeEmail' LIMIT 1].Id;
        Map<String,String> parameterMap = new Map<String,String>();
        for (Account ld: accountList) {
            parameterMap.put(ld.PersonContactId, templateId);
        }
        System.debug('parameterMap: ' + parameterMap);
        futureSendEmails(parameterMap);
    }

    @future(callout=true)
    private static void futureSendEmails(Map<String,String> accountMap) {
        List<Messaging.SingleEmailMessage> messageList = new List<Messaging.SingleEmailMessage>();
        System.debug('accountMap-futureSendEmails: ' + accountMap);
        for (String accountId: accountMap.keySet()) {
            if (accountId != null && accountMap.get(accountId) != null) {
                Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                email.setTargetObjectId(accountId);
                email.setTemplateId(accountMap.get(accountId));
                messageList.add(email);
            }
        }
        System.debug('messageList: ' + messageList);
        if (!messageList.isEmpty()) {
            Messaging.SendEmailResult[] serList = Messaging.sendEmail(messageList, false);
            manageErrors(serList);
        }
    }
    
    private static void manageErrors(List<Messaging.SendEmailResult> serList) {
        for (Messaging.SendEmailResult ser: serList) {
            if (!ser.isSuccess()) {
                System.debug('Fallo en el envío de emails');
                for (Messaging.SendEmailError see: ser.getErrors()) {
                    System.debug(see.getMessage());
                }
            }
        }
    }
}