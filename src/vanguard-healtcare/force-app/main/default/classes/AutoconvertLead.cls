public without sharing class AutoconvertLead {

    @InvocableMethod
    public static void LeadAssign(List<Id> leadIds) {
        Map<Id, String> leadTypeMap = new Map<Id, String>();
        Map<Id, String> leadOwnerMap = new Map<Id, String>();
        Map<Id, String> leadNameMap = new Map<Id, String>();
        System.debug('leadIds: ' + leadIds);
        Id pacienteRTid = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Patient').getRecordTypeId();
        //Id medicalReportRTid = Schema.SObjectType.MedicalReport__c.getRecordTypeInfosByDeveloperName().get('AccountMedicalReport').getRecordTypeId();
        User defaultOwnerUser = [select id, username, email from user where email = 'manuel.baquero@adesso.es' LIMIT 1];
        List<Lead> leadList = new List<Lead>();
        for(Lead leadAux : [SELECT Id, Name, OwnerId, Owner.Type FROM Lead WHERE Id IN : leadIds]) {
            if(leadAux.Owner.Type == 'User') {
                leadOwnerMap.put(leadAux.Id, leadAux.OwnerId);
            } else {
                leadOwnerMap.put(leadAux.Id, defaultOwnerUser.Id);
            }
            leadNameMap.put(leadAux.Id, leadAux.Name);
        }
        for(Id idAux : leadIds) {
            Id newOwnerId;
            if(idAux.getSObjectType() == Schema.User.SObjectType) {
                newOwnerId = leadOwnerMap.get(idAux);
            }
            Lead leadAux = new Lead(
                OwnerId = UserInfo.getUserId(),
            	Id = idAux,
                OwnerBeforeConversion__c = newOwnerId
            );
            leadList.add(leadAux);
        }
        update leadList;
        System.debug('pacienteRTid: ' + pacienteRTid);
        //System.debug('medicalReportRTid: ' + medicalReportRTid);
        
        System.debug('leadTypeMap: ' + leadTypeMap);
        System.debug('leadOwnerMap: ' + leadOwnerMap);
        
        LeadStatus CLeadStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true Limit 1];
        List<Database.LeadConvert> MassLeadconvert = new List<Database.LeadConvert>();
        for(id currentlead : LeadIds) {
            Database.LeadConvert Leadconvert = new Database.LeadConvert();
            Leadconvert.setLeadId(currentlead);
            Leadconvert.setConvertedStatus(CLeadStatus.MasterLabel);
            leadConvert.setOwnerId(leadOwnerMap.get(currentlead));
            leadConvert.setDoNotCreateOpportunity(true);
            MassLeadconvert.add(LeadConvert);
        }
        System.debug('MassLeadconvert: ' + MassLeadconvert);
        if(!MassLeadconvert.isEmpty()) {
            List<Opportunity> oppsToUpdate = new List<Opportunity>();
            List<Account> accsToUpdate = new List<Account>();
            List<MedicalReport__c> medicalReportList = [SELECT Id, Lead__c, Patient__c FROM MedicalReport__c WHERE Lead__c IN :leadIds];
			List<Payment__c> paymentList = [SELECT Id, RelatedLead__c, RelatedAccount__c FROM Payment__c WHERE RelatedLead__c IN :leadIds];
            List<Event> eventList = [SELECT Id, WhoId, WhatId FROM Event WHERE WhoId IN :leadIds];
            System.debug('eventList: ' + eventList);
            Map<Id, Id> leadAccIdMap = new Map<Id, Id>();
            List<Database.LeadConvertResult> lcr = Database.convertLead(MassLeadconvert);
            for(Database.LeadConvertResult regConversion : lcr){
                if(regConversion.isSuccess()){
                    // copio el owner del lead en el asesor del paciente
                    Account accountAux = new Account(Id = regConversion.getAccountId(), 
                                                     OwnerId = leadOwnerMap.get(regConversion.getLeadId()),
                                                     RecordTypeId = pacienteRTid);
                    accsToUpdate.add(accountAux);
                    // mapa leadId, accountId
                    leadAccIdMap.put(regConversion.getLeadId(), accountAux.Id);
                    
                }
            }
            if(!accsToUpdate.isEmpty()) {
                System.debug('accsToUpdate: ' + accsToUpdate);
                update accsToUpdate;
            }
            //loop medical reports
            for(MedicalReport__c medicalReportAux : medicalReportList) {
                medicalReportAux.Patient__c = leadAccIdMap.get(medicalReportAux.Lead__c);
                //medicalReportAux.RecordTypeId = medicalReportRTid;
                medicalReportAux.Lead__c = null;
                System.debug('medicalReportAux.Patient__c: ' + medicalReportAux.Patient__c);
            }
            System.debug('medicalReportList: ' + medicalReportList);
            update medicalReportList;
            
            //loop payments
            for(Payment__c paymentAux : paymentList) {
                paymentAux.RelatedAccount__c = leadAccIdMap.get(paymentAux.RelatedLead__c);
                //paymentAux.RecordTypeId = medicalReportRTid;
                //medicalReportAux.Lead__c = null;
            }
            System.debug('paymentList: ' + paymentList);
            update paymentList;
            
            //loop events
            /*for(Event eventAux : eventList) {
                eventAux.WhatId = leadAccIdMap.get(eventAux.WhoId);
                eventAux.WhoId = null;
               // eventAux.isDeleted = false;
            }
            System.debug('eventList: ' + eventList);
            //undelete eventList;
            String jsonStringEvent = json.serialize(eventList);
            AutoconvertLeadHandler.undeleteEvents(jsonStringEvent);*/
			
            //set original lead owner
            String jsonString = json.serialize(leadOwnerMap);
			AutoconvertLeadHandler.changeLeadOwner(jsonString);
        }
    }
}