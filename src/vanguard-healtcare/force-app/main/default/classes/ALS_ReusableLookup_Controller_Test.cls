/**
 * In this class I have all the methods for testing ALS_ReusableLookup_Controller class
 *
 * @author		Jesús Martínez  <jesus.martinez@adesso.es>
 */
@isTest
public inherited sharing class ALS_ReusableLookup_Controller_Test {
    @TestSetup
    static void makeData(){
        Account acc = new Account();
        acc.Name = 'Account Test';
        acc.Phone = '666666666';
        insert acc;
    }

    static testMethod void fetchLookUpValues_test(){
        String searchKey = 'Account';
        String fieldName = 'Name';
        String fields = 'Phone';
        String ObjectName = 'Account';
        String keyField = 'Id';

        Test.startTest();
        List<sObject> listRecords = ALS_ReusableLookup_Controller.fetchLookUpValues(searchKey, fieldName, fields, ObjectName, keyField);

        List<sObject> getRecord = ALS_ReusableLookup_Controller.getRecord([Select id FROM Account Limit 1].id, fieldName, fields, ObjectName, keyField);
        Test.stopTest();

        System.assertEquals(1, listRecords.size());
    }
}