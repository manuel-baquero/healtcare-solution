/**
 * In this class I have all the methods that I will use in calendar component
 *
 * @author		Jesús Martínez  <jesus.martinez@adesso.es>
 */
public with sharing class ALS_calendarManagement_ctrl{

	/**
     * This method gets the system events
     *
     * @param		<agendaDate> - Date from which to make the query
     * @param		<agendasId> - List of Agenda ids to search
     * @param		<calendarType> - Type of calendar (day, week, month...)
     * @param		<lstSelectables> - List of picklist objects
     *
     * @return		List of events
     */
	@AuraEnabled(cacheable = false)
	public static List<Event> getAppointmentByDate(Date agendaDate, List<String> agendasId, String calendarType, List<SelectableWrapper> lstSelectables){
		Date startDate = agendaDate;
		Date endDate = agendaDate.addDays(1);
		if (calendarType.contains('Week')){
			startDate = agendaDate.toStartofWeek();
			endDate = startDate.addDays(7);
		} else if (calendarType.contains('Month')){
			startDate = agendaDate.toStartofMonth();
			endDate = startDate.addMonths(1);
		}
		String query = 'SELECT Id, StartDateTime, EndDateTime, Subject, WhatId, What.Name, ALS_MainAgenda__c, ALS_ParentId__c';
		for(Schema.FieldSetMember f : SObjectType.Event.FieldSets.ALS_Appointment_Fields.getFields()) {
			query += ', ' + f.getFieldPath();
		}
		query += ' FROM Event WHERE (ActivityDate >= :startDate AND ActivityDate < :endDate)';
		if (agendasId != null && agendasId.size() > 0){
			query +=  ' AND ALS_MainAgenda__c IN :agendasId';
		}
		query += getAdditionalConditions(lstSelectables);
		System.debug(query);
		return (List<Event>) Database.query(query);
		// return new List<Event>();
	}

    /**
    * @description 
    * @author intxixu.latorre@adesso.es | 2021-09-10 
    * @param List<SelectableWrapper> lstSelectables 
    * @return String 
    **/
    private static String getAdditionalConditions(List<SelectableWrapper> lstSelectables) {
        String conditions = '';
        for(SelectableWrapper selectable: lstSelectables) {
            List<String> selectedValues = selectable.selectedValues;
            if(!selectedValues.isEmpty()) {
                String eventField = String.escapeSingleQuotes(selectable.eventField);
                conditions += ' AND ' + eventField + ' IN (';
                String innerSeparator = '';
                for(String value: selectedValues) {
                    conditions += innerSeparator + '\'' + String.escapeSingleQuotes(value) + '\'';
                    innerSeparator = ',';
                }
                conditions += ')';
            }
        }
		return conditions;
    }

	/**
     * This method get the main agenda List filter by parameter.
     *
     * @param		<filter> - Where sentence of the query
     *
     * @return		List of agendas
     */
	@AuraEnabled(cacheable = true)
	public static List<AgendaData> getMainAgenda(String filter){
		List<AgendaData> result = new List<AgendaData>();
		System.debug('filter: ' + filter);
		String query = 'SELECT id, RelatedDoctor__c, RelatedDoctor__r.Name, Name, ('
			+ 'SELECT StartDate__c, EndDate__c, ActivityStart__c, ActivityEnd__c, ' + 
			+ 'Days__c FROM Agenda_Entries__r'
		+ ') FROM ALS_Agenda__c';
		String whereFilter = (filter != null && filter != '' ? ' WHERE ' + filter : '');

		for (sObject sAgenda : Database.query(query + whereFilter)){
			ALS_Agenda__c agenda = (ALS_Agenda__c)sAgenda;
			List<BusinessHourData> businessHourList = new List<BusinessHourData>();
			for (ALS_AgendaEntry__c ae : agenda.Agenda_Entries__r){
				businessHourList.add(new BusinessHourData(ae.ActivityStart__c, ae.ActivityEnd__c, ae.StartDate__c, ae.EndDate__c, ae.Days__c));
			}
			result.add(new AgendaData(agenda.Name, agenda.id, agenda.RelatedDoctor__c, businessHourList));
		}
		return result;
	}

	/**
     * This method get all agendas except the main agenda
     *
     * @param		<agendaId> - Main agenda Id
     *
     * @return		List of agendas
     */
	@AuraEnabled(cacheable = true)
	public static List<AgendaData> getOtherAgendas(String agendaId){
		List<AgendaData> result = new List<AgendaData>();
		for (ALS_Agenda__c agenda : [SELECT id, Name, RelatedDoctor__c, (SELECT StartDate__c, EndDate__c, ActivityStart__c, ActivityEnd__c, Days__c
		                                               FROM Agenda_Entries__r)
		                             FROM ALS_Agenda__c
		                             WHERE id != :agendaId]){
			List<BusinessHourData> businessHourList = new List<BusinessHourData>();
			for (ALS_AgendaEntry__c ae : agenda.Agenda_Entries__r){
				businessHourList.add(new BusinessHourData(ae.ActivityStart__c, ae.ActivityEnd__c, ae.StartDate__c, ae.EndDate__c, ae.Days__c));
			}
			result.add(new AgendaData(agenda.Name, agenda.id, agenda.RelatedDoctor__c, businessHourList));
		}
		return result;
	}

	/**
     * This method get calendar configuration
     *
     * @param		<calendar> - calendar Name for get his information
     *
     * @return		Calendar configuration
     */
	@AuraEnabled(cacheable = true)
	public static CalendarConfiguration getCalendarConfiguration(String calendar){
		System.debug(calendar);
		CalendarConfiguration result;
		List<LegendClass> legendList = new List<LegendClass>();
		// List<CalendarFieldClass> fieldList = new List<CalendarFieldClass>();
		//SOQL de la información del calendario
		for(ALS_CalendarConfiguration__c cc :[
			SELECT id, Type__c, DisableDayView__c, DisableMonthView__c, DisableWeekView__c, DisableOtherAgendas__c, EndHour__c, 
				PrimaryAgendaFilter__c, ShowWeekends__c, SlotDuration__c, StartHour__c, ResourceAreaWidth__c, ResourceLabelText__c, BrandingColor__c,
				NumberLegendItems__c, 
			(
				SELECT Label__c, Color__c, TextColor__c, id 
				FROM Legends__r Order by createdDate ASC
			), 
			(
				SELECT Id, RecordType.DeveloperName, ApiName__c, FieldName__c, IconName__c, KeyField__c, Label__c, Object__c,
					Order__c, Placeholder__c, Required__c, Size__c, DefaultValues__c
				FROM Calendar_Fields__r Order By Order__c ASC NULLS Last
			),
			(
				SELECT Id, Name, CalendarConfiguration__c, Position__c, Filter__c, ObjectName__c,
					LabelField__c, ValueField__c, EventField__c, MaxSelectedItems__c
				FROM CalendarFilters__r
				ORDER BY Position__c ASC
			)
			FROM ALS_CalendarConfiguration__c Where name =: calendar Limit 1]){
				//Extraemos la info de la legenda
				Map<Id, List<ALS_CalendarCondition__c>> mapCondition = new Map<Id, List<ALS_CalendarCondition__c>>();
				for(ALS_CalendarCondition__c condition :[
					SELECT id, Value__c, Field_ApiName__c, Operation__c, Legend__c
					FROM ALS_CalendarCondition__c Where Legend__c in :cc.Legends__r]){
						if(mapCondition.get(condition.Legend__c) == null) mapCondition.put(condition.Legend__c, new List<ALS_CalendarCondition__c>()); 
						List<ALS_CalendarCondition__c> aux = mapCondition.get(condition.Legend__c);
						aux.add(condition);
						mapCondition.put(condition.Legend__c, aux);
				}

				for(ALS_CalendarLegend__c legend :cc.Legends__r){
					legendList.add(new LegendClass(legend.Label__c, legend.Color__c, legend.TextColor__c, mapCondition.get(legend.id)));
				}

				Map<Id, CalendarFieldClass> pickListFieldMap= new Map<Id,CalendarFieldClass>();
				List<CalendarFieldClass> calendarFieldList = new List<CalendarFieldClass>();
				//Extraemos la info de los campos a mostrar

// ---> START DIFF

				// START V1
				Map<Id, List<FieldOptionClass>> optionMap = new Map<Id, List<FieldOptionClass>>();
				for(ALS_CalendarField__c field :cc.Calendar_Fields__r){

					CalendarFieldClass calendarField = new CalendarFieldClass(field, null);

					//PickList/Multipicklist tienen opciones de selección, que recuperaremos más tarde
					if(field.RecordType.DeveloperName.contains('Picklist')){
						if(!field.DefaultValues__c) pickListFieldMap.put(field.id, calendarField);
						//Obtenemos los valores por defecto de la picklist
						else{
							String[] types = new String[]{'Event'};
							Schema.DescribeSobjectResult[] results = Schema.describeSObjects(types);
							for(Schema.DescribeSobjectResult res : results) {
								Integer cont = 0;
								String fieldName = field.ApiName__c;
								try {						
									for (Schema.PicklistEntry entry : res.fields.getMap().get(field.ApiName__c).getDescribe().getPicklistValues()) {
										if (entry.isActive()) {
											if(optionMap.get(field.id) == null){
												FieldOptionClass fo = new FieldOptionClass(entry.getLabel(), entry.getValue(), cont++);
												optionMap.put(field.id, new List<FieldOptionClass>{fo});
											}else {
												List<FieldOptionClass> aux  = optionMap.get(field.id);
												FieldOptionClass fo = new FieldOptionClass(entry.getLabel(), entry.getValue(), cont++);
												aux.add(fo);
												optionMap.put(field.id, aux);
											}
										}
									}  
								} catch (Exception e) {
									throw new AuraHandledException('El campo ' + fieldName + ' no existe en los eventos o no es de tipo picklist.');
								}
							}
						}
					}
					calendarFieldList.add(calendarField);
				}
				// END V1

				// START V2
				/*
				for(ALS_CalendarField__c field :cc.Calendar_Fields__r){

					CalendarFieldClass calendarField = new CalendarFieldClass(field, null);

					//PickList/Multipicklist tienen opciones de selección, que recuperaremos más tarde
					if(field.RecordType.DeveloperName.contains('Picklist')){
						pickListFieldMap.put(field.id, calendarField);
					}
					calendarFieldList.add(calendarField);
				}

				Map<Id, List<FieldOptionClass>> optionMap = new Map<Id, List<FieldOptionClass>>();
				*/
				// END V2
				
// ---> END DIFF

				//Recuperamos las opciones de selección de los campos tipo lista de selección
				for(ALS_CalendarOptions__c option : [
					SELECT Label__c, Value__c, Order__c, Field__c 
					FROM ALS_CalendarOptions__c Where Field__c IN :pickListFieldMap.keySet() Order By Order__c ASC NULLS Last]){
					if(optionMap.get(option.Field__c) == null){
						FieldOptionClass fo = new FieldOptionClass(option.Label__c, option.Value__c, Integer.valueOf(option.Order__c));
						optionMap.put(option.Field__c, new List<FieldOptionClass>{fo});
					}else {
						List<FieldOptionClass> aux  = optionMap.get(option.Field__c);
						FieldOptionClass fo = new FieldOptionClass(option.Label__c, option.Value__c, Integer.valueOf(option.Order__c));
						aux.add(fo);
						optionMap.put(option.Field__c, aux);
					}
				}

				//Actualizamos el objeto que devolveremos al JS
				for(CalendarFieldClass cf :calendarFieldList){
					if(optionMap.get(cf.sfId) !=  null) cf.Options = optionMap.get(cf.sfId);
				}

				// NEW:INTXI
				List<SelectableWrapper> filterList = getMultiSelectors(cc.CalendarFilters__r);
				result = new CalendarConfiguration(cc, legendList, calendarFieldList, filterList);
				// result = new CalendarConfiguration(cc, legendList, calendarFieldList);
		}

		System.debug(result);
		return result;
	}

	/**
     * This method create a new event.
     *
     * @param		<subject> - Subject field
	 * @param		<description> - Description field
	 * @param		<recordType> - RecordType ID 
	 * @param		<mainAgenda> - Main agenda Id
	 * @param		<recordId> - related Record Id
	 * @param		<startDate> - Event start date
	 * @param		<otherAgendas> - Other agendas Ids for duplicate events
	 * @param		<startHour> - Start hour of the event
	 * @param		<duration> - Duration of the event in minutes
	 * @param		<fields> - FieldData list for dynamic field information.
     *
     * @return		New event id
     */
	@AuraEnabled(cacheable = false)
	public static Id createEvent(String subject, String description, String recordType, String mainAgenda, String recordId, String startDate, List<String> otherAgendas, 
		String startHour, Boolean leadRecord, Integer duration, String fields){

		sObject temp = Schema.getGlobalDescribe().get('Event').newSObject();
		if (recordType != null && recordType != '' && isAvailableField('Event', 'RecordTypeId'))
			temp.put('RecordTypeId', recordType);
		
		DateTime startDateTime = DateTime.newInstance(Integer.valueOf(startDate.split('-')[0]), Integer.valueOf(startDate.split('-')[1]), Integer.valueOf(startDate.split('-')[2]), Integer.valueOf(startHour.split(':')[0]), Integer.valueOf(startHour.split(':')[1]), 0);
		temp.put('startDateTime', startDateTime);
		temp.put('DurationInMinutes', duration);
		temp.put('Description', description);
		if(mainAgenda != null){
			temp.put('ALS_MainAgenda__c', mainAgenda);
		}
		if(recordId != null && !leadRecord){
			temp.put('WhatId', recordId);
		}else if (recordId != null && leadRecord){
			temp.put('WhoId', recordId);
		}
		temp.put('Subject', subject);

		//Dynamic fields
		if(fields != '' && fields != null){
			FieldData fieldList = (FieldData)System.JSON.deserialize(fields, FieldData.class);
			for(Data fd : fieldList.data){
				if(fd.value != null && fd.value != '') {
					try {
						if(fd.apiName == 'WhatId' && ((Id) fd.value).getSObjectType() == Schema.Lead.SObjectType) {
							fd.apiName = 'WhoId';
						}
					} catch(Exception e) {
						// pass
					}
					temp.put(fd.apiName, fd.value);
				}
			}
		}
		
		insert temp;

		//Insert Associated Agendas
		List<Event> associatedEvents = new List<Event>();
		Event aux = (Event) temp;
		for(String otherAgenda : otherAgendas){			
			Event cloneEvent = aux.clone(false, true, false, false);
			cloneEvent.ALS_ParentId__c = temp.id;
			cloneEvent.ALS_MainAgenda__c = otherAgenda; 
			associatedEvents.add(cloneEvent);
		}

		System.debug(associatedEvents);
		if(!associatedEvents.isEmpty()) insert associatedEvents;

		return temp.Id;
	}

	/**
     * This method return avaibles event recordtype
     *
     * @return		List of recordTypes
     */
	@AuraEnabled(cacheable = false)
	public static List<RecordType> getEventRecordType(){
		return [select Id, Name from RecordType where sObjectType = 'Event' AND isActive = true AND developerName != 'Cita_Asociada'];
	}

	/**
     * This method create a ALS_CalendarLegend__c record
	 * 
	 * @param		<label> - legend label
	 * @param		<color> - legend color
	 * @param		<calendar> - ALS_CalendarConfiguration__c id
     *
     * @return		New Calendar Legend id
     */
	@AuraEnabled
    public static Id createCalendarLegend(String label, String color, String calendar){
        try {
			ALS_CalendarLegend__c legend = new ALS_CalendarLegend__c(Color__c = color, Label__c = label, Calendar__c = calendar);
			insert legend;
			return legend.id;
		} catch (Exception e) {
			System.debug('Error: ' +  e.getStackTraceString());
			return null;	
		}		 
    }

	/**
     * This method checks that the field exists for a certain object
	 * 
	 * @param		<sObjectAPIName> - Object ApiName
	 * @param		<fieldAPIName> - Field ApiName
     *
     * @return		True/False
     */
	public static Boolean isAvailableField(String sObjectAPIName, String fieldAPIName){
		// call to get the appropriate object's describe
		Map<String, Schema.SObjectType> mapGlobalDescribe = Schema.getGlobalDescribe();
		Schema.SObjectType objSObjectType = mapGlobalDescribe.get(sObjectAPIName);

		// field describe
		Schema.DescribeSObjectResult objectDescribe = objSObjectType.getDescribe();
		Map<String, Schema.SObjectField> mapFieldDescribe = objSObjectType.getDescribe().fields.getMap();

		// check if field exists in describe call
		return mapFieldDescribe.containsKey(fieldAPIName);
	}

	/**
     * This method update or clone a event
	 * 
	 * @param		<recordId> - Event id
	 * @param		<startDate> - new StartDate
	 * @param		<endDate> - new EndDate
	 * @param		<ownerId> - Event Owner Id 
	 * @param		<agendaId> - Assigned Agenda Id
	 * @param		<isClone> - True/False
     *
     */
	@AuraEnabled(cacheable = false)
	public static void updateCita(String recordId, String startDate, String endDate, String ownerId, String agendaId, Boolean isClone){		
		DateTime startDT = DateTime.valueOf(startDate);
		Event e;
		if(isClone){
			String query = 'SELECT Id, StartDateTime, EndDateTime, Subject, WhatId, What.Name, ALS_MainAgenda__c, ALS_ParentId__c, OwnerId ';
			for(Schema.FieldSetMember f : SObjectType.Event.FieldSets.ALS_Appointment_Fields.getFields()) {
				query += ', ' + f.getFieldPath();
			}
			query += ' FROM Event WHERE id =: recordID';
			List<Event> eventList = (List<Event>) Database.query(query);

			e = eventList[0].clone(false, true, false, false);
			e.startDateTime = DateTime.valueOf(startDate);
			e.endDateTime = null;
			e.ALS_MainAgenda__c = agendaId;
			if(ownerId != null) e.OwnerId = ownerId;
		}else{
			e = new Event(id= recordId, ALS_MainAgenda__c = agendaId);
			if(ownerId != null) e.OwnerId = ownerId;
			if(startDate != '') e.startDateTime = DateTime.valueOf(startDate);
			if(endDate != '') e.endDateTime = DateTime.valueOf(endDate);
		}		
		if(isClone) insert e;
		else update(e);	
	}

	public class AgendaData{
		@AuraEnabled
		public String Name;
		@AuraEnabled
		public String Id;
		@AuraEnabled
		public String Owner;
		@AuraEnabled
		public List<BusinessHourData> businessHour;
		public AgendaData(String Name, String Id, String Owner, List<BusinessHourData> businessHour){
			this.Name = name;
			this.Id = id;
			this.Owner = Owner;
			this.businessHour = businessHour;
		}
	}

	public class BusinessHourData{
		@AuraEnabled
		public String startTime;
		@AuraEnabled
		public String endTime;
		@AuraEnabled
		public Date startDate;
		@AuraEnabled
		public Date endDate;
		@AuraEnabled
		public String days;
		public BusinessHourData(String startTime, String endTime, Date startDate, Date endDate, String days){
			this.startTime = startTime;
			this.endTime = endTime;
			this.startDate = startDate;
			this.endDate = endDate;
			this.days = days;
		}
	}	

	public class FieldData{
		public List<Data> data {get;set;}
	}

	public class Data{
		public String apiName {get;set;}
		public String value {get;set;}
	}

	public class CalendarConfiguration{
		@AuraEnabled
		public String Id {get;set;}
		@AuraEnabled
		public String Type {get;set;}
		@AuraEnabled
		public String Filter {get;set;}
		@AuraEnabled
		public Integer NumberLegendItems {get;set;}
		@AuraEnabled
		public String SlotDuration {get;set;}
		@AuraEnabled
		public String StartHour {get;set;}
		@AuraEnabled
		public String EndHour {get;set;}
		@AuraEnabled
		public Boolean DisableOtherAgendas {get;set;}
		@AuraEnabled
		public Boolean ShowWeekends {get;set;}
		@AuraEnabled
		public Boolean DisableDayView {get;set;}
		@AuraEnabled
		public Boolean DisableWeekView {get;set;}
		@AuraEnabled
		public Boolean DisableMonthView {get;set;}
		@AuraEnabled
		public List<LegendClass> Legends {get;set;}
		@AuraEnabled
		public List<CalendarFieldClass> Fields {get;set;}
		@AuraEnabled
		public Decimal ResourceAreaWidth {get;set;}
		@AuraEnabled
		public String ResourceLabelText {get;set;}
		@AuraEnabled
		public String BrandingColor {get;set;}
		@AuraEnabled
		public String Picklists {get; set;}

		public CalendarConfiguration(ALS_CalendarConfiguration__c cc, List<LegendClass> Legends, List<CalendarFieldClass> Fields, List<SelectableWrapper> Filters){
			this.Id = cc.Id;
			this.Type = cc.Type__c;
			this.Filter = cc.PrimaryAgendaFilter__c;
			if(cc.NumberLegendItems__c != null) this.NumberLegendItems = Integer.valueOf(cc.NumberLegendItems__c);
			this.SlotDuration = cc.SlotDuration__c;
			this.StartHour = cc.StartHour__c;
			this.EndHour = cc.EndHour__c;
			this.DisableOtherAgendas = cc.DisableOtherAgendas__c;
			this.ShowWeekends = cc.ShowWeekends__c;
			this.DisableDayView = cc.DisableDayView__c;
			this.DisableWeekView = cc.DisableWeekView__c;
			this.DisableMonthView = cc.DisableMonthView__c;
			this.Legends = Legends;
			this.Fields = Fields;
			this.ResourceAreaWidth = cc.ResourceAreaWidth__c;
			this.ResourceLabelText = cc.ResourceLabelText__c;
			this.BrandingColor = cc.BrandingColor__c;
			this.Picklists = JSON.serialize(Filters);
		}
	}

	public class LegendClass{
		@AuraEnabled
		public String Label {get;set;}
		@AuraEnabled
		public String Color {get;set;}
		@AuraEnabled
		public String TextColor {get;set;}	
		@AuraEnabled
		public List<CalendarConditionClass> Conditions {get;set;}

		public LegendClass(String Label, String Color, String TextColor, List<ALS_CalendarCondition__c> conditions){
			this.Label = Label;
			this.Color = Color;
			this.TextColor = TextColor;
			this.Conditions = new List<CalendarConditionClass>();
			if(conditions != null){
				for(ALS_CalendarCondition__c cc : conditions){
					this.Conditions.add(new CalendarConditionClass(cc.Value__c, cc.Operation__c, cc.Field_ApiName__c));
				}
			}			
		}
	}

	public class CalendarConditionClass{
		@AuraEnabled
		public String Value {get;set;}
		@AuraEnabled
		public String Operation {get;set;}
		@AuraEnabled
		public String FieldApiName {get;set;}
		
		public CalendarConditionClass(String Value, String Operation, String FieldApiName){
			
			this.Value = Value;
			this.Operation = Operation;
			this.FieldApiName = FieldApiName;
		}
	}

	public class CalendarFieldClass{
		@AuraEnabled
		public Id SfId {get;set;}
		@AuraEnabled
		public String Type {get;set;}
		@AuraEnabled
		public String ApiName {get;set;}
		@AuraEnabled
		public String FieldName {get;set;}
		// @AuraEnabled
		// public String RecordFields {get;set;}
		@AuraEnabled
		public String IconName {get;set;}
		@AuraEnabled
		public String KeyField {get;set;}
		@AuraEnabled
		public String Label {get;set;}
		@AuraEnabled
		public String ObjectName {get;set;}
		@AuraEnabled
		public Integer Order {get;set;}
		@AuraEnabled
		public String Placeholder {get;set;}
		@AuraEnabled
		public Boolean Required {get;set;}
		@AuraEnabled
		public Integer Size {get;set;}		
		@AuraEnabled
		public List<FieldOptionClass> Options {get;set;}

		public CalendarFieldClass(ALS_CalendarField__c field, List<FieldOptionClass> Options){
			this.SfId = field.Id;
			this.Type = field.RecordType.DeveloperName;
			this.ApiName = field.ApiName__c;
			this.FieldName = field.FieldName__c;
			this.IconName = field.IconName__c;
			this.KeyField = field.KeyField__c;
			this.Label = field.Label__c;
			this.ObjectName = field.Object__c;
			this.Order = Integer.valueOf(field.Order__c);
			this.Placeholder = field.Placeholder__c;
			this.Required = field.Required__c;
			this.Size = Integer.valueOf(field.Size__c);
			this.Options = Options;
		}

	}

	public class FieldOptionClass{
		@AuraEnabled
		public String Label {get;set;}
		@AuraEnabled
		public String Value {get;set;}	
		@AuraEnabled
		public Integer Order {get;set;}	

		Public FieldOptionClass(String Label, String Value, Integer Order){
			this.Label = Label;
			this.Value = Value;
			this.Order = Order;
		}
	}


	// NEW:INTXI
    private static List<SelectableWrapper> getMultiSelectors(List<ALS_CalendarFilter__c> lstFilter/*, List<Map<String,Object>> lstMapSelectables*/) {

		List<SelectableWrapper> lstSelectable = new List<SelectableWrapper>();

        for(ALS_CalendarFilter__c filter: lstFilter) {

			SelectableWrapper slct = new SelectableWrapper(filter);
            String objectName = slct.objectName; // String.escapeSingleQuotes(slct.objectName);
            String valueField = slct.valueField; // String.escapeSingleQuotes(slct.valueField);
            String labelField = slct.labelField; // String.escapeSingleQuotes(slct.labelField);
            String eventField = slct.eventField; // String.escapeSingleQuotes(slct.eventField);

            if(objectName == 'Event') {

                List<PicklistEntry> lstPlEntry = Schema.getGlobalDescribe()
                    .get(objectName).getDescribe().fields.getMap()
                    .get(eventField).getDescribe().getPicklistValues();

                List<PicklistOption> options = new List<PicklistOption>();
                for(PicklistEntry plEntry: lstPlEntry) {
                    options.add(new PicklistOption(
                        plEntry.getLabel(),
                        plEntry.getValue()
                    ));
                }
                slct.options = options;

            } else {

                String filterString = filter.Filter__c;

                List<SObject> sObjList = Database.query('SELECT '
                    + valueField + ','
                    + labelField
                    + ' FROM ' + objectName
                    + (String.isNotBlank(filterString) ? ' WHERE ' + filterString : '')
                    + ' ORDER BY ' + labelField + ' ASC'
                );

                List<PicklistOption> options = new List<PicklistOption>();
                for(SObject sObj: sObjList) {
                    options.add(new PicklistOption(
                        (String) sObj.get(labelField),
                        (String) sObj.get(valueField)
                    ));
                }
                slct.options = options;

            }

			lstSelectable.add(slct);

        }
        return lstSelectable;
    }

    @AuraEnabled(cacheable=true)
    public static Event getEventData(Id recordId) {
        return [
            SELECT Id, Subject, Account.Name, ALS_Specialty__c
                // TOLABEL(EVT_pck_especialidad__c), EVT_lkp_Tratamiento__r.StageName, EVT_lkp_prestacion__r.Name
            FROM Event
            WHERE Id = :recordId
            // WITH SECURITY_ENFORCED Cannnot be used w polymorphic
        ];
    }

	public class SelectableWrapper {
		@AuraEnabled public Integer position {get; set;}
		@AuraEnabled public String label {get; set;}
		@AuraEnabled public String objectName {get; set;}
		@AuraEnabled public String labelField {get; set;}
		@AuraEnabled public String valueField {get; set;}
		@AuraEnabled public String eventField {get; set;}
		@AuraEnabled public Integer maxSelectedItems {get; set;}
		@AuraEnabled public List<PicklistOption> options {get; set;}
		@AuraEnabled public List<String> selectedValues {get; set;}
		public SelectableWrapper() {
			// Empty constructor needed for auto deserialization
		}
		public SelectableWrapper(ALS_CalendarFilter__c filter) {
			this.position         = (Integer) filter.Position__c;
			this.label            = filter.Name;
			this.objectName       = filter.ObjectName__c;
			this.labelField       = filter.LabelField__c;
			this.valueField       = filter.ValueField__c;
			this.eventField       = filter.EventField__c;
			this.maxSelectedItems = (Integer) filter.MaxSelectedItems__c;
			this.options          = new List<PicklistOption>();
            this.selectedValues   = new List<String>();
		}
	}

	public class PicklistOption {
		@AuraEnabled public String value {get; set;}
		@AuraEnabled public String key {get; set;}
		public PicklistOption() {
			// Empty constructor needed for auto deserialization
		}
		public PicklistOption(String value, String key) {
			this.value = value;
			this.key = key;
		}
	}

}