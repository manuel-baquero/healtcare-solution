/**
 * In this class I have all the configuration for Event trigger
 *
 * @author		Jesús Martínez  <jesus.martinez@adesso.es>
 */
public inherited sharing class ALS_ReusableLookup_Controller {

    /**
     * This method gets objects with a less accurate search using a 'Like' query
     *
     * @param		<searchKey> - Value for Apiname that its going to be used for the where query sentence
     * @param		<fieldName> - Apiname that its going to be used for the where query sentence
     * @param		<fields> - List of Apiname for select fields
     * @param		<ObjectName> - Object ApiName
     * @param		<keyField> - Select key field ApiName
     *
     * @return		List of sobject
     */
    @AuraEnabled(cacheable=false)
    public static List<SObject> fetchLookUpValues(String searchKey, String fieldName, String fields, String ObjectName, String keyField) {
        List<SObject> lstSObject = new List<SObject>();
        for(String objName: ObjectName.split(',')) {
            lstSObject.addAll(handleFetchLookUpValues(searchKey, fieldName, fields, objName, keyField));
        }
        return lstSObject;
    }

    public static List<sObject> handleFetchLookUpValues(String searchKey,String fieldName,String fields,String ObjectName, String keyField) {
        String queryKey = '\'%' + String.escapeSingleQuotes(searchKey.trim()) + '%\'';
        String sQuery = 'SELECT ' + keyField + ', ' + fieldName + (fields != null ? ', ' + fields : '') + ' FROM ' + ObjectName + 
              ' WHERE ' + fieldName + ' LIKE ' + queryKey +
              getConditions(ObjectName) +
              ' ORDER BY ' + fieldName + ' LIMIT 50 ';
        return Database.query(sQuery);
    }

    private static String getConditions(String objectName) {
        String conditions = '';
        switch on (objectName) {
            when 'Account' {
                conditions = 'IsPersonAccount = true';
            }
            when 'Lead' {
                conditions = 'IsConverted = false';
            }
        }
        if(String.isNotBlank(conditions)) {
            conditions = ' AND ' + conditions;
        }
        return conditions;
    }

    /**
     * This method gets objects with a precise search using a equal query
     *
     * @param		<searchKey> - Value for Apiname that its going to be used for the where query sentence
     * @param		<fieldName> - Apiname that its going to be used for the where query sentence
     * @param		<fields> - List of Apiname for select fields
     * @param		<ObjectName> - Object ApiName
     * @param		<keyField> - Select key field ApiName
     *
     * @return		List of sobject
     */
    @AuraEnabled(cacheable = false)
    public static List<sObject> getRecord(String searchKey, String fieldName, String fields, String ObjectName, String keyField){
        List<sObject> returnList = new List<sObject>();
        String queryKey = '\'' + String.escapeSingleQuotes(searchKey.trim())+'\'';
        String sQuery = 'select ' + keyField + ', ' + fieldName + (fields != null ? ', ' + fields : '')+' from ' + ObjectName + 
          ' where ' + keyField + ' = ' + queryKey;
        List<sObject> lstOfRecords = Database.query(sQuery);
        for (sObject obj : lstOfRecords){
            returnList.add(obj);
        }
        System.debug(returnList);
        return returnList;
    }
  }