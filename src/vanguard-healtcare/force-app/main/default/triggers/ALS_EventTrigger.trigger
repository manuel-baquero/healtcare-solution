trigger ALS_EventTrigger on Event (before delete, after update) {

    if(Trigger.isBefore){
        if(Trigger.isDelete){
            ALS_EventTriggerHandler.beforeDelete(Trigger.old);
        }
    }
    if(Trigger.isAfter){
        if(Trigger.isUpdate){
            ALS_EventTriggerHandler.afterUpdate(Trigger.new, Trigger.oldMap);
        }
    }

}