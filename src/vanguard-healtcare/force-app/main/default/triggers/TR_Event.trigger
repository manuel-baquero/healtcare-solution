trigger TR_Event on Event(before insert, before update, after insert, after update, before delete) {
  if(VC_TR_Event_Handler.runTrigger){
    VC_TR_Event_Handler.handleTrigger(Trigger.operationType, Trigger.newMap, Trigger.oldMap);
    
  }   
}