import { LightningElement, api, wire, track } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getSurvey from '@salesforce/apex/SurveyTakerController.getSurvey';
import getNewSurveyTaker from '@salesforce/apex/SurveyTakerController.getNewSurveyTaker';
import respondSurvey from '@salesforce/apex/SurveyTakerController.respondSurvey';

// Static resource
import SurveyImages from '@salesforce/resourceUrl/SurveyImages';

const DEFAULT_LAUNCH_BUTTON_LABEL = 'Launch Survey';
const DEFAULT_BACKGROUND_IMAGE = 'backgroundIMG3.jpg';
const DEFAULT_BACKGROUND_STYLES = 'background-size: cover; background-repeat: no-repeat; background-position: center center;';
const DELAY_IN_MS = 1000;

// Styling hooks
// button
const BUTTON_BG = '--sds-c-button-brand-color-background';
const BUTTON_BG_HOVER = '--sds-c-button-brand-color-background-hover';
const BUTTON_BG_ACTIVE = '--sds-c-button-brand-color-background-active';
const BUTTON_SHADOW = '--sds-c-button-shadow-focus';
const BUTTON_BORDER = '--sds-c-button-brand-color-border';
const BUTTON_BORDER_HOVER = '--sds-c-button-brand-color-border-hover';
const BUTTON_BORDER_ACTIVE = '--sds-c-button-brand-color-border-active';
const BUTTON_BORDER_OUTLINE = '--sds-c-button-outline-brand-color-border';
const BUTTON_BORDER_OUTLINE_HOVER = '--sds-c-button-outline-brand-color-border-hover';
// dual-listbox
const DL_BUTTONICON_HOVER = '--sds-c-button-text-color-hover';
const DL_BUTTONICON_ACTIVE = '--sds-c-button-text-color-active';

export default class SurveyTaker extends LightningElement {

    @api surveyCode;
    @api launchButtonLabel;

    _surveyTakerId;
    @track _parsedData = {};
    @track _componentList = [];

    _showSurvey = false;
    _showConfirmation = false;
    _showInformation = false;
    _showError = false;
    _surveyStarted = false;
    _showSpinner = false;

    get _launchButtonLabel() {
        return this._parsedData.launchLabel || DEFAULT_LAUNCH_BUTTON_LABEL;
    }
    get _headerPic() {
        return this._parsedData.headerImage;
    }
    get _backgroundPic() {
        let styleString = DEFAULT_BACKGROUND_STYLES;
        if (this._parsedData.backgroundImage) {
            styleString += `background-image: url('${this._parsedData.backgroundImage}');`;
        } else {
            const resourcePath = `${SurveyImages}/${DEFAULT_BACKGROUND_IMAGE}`;
            styleString += `background-image: url('${resourcePath}');`;
        }
        return styleString;
    }
    get _backgroundColor() {
        return `background-color: ${this._parsedData.backgroundColor};`;
    }
    get _buttonStyle() {
        return `${BUTTON_BG}: ${this._parsedData.brandColor};`
            + `${BUTTON_SHADOW}: ${this._parsedData.brandColor};`
            + `${BUTTON_BORDER}: ${this._parsedData.brandColor};`
            + `${BUTTON_BG_HOVER}: ${this._parsedData.brandColorDark};`
            + `${BUTTON_BG_ACTIVE}: ${this._parsedData.brandColorDark};`
            + `${BUTTON_BORDER_HOVER}: ${this._parsedData.brandColorDark};`
            + `${BUTTON_BORDER_ACTIVE}: ${this._parsedData.brandColorDark};`
            + `${BUTTON_BORDER_OUTLINE}: ${this._parsedData.brandColorDark};`
            + `${BUTTON_BORDER_OUTLINE_HOVER}: ${this._parsedData.brandColorDark};`;
    }
    get _multiPicklistStyle() {
        return `${DL_BUTTONICON_HOVER}: ${this._parsedData.brandColor};`
            + `${DL_BUTTONICON_ACTIVE}: ${this._parsedData.brandColor};`;
    }

    @wire(getSurvey, { surveyTakerId: '$_surveyTakerId', surveyCode: '$surveyCode' })
    handleGetSurvey({ error, data }) {
        if (error) {
            this._surveyTakerId = null;
            this._showError = true;
            this._showSpinner = false;
        } else if (data) {
            this.handleSurveyData(data);
            this._showSpinner = false;
        }
    }

    connectedCallback() {
        this.handleInit();
    }

    handleInit() {
        this._showSpinner = true;
        // if no surveyCode, search for surveyTakerId in the URL
        const search = location.search.substring(1);
        
        let params;
        if (search) {
            params = this.decodeParams(search);
        }
        if (params?.surveyTakerId) {
            this._surveyTakerId = params.surveyTakerId;
            this.surveyCode = null;
        } else if (params?.surveyCode) {
            this._surveyTakerId = null;
            this.surveyCode = params.surveyCode;
        } else if (this.surveyCode) {
            this._surveyTakerId = null;
        } else {
            this._showError = true;
        }
    }

    decodeParams(inputStr) {
        return JSON.parse('{"' + inputStr.replace(/&/g, '","').replace(/=/g, '":"') + '"}', (key, value) => {
            return key === "" ? value : decodeURIComponent(value)
        });
    }

    startSurvey() {
        this._showSpinner = true;
        if(!this._surveyTakerId) {
            this.callGetNewSurveyTaker();
        }
        setTimeout(() => {
            this._showSpinner = false;
            this._surveyStarted = true;
        }, DELAY_IN_MS);
    }

    callGetNewSurveyTaker() {
        const surveyCode = this.surveyCode;
        getNewSurveyTaker({ surveyCode })
            .then(result => {
                if (result) {
                    this._surveyTakerId = result;
                } else {
                    this._showError = true;
                }
            })
            .catch(error => {
                this._showError = true;
            });
    }

    handleSurveyData(data) {
        this._parsedData = JSON.parse(data);
        if (this._parsedData.taken) {
            this._showInformation = true;
            this._surveyStarted = true;
        } else if (this._parsedData.questions) {
            this.renderSurvey(this._parsedData.questions);
        }
    }

    renderSurvey(questionList) {
        for (let question of questionList) {
            question[question.type] = true;  // this decides the conditional rendering
            this.handleTypeSpecificActions(question);
        }
        this._componentList = questionList;
        this._showSurvey = true;
    }

    handleTypeSpecificActions(question) {
        switch (question.type) {
            case 'Picklist_Other':
                question.choices.push({
                    label: this._parsedData.freeTextOption,
                    value: this._parsedData.freeTextOption
                });
                break;
        }
    }

    handlePicklistChange(event) {
        const newValue = event.detail.value;
        let cmp = this.getElementByLocalId(event);
        // Conditionally show the free text area
        cmp.showTextArea = newValue === this._parsedData.freeTextOption;
    }

    getElementByLocalId(event) {
        const localId = event.target.getAttribute('data-id');
        return this._componentList.find(elem => elem.questionId === localId);
    }

    handleSendClicked() {
        const lstResponse = this.checkResponses();
        if (lstResponse) {
            const _surveyTakerId = this._surveyTakerId;
            this.sendResponses(_surveyTakerId, lstResponse);
        }
    }

    sendResponses(surveyTakerId, lstResponse) {
        respondSurvey({ surveyTakerId, lstResponse })
            .then(result => {
                if (result) {
                    this.handleSendSuccess();
                } else {
                    this.handleSendError();
                }
            })
            .catch(error => {
                this.handleSendError();
            });
    }

    handleSendSuccess() {
        this._showSurvey = false;
        this._showError = false;
        this._showConfirmation = true;
    }

    handleSendError() {
        this._showSurvey = true;
        this._showError = true;
    }

    checkResponses() {
        // Get all questions from template
        const questionArray = this.template.querySelectorAll('.question');
        let allValid = true;
        const responseArray = [];
        for (let question of questionArray) {

            // Check the validity state for each question and report if invalid
            allValid &= question.reportValidity();

            const questionId = question.getAttribute('data-id');
            const response = question.value;
            if (Array.isArray(response)) {
                // For arrays, add a ranked response for each element
                let i = 1;
                for (let elem of response) {
                    const mapped = this.getResponseMap(i, questionId, elem);
                    responseArray.push(mapped);
                    i++;
                };
            } else {
                const mapped = this.getResponseMap(null, questionId, response);
                responseArray.push(mapped);
            }

        }
        if (allValid) {
            return responseArray;
        }
        return null;
    }

    getResponseMap(position, questionId, response) {
        return { position, questionId, response };
    }

    // Be careful when to use, the client should not receive detailed messages
    showToast(variant, title, message) {
        this.dispatchEvent(
            new ShowToastEvent({ variant, title, message })
        );
    }

}