import { LightningElement, api } from 'lwc';
import { updateRecord } from 'lightning/uiRecordApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getProfilePhoto from '@salesforce/apex/SurveyProfilePhotoUploaderController.getProfilePhotoURL';
import publishFile from '@salesforce/apex/SurveyProfilePhotoUploaderController.publishFile';
import PROFILE_PICTURE_FIELD from '@salesforce/schema/ContentVersion.ProfilePicture__c';
import ID_FIELD from '@salesforce/schema/ContentVersion.Id';

export default class SurveyProfilePhotoUploader extends LightningElement {

    @api isProfilePicture;
    @api panelTitle;
    @api uploadLabel;
    @api size;
    @api isRoundCorner;
    @api recordId;
    @api photoUrl;
    @api successMessage;
    @api defaultPhoto;
    @api fieldToUpdate;
    loaded = false;
    newPhoto = true;

    get acceptedFormats() {
        return ['.jpg', '.png', '.jpeg'];
    }

    get cssClass() {
        if (this.isRoundCorner) {
            return 'roundImg';
        } else {
            return 'squareImg';
        }
    }

    connectedCallback() {
        this.setUpPhoto();
    }

    setUpPhoto() {
        getProfilePhoto({ recordId: this.recordId, isProfilePicture: this.isProfilePicture })
            .then(result => {
                if (result !== '') {
                    this.photoUrl = result;
                    this.loaded = !this.loaded;
                    this.newPhoto = !this.newPhoto;
                } else {
                    this.resetPhoto();
                }
            })
            .catch(error => {
                this.resetPhoto();
            });
    }


    handleUploadFinished(event) {
        const uploadedFiles = event.detail.files;
        const fields = {};
        fields[ID_FIELD.fieldApiName] = uploadedFiles[0].contentVersionId;
        fields[PROFILE_PICTURE_FIELD.fieldApiName] = this.isProfilePicture;
        const recordInput = { fields };

        updateRecord(recordInput)
            .then(() => {
                this.photoUrl = '/sfc/servlet.shepherd/version/download/' + uploadedFiles[0].contentVersionId;
                this.newPhoto = !this.newPhoto;
                const contentVersionId = event.detail?.files[0]?.contentVersionId;
                this.handleNewContentVersion(contentVersionId);
            })
            .catch(error => {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: '',
                        message: error.message,
                        variant: 'error'
                    })
                );
            });
    }

    showNewPhotoUploader() {
        this.newPhoto = !this.newPhoto;
    }

    resetPhoto() {
        this.photoUrl = 'https://www.lightningdesignsystem.com/assets/images/avatar2.jpg';
        this.loaded = !this.loaded;
    }

    handleNewContentVersion(contentVersionId) {
        const recordId = this.recordId;
        const fieldToUpdate = this.fieldToUpdate;
        if(contentVersionId) {
            publishFile({contentVersionId, recordId, fieldToUpdate})
                .then(result => {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: '',
                        message: this.successMessage,
                        variant: 'success'
                    })
                );
                })
                .catch(error => {
                    this.dispatchEvent(
                        new ShowToastEvent({
                            title: '',
                            message: error.message,
                            variant: 'error'
                        })
                    );
                })
        }
    }

}