import { LightningElement, api, track, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { getRecord, getFieldValue } from 'lightning/uiRecordApi';
import getEmailFields from '@salesforce/apex/SurveyInvitationManagerController.getEmailFields';
import getEmailTemplates from '@salesforce/apex/SurveyInvitationManagerController.getEmailTemplates';
import getOrgWideEmails from '@salesforce/apex/SurveyInvitationManagerController.getOrgWideEmails';
import sendEmail from '@salesforce/apex/SurveyInvitationManagerController.sendEmail';
import ACCOUNT_OBJECT from '@salesforce/schema/Account';

const DEFAULT_EMAIL_FIELD = 'PersonEmail';
const DEFAULT_EMAIL_TEMPLATE = 'SurveyEmailTemplate';
const DEFAULT_COPY_ICON = 'utility:copy';
const SUCCESS_COPY_ICON = 'utility:check';
const DEFAULT_COPY_ICON_VARIANT = null;
const SUCCESS_COPY_ICON_VARIANT = 'success';
const COPY_ICON_RESET_TIMER = 1500;

export default class SurveyInvitationManager extends LightningElement {
    @api recordId;
    @api objectApiName;
    @api defaultField = DEFAULT_EMAIL_FIELD;
    @api defaultTemplate = DEFAULT_EMAIL_TEMPLATE;
    @api defaultOrgWideEmail;
    _record;
    _surveyTakerId;
    _surveyUrl;
    _sendingMail = false;
    _generatingLink = false;
    _submitted = false;
    _sent = false;
    @track _fieldChoices = [];
    @track _templateChoices = [];
    @track _orgWideEmailChoices = [];
    @track _selectedOrgWideEmail = {};
    _emailField;
    _selectedTemplate;
    _copyIcon = DEFAULT_COPY_ICON;
    _copyIconVariant = DEFAULT_COPY_ICON_VARIANT;

    get _accountId() {
        if(this.objectApiName === ACCOUNT_OBJECT.objectApiName) {
            return this.recordId;
        }
        return null;
    }
    get _generateLinkDisabled() {
        return this._generatingLink
            || this._sendingMail
            || this._submitted;
    }
    get _sendDisabled() {
        return !this._emailFieldValue
            || !this._selectedTemplate
            || this._sendingMail
            || this._generatingLink
            || this._sent;
    }
    get _emailFieldFullName() {
        return this._emailField ? `${this.objectApiName}.${this._emailField}` : null;
    }
    get _emailFieldValue() {
        return getFieldValue(this._record, this._emailFieldFullName);
    }
    @wire(getRecord, {recordId: '$recordId', fields: '$_emailFieldFullName'})
    handleGetRecord({error, data}) {
        if(error) {
            console.error(JSON.stringify(error));
        } else if(data) {
            this._record = data;
        }
    }
    @wire(getEmailFields, {recordId: '$recordId'})
    handleGetEmailFields({error, data}) {
        if(error) {
            console.error(JSON.stringify(error));
        } else if(data) {
            this._fieldChoices = data;
            this.selectDefaultField();
        }
    }
    @wire(getEmailTemplates, {})
    handleGetEmailTemplates({error, data}) {
        if(error) {
            console.error(JSON.stringify(error));
        } else if(data) {
            this._templateChoices = data;
            this.selectDefaultTemplate();
        }
    }
    @wire(getOrgWideEmails, {})
    handleGetOrgWideEmails({error, data}) {
        if(error) {
            console.error(JSON.stringify(error));
        } else if(data) {
            this._orgWideEmailChoices = data;
            this.selectDefaultOrgWideEmail();
        }
    }

    selectDefaultField() {
        const fieldName = this._fieldChoices.find(elem => elem.value === this.defaultField)?.value;
        this._emailField = fieldName;
    }

    selectDefaultTemplate() {
        const templateName = this._templateChoices.find(elem => elem.label === this.defaultTemplate)?.value;
        this._selectedTemplate = templateName;
    }

    selectDefaultOrgWideEmail() {
        const orgWideEmail = this._orgWideEmailChoices.find(elem =>
            elem.address === this.defaultOrgWideEmail || elem.label === this.defaultOrgWideEmail);
        if(orgWideEmail) {
            this._selectedOrgWideEmail = orgWideEmail;
        }
    }

    handleFieldSelected(event) {
        this._emailField = event.detail.value;
    }

    handleTemplateSelected(event) {
        this._selectedTemplate = event.detail.value;
    }

    handleOrgWideEmailSelected(event) {
        const addressId = event.detail.value;
        this._selectedOrgWideEmail = this._orgWideEmailChoices
            .find(elem => elem.value === addressId);
    }

    handleFormSuccess(event) {
        this._generatingLink = false;
        this._submitted = true;
        const surveyTakerId = event.detail.id;
        const surveyUrl = event.detail.fields?.SurveyURL__c?.value;
        const ref = this.template.querySelector("lightning-record-edit-form");
        ref.recordId = surveyTakerId;
        this._surveyTakerId = surveyTakerId;
        this._surveyUrl = surveyUrl;
        if(this._sendingMail) {
            this.callSendEmail();
        }
    }
    handleFormError() {
        this._generatingLink = false;
        this._sendingMail = false;
    }

    copyUrl() {
        const resUrl = this.template.querySelector(".result-url");
        resUrl.disabled = false;
        resUrl.select();
        const copied = document.execCommand('copy');
        resUrl.disabled = true;
        if(copied) {
            this.handleLinkCopied();
        }
    }

    handleLinkCopied() {
        this._copyIcon = SUCCESS_COPY_ICON;
        this._copyIconVariant = SUCCESS_COPY_ICON_VARIANT;
        setTimeout(() => {
            this._copyIcon = DEFAULT_COPY_ICON;
            this._copyIconVariant = DEFAULT_COPY_ICON_VARIANT;
        }, COPY_ICON_RESET_TIMER);
    }

    handleGenerateLink() {
        this._generatingLink = true;
        this.submitForm();
    }

    handleSendMail() {
        this._sendingMail = true;
        if(!this._submitted) {
            this.submitForm();
        } else {
            this.callSendEmail();
        }
    }

    submitForm() {
        const ref = this.template.querySelector('lightning-record-edit-form');
        ref.submit();
    }

    callSendEmail() {
        const surveyTakerId = this._surveyTakerId;
        const template = this._selectedTemplate;
        const orgWideAddressId = this._selectedOrgWideEmail?.value;
        const toAddress = this._emailFieldValue;
        // Apex controls blank parameters
        sendEmail({surveyTakerId, template, orgWideAddressId, toAddress})
            .then(result => {
                this._sent = true;
                this.showToast('Email successfully sent', '', 'success');
            }).catch(error => {
                this.showToast(
                    error.status,
                    error.body?.message,
                    'error'
                );
            }).finally(() => {
                this._sendingMail = false;
            });
    }

    showToast(title, message, variant) {
        this.dispatchEvent(
            new ShowToastEvent({
                title,
                message,
                variant
            })
        );
    }

}