import { LightningElement, api, wire } from 'lwc';
import getEventData from '@salesforce/apex/ALS_calendarManagement_ctrl.getEventData';
import { NavigationMixin } from 'lightning/navigation';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

const CSS_CLASS = 'modal-hidden';
const LABEL_ACCOUNT = 'Paciente';
const LABEL_SUBJECT = 'Asunto';
const LABEL_SPECIALTY = 'Especialidad';
const LABEL_ACCEPT_BUTTON = 'Mostrar detalles';
const LABEL_CANCEL_BUTTON = 'Cerrar';

export default class CalendarEventInfo extends NavigationMixin(LightningElement) {

    labelAccount = LABEL_ACCOUNT;
    labelSubject = LABEL_SUBJECT;
    labelSpecialty = LABEL_SPECIALTY;
    labelAcceptButton = LABEL_ACCEPT_BUTTON;
    labelCancelButton = LABEL_CANCEL_BUTTON;

    showModal = false;
    _eventId;
    _event;
    hasHeaderString = false;
    _headerPrivate;

    @api
    set eventId(value) {
        this._event = null;
        this._eventId = value;
    }
    get eventId() {
        return this._eventId;
    }

    get account() {
        return this._event?.Account || {};
    }

    @wire(getEventData, {recordId: '$_eventId'})
    handleGetEvent({error, data}) {
        if(error) {
            console.error(JSON.stringify(error));
            this.dispatchEvent(
                new ShowToastEvent({
                    title: `Status: ${error.status}`,
                    message: `${error.body.exceptionType}: ${error.body.message}`,
                    variant: 'error'
                })
            );
        } else if(data) {
            this._event = data;
        }
    }

    @api
    set header(value) {
        this.hasHeaderString = !!value;
        this._headerPrivate = value;
    }
    get header() {
        return this._headerPrivate;
    }

    @api show() {
        this.showModal = true;
    }

    @api hide() {
        this.showModal = false;
    }

    handleDialogClose() {
        //Let parent know that dialog is closed (mainly by that cross button) so it can set proper variables if needed
        const closedialog = new CustomEvent('closedialog');
        this.dispatchEvent(closedialog);
        this.hide();
    }

    handleSlotTaglineChange() {
        // Only needed in "show" state. If hiding, we're removing from DOM anyway
        // Added to address Issue #344 where querySelector would intermittently return null element on hide
        if (this.showModal === false) {
            return;
        }
        const taglineEl = this.template.querySelector('p');
        taglineEl.classList.remove(CSS_CLASS);
    }

    handleSlotFooterChange() {
        // Only needed in "show" state. If hiding, we're removing from DOM anyway
        // Added to address Issue #344 where querySelector would intermittently return null element on hide
        if (this.showModal === false) {
            return;
        }
        const footerEl = this.template.querySelector('footer');
        footerEl.classList.remove(CSS_CLASS);
    }

    handleGoToRecord() {
        if(!this._eventId) {
            return;
        }
        this.hide();
        let self = this;
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId: self._eventId,
                actionName: 'view'
            }
        })
        .catch(error => {
            console.error(error);
            this.dispatchEvent(
                new ShowToastEvent({
                    title: '',
                    message: error,
                    variant: 'error'
                })
            );
        });
    }
}