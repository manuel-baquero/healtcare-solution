import { LightningElement, api, wire, track } from "lwc";

import fetchLookUpValues from "@salesforce/apex/ALS_ReusableLookup_Controller.fetchLookUpValues";
import getRecord from "@salesforce/apex/ALS_ReusableLookup_Controller.getRecord";

export default class LwcReusableLookup extends LightningElement {
  @api searchKey;
  @track searchValue = "";
  @api objectName;
  @api fieldName;
  @api subtitleName;
  @api placeHolder;
  @api iconName;
  @api fields;
  @api keyField;
  @api label;
  @track selectedRecord;
  @api selectedRecordId;
  @api selectedValue;
  @track showLookupListFlag = false;
  @track options = [];
  @api recordFound = false;
  loaded = true;
  doInit = true;

  //Se ejecuta cuando le pasas el valor seleccionado al campo @api selectedValue
  renderedCallback() { 
    if(this.selectedValue){
      if (this.selectedValue?.label && this.selectedValue?.value) {
        this.selectedRecord = this.selectedValue.label;
        this.selectedRecordId = this.selectedValue.value;
        this.selectedValue = undefined;
        this.options.push({
          key: this.selectedRecordId,
          value: this.selectedRecord,
          fields: [],
          subtitle: this.subtitleName
        });
        const selectedEvent = new CustomEvent("recordchange", {
          detail: { id: this.selectedRecordId, value: this.selectedRecord }
        });
        this.dispatchEvent(selectedEvent);
        this.template.querySelector(".lookup_list").classList.add("slds-hide");
      }else if(this.doInit && (typeof this.selectedValue === 'string') || (this.selectedValue instanceof String)){
        this.doInit = false;
        getRecord({searchKey: this.selectedValue, fieldName: this.fieldName, fields: this.fields, ObjectName: this.objectName, keyField: this.keyField})
          .then((result) => {
            if (result != null && result.length > 0) {
              this.recordFound = true;
              this.options = this.parseInfo(result);
              this.loaded = true;
  
              this.selectedRecord = this.options[0].value;
              this.selectedRecordId = this.options[0].key;
              const selectedEvent = new CustomEvent("recordchange", {
                detail: {
                  id: this.selectedRecordId,
                  name: this.selectedRecord,
                  fields: this.options[0].fields
                }
              });
  
              this.dispatchEvent(selectedEvent);
              this.options = [];
            }else{
              //Id no encontrado
            }          
          })
          .catch((error) => {
            this.error = error;
            console.error(error);    
            const selectedEvent = new CustomEvent("customerror", { detail: { message: 'Se ha producido un error. '+ error.body.message}});
            this.dispatchEvent(selectedEvent);       
          });
      }
    }  
  }

  handleClick() {
    console.log('handleClick');
    if (!this.showLookupListFlag) {
      this.showLookupListFlag = true;
      this.template.querySelector(".lookup_list").classList.remove("slds-hide");
      this.template
        .querySelector(".slds-searchIcon")
        .classList.add("slds-hide");
      this.template
        .querySelector(".slds-icon-utility-down")
        .classList.remove("slds-hide");
    }
    this.template
      .querySelector(".slds-dropdown-trigger")
      .classList.add("slds-is-open");
  }

  handleKeyUp(event) {
    console.log('handleKeyUp');
    this.searchValue = event.target.value;

    if (this.searchValue === "") {
      this.template.querySelector(".lookup_list").classList.add("slds-hide");
      this.showLookupListFlag = false;
    } else {
      this.showLookupListFlag = true;
      this.template.querySelector(".lookup_list").classList.remove("slds-hide");
      this.template
        .querySelector(".slds-dropdown-trigger")
        .classList.add("slds-is-open");
      this.showLookupListFlag = true;
      if (this.searchValue.length >= 3 && this.loaded == true) {
        this.loaded = false;
        setTimeout(() => {
          fetchLookUpValues({
            searchKey: this.searchValue,
            fieldName: this.fieldName,
            fields: this.fields,
            ObjectName: this.objectName,
            keyField: this.keyField
          })
            .then((result) => {
              let picklistOptions = [];
              if (result != null && result.length > 0) {
                this.recordFound = true;
                picklistOptions = this.parseInfo(result);
              } else {
                this.recordFound = false;
                picklistOptions.push({
                  key: "Sin resultados",
                  value: "No se han encontrado resultados"
                });
              }

              this.options = picklistOptions;
              this.loaded = true;
            })
            .catch((error) => {
              this.error = error;
              console.error(error);
            });
        }, 1000);
      } else if (this.searchValue.length < 3) {
        this.options = [];
        let picklistOptions = [];
        picklistOptions.push({
          key: "Sin resultados",
          value: "No se han encontrado resultados"
        });
        this.options = picklistOptions;
      }
    }
  }

  handleOptionSelect(event) {
    console.log('handleOptionSelect');
    var fields;
    for (var i = 0; i < this.options.length; i++) {
      var option = this.options[i];
      if (option.key === event.currentTarget.dataset.id) {
        fields = option.fields;
      }
    }

    this.selectedRecord = event.currentTarget.dataset.name;
    this.selectedRecordId = event.currentTarget.dataset.id;
    const selectedEvent = new CustomEvent("recordchange", {
      detail: {
        id: this.selectedRecordId,
        name: event.currentTarget.dataset.name,
        fields: fields
      }
    });
    this.dispatchEvent(selectedEvent);
    this.template.querySelector(".lookup_list").classList.add("slds-hide");
    this.searchKey = "";
    this.searchValue = "";
    this.options = [];
  }

  parseInfo(result){
    console.log('parseInfo');
    let picklistOptions = [];
    result.forEach((key) => {
      var value = key[this.fieldName];
      if (this.fieldName.includes(".")) {
        value = key[this.fieldName.split(".")[0]];
        for (var i = 1; i < this.fieldName.split(".").length; i++) {
          value = value[this.fieldName.split(".")[i]];
        }
      }

      var fieldsList = [];
      if (this.fields) {
        if (this.fields.includes(",")) {
          this.fields = this.fields.replace(" ", "");
          for (var i = 0; i < this.fields.split(",").length; i++) {
            var aux = {
              key: this.fields.split(",")[i],
              value: key[this.fields.split(",")[i]]
            };
            /*if(this.fields.split(',')[i].includes('.')){                        
              for(var j = 1; j < this.fields.split(',')[i].split('.').length; j++){
                aux = aux[this.fields.split(',')[i].split('.')[j]];
              }
            }*/
            fieldsList.push(aux);
          }
        } else {
          var aux = {
            key: this.fields,
            value: key[this.fields]
          };
          /*if (this.fields.includes(".")) {
            for (var j = 1; j < this.fields.split(".").length; j++) {
              aux = aux[this.fields.split(".")[j]];
            }
          }*/
          fieldsList.push(aux);
        }
      }

      picklistOptions.push({
        key: key[this.keyField],
        value: value,
        fields: fieldsList,
        subtitle: key[this.subtitleName]
      });
    });

    return picklistOptions;
  }

  @api
  recordChange(record){
    console.log('recordChange');
    this.selectedRecord = record.name;
    this.selectedRecordId = record.id;
  }
}