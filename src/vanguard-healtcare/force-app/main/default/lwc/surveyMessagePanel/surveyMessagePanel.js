import { LightningElement, api } from 'lwc';

const DEFAULT_ERROR_MESSAGE = 'An unexpected error occurred';
export default class SurveyMessagePanel extends LightningElement {
    @api sendSuccess;
    @api alreadyAnswered;
    @api errorMessage;
    @api brandColor;

    get _errorMessage() {
        return this.errorMessage || DEFAULT_ERROR_MESSAGE;
    }

    @api showConfirmation = false;
    @api showInformation = false;
    @api showError = false;
}