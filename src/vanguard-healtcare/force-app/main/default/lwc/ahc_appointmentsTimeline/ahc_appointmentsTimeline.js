import { LightningElement, api } from 'lwc';
import DynamicIcons from '@salesforce/resourceUrl/dynamicIcons';
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import getAppointmentByDate from "@salesforce/apex/AHC_AppointmentsTimeline_Controller.getAppointmentsByDate";
import getNext from '@salesforce/apex/AHC_AppointmentsTimeline_Controller.getNext';
import getPrevious from '@salesforce/apex/AHC_AppointmentsTimeline_Controller.getPrevious';
import TotalRecords from '@salesforce/apex/AHC_AppointmentsTimeline_Controller.TotalRecords';

const today = new Date();
const date = ("0" + today.getDate()).slice(-2) + "/" + ("0" + (today.getMonth() + 1)).slice(-2) + "/" + today.getFullYear();
const queryDate = today.getFullYear() + "-" + ("0" + (today.getMonth() + 1)).slice(-2) + "-" + ("0" + today.getDate()).slice(-2);

export default class Ahc_appointmentsTimeline extends LightningElement {
    
    myGIF = DynamicIcons + '/clock.gif';

    @api date = date;
    @api scheduledAppointments;
    @api loaded = false;
    @api showAppointments = false;
    @api v_Offset=0;
    @api v_TotalRecords;
    @api page_size = 5;
    @api totalPages = 1;
    @api recordsSize = 0;
    @api page = 1;
    @api disableNext = false;
    @api disablePrev = false;
    @api disableLast = false;
    @api disableFirst = false;
    @api showFilters = false;
    @api selectedFilter;
    @api selectedDateLabel = 'HOY';

    get options(){
        return [
            { label: 'HOY', value: 0 },
            { label: 'MAÑANA', value: 1 }
        ];
    }
    connectedCallback(){
        TotalRecords({selectedDate: queryDate})
        .then(result=>{
            this.v_TotalRecords = result;
            this.totalPages = Math.ceil(result / this.page_size);
            this.disablePrev = true;
            this.disableFirst = true;
            this.selectedFilter = 0;
        });
        this.getAppointments(0,this.v_Offset,this.page_size);
    }

    getAppointments(filter, offset, pagesize) {
        this.loaded = false;
        getAppointmentByDate({ selectedDate: filter, v_Offset: offset, v_pagesize: pagesize})
        .then(result => {
            this.loaded = true;
            if (result.length > 0 ) {
                this.recordsSize = result.length;
                if(this.recordsSize >= this.v_TotalRecords){
                    this.disableNext = true;
                    this.disableLast = true;
                }
                this.showAppointments = true;
                this.scheduledAppointments = result;
            } else{
                this.showAppointments = false;
            }
        })
        .catch(error => {
            this.showAppointments = false;
            this.loaded = true;
            console.log(error);
        });
    }

    previousHandler2(){
        getPrevious({v_Offset: this.v_Offset, v_pagesize: this.page_size})
        .then(result=>{
            this.v_Offset = result;
            this.page = this.page - 1;
            if(this.v_Offset === 0){
                this.disableFirst = true;
                this.disablePrev = true;
                this.disableNext = false;
                this.disableLast = false;
            }else{
                this.disableFirst = false;
                this.disablePrev = false;
            }
            this.getAppointments(this.selectedFilter, result, this.page_size);
        });
        
    }

    nextHandler2(){
        getNext({v_Offset: this.v_Offset, v_pagesize: this.page_size})
        .then(result => {
            this.v_Offset = result;
            this.page = this.page + 1;
           if(result + this.page_size > this.v_TotalRecords){
                this.disableNext = true;
                this.disableLast = true;
                this.disableFirst = false;
                this.disablePrev = false;
            }else{
                this.disableNext = false;
                this.disableLast = false;
            }
            this.getAppointments(this.selectedFilter, result, this.page_size);
        });
        
    }
    
    changeHandler2(event){
        const det = event.detail;
        this.page_size = det;
        this.totalPages = Math.ceil(this.v_TotalRecords / this.page_size);
        this.getAppointments(this.selectedFilter, this.v_Offset, this.page_size);
    }

    firstpagehandler(){
        this.v_Offset = 0;
        this.disableFirst = true;
        this.disablePrev = true;
        this.disableNext = false;
        this.disableLast = false;
        this.page = 1;
        this.getAppointments(this.selectedFilter, 0, this.page_size);
    }

    lastpagehandler(){
        this.v_Offset = this.v_TotalRecords - (this.v_TotalRecords)%(this.page_size);
        this.disableNext = true;
        this.disableLast = true;
        this.disableFirst = false;
        this.disablePrev = false;
        this.page = this.totalPages;
        this.getAppointments(this.selectedFilter, this.v_Offset, this.page_size);
    }

    refreshAppointments(){
        this.v_Offset = 0;
        this.v_pagesize = 5;
        this.disableFirst = true;
        this.disablePrev = true;
        this.disableNext = false;
        this.disableLast = false;
        this.getAppointments(this.selectedFilter, this.v_Offset, this.page_size);
    }

    showFiltersPanel (){
        this.showFilters = !this.showFilters;
    }

    handleChange(event) {
        const selectedOption = event.detail.value;
        this.selectedFilter = selectedOption;
        if(this.selectedFilter == 0){
            this.selectedDateLabel = 'HOY';
            const today = new Date();
            this.date = ("0" + today.getDate()).slice(-2) + "/" + ("0" + (today.getMonth() + 1)).slice(-2) + "/" + today.getFullYear();
        }else if(this.selectedFilter == 1){
            this.selectedDateLabel = 'MAÑANA';
            const today = new Date();
            const tomorrow = new Date();
            tomorrow.setDate(today.getDate() + 1);
            this.date = ("0" + tomorrow.getDate()).slice(-2) + "/" + ("0" + (tomorrow.getMonth() + 1)).slice(-2) + "/" + tomorrow.getFullYear();
        }
        this.showFilters = !this.showFilters;
        this.getAppointments(this.selectedFilter, this.v_Offset, this.page_size);
    }
}