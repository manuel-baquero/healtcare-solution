import { LightningElement, api } from 'lwc';

export default class LwcHomePageHeader extends LightningElement {
    @api panelTitle;
    @api panelSubtitle;
    @api dynamicIconType;
    @api iconName;
}