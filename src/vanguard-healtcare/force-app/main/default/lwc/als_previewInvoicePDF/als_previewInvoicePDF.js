import { LightningElement, api, wire } from 'lwc';
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import { getRecord, getFieldValue } from 'lightning/uiRecordApi';
import { CloseActionScreenEvent } from 'lightning/actions';
import sendQuotationWithAttachedPDF from '@salesforce/apex/PreviewPresupuestoPDF_Ctrl.sendQuotationWithAttachedPDF';

const PATIENT_FIELD = 'Invoice__c.Patient__c';
const PATIENT_PCID_FIELD = 'Invoice__c.Patient__r.PersonContactId';
const PATIENT_BCTRY_FIELD = 'Invoice__c.Patient__r.BillingCountry';
const PATIENT_BSTAT_FIELD = 'Invoice__c.Patient__r.BillingState';
const PATIENT_BCTY_FIELD = 'Invoice__c.Patient__r.BillingCity';
const PATIENT_BPC_FIELD = 'Invoice__c.Patient__r.BillingPostalCode';
const PATIENT_BSTR_FIELD = 'Invoice__c.Patient__r.BillingStreet';
const PATIENT_IDCARD_FIELD = 'Invoice__c.Patient__r.IDCard__c';
const RECORD_FIELDS = [
    PATIENT_FIELD,
    PATIENT_PCID_FIELD,
    PATIENT_BCTRY_FIELD,
    PATIENT_BSTAT_FIELD,
    PATIENT_BCTY_FIELD,
    PATIENT_BPC_FIELD,
    PATIENT_BSTR_FIELD,
    PATIENT_IDCARD_FIELD
];
const DEFAULT_VF = 'FacturaPDF_Vfp';

export default class Als_previewInvoicePDF extends LightningElement {

    @api recordId;
    _record = {};
    _vfName = DEFAULT_VF;
    _isProcessing = false;
    _isValidated;

    get wasValidated() {
        return this._isValidated != null;
    }
    get vfAddress() {
        if(this._vfName) {
            return '/apex/' + this._vfName + '?Id=' + this.recordId;
        }
        return null;
    }

    @wire(getRecord, {recordId: '$recordId', fields: RECORD_FIELDS})
    handleGetRecord({error, data}) {
        if(error) {
            this.showToast('', 'Ha habido un error al traer los campos del registro', 'error');
        } else if(data) {
            this._record = data;
            this.validateRecord();
        }
    }
    
    validateRecord() {
        const record = this._record;
        if (getFieldValue(record, PATIENT_FIELD)
                && getFieldValue(record, PATIENT_BCTRY_FIELD)
                && getFieldValue(record, PATIENT_BSTAT_FIELD)
                && getFieldValue(record, PATIENT_BCTY_FIELD)
                && getFieldValue(record, PATIENT_BPC_FIELD)
                && getFieldValue(record, PATIENT_BSTR_FIELD)
                && getFieldValue(record, PATIENT_IDCARD_FIELD)) {
            this._isValidated = true;
        } else {
            this._isValidated = false;
        }
    }

    saveQuotation() {
        this.processQuotation(false);
    }

    sendQuotation() {
        this.processQuotation(true);
    }

    getPersonContactId() {
        return getFieldValue(this._record, PATIENT_PCID_FIELD);
    }

    processQuotation(getContactId) {
        this._isProcessing = true;
        const recordId = this.recordId;
        const toWhoId = getContactId ? this.getPersonContactId() : null;

        sendQuotationWithAttachedPDF({recordId, toWhoId})
            .then(result => {
                this.showToast('', `Archivo guardado${toWhoId ? ' y enviado ' : ' '}con éxito`, 'success');
                this.refreshTab();
                this.closeQuickAction();
            })
            .catch(error => {
                console.error(JSON.stringify(error, null, 2));
                this.showToast('', `No se ha podido guardar${toWhoId ? ' o enviar ' : ' '}el archivo`, 'error');
            })
            .finally(() => {
                this._isProcessing = false;
            });
    }
    showToast(title, message, variant) {
        this.dispatchEvent(
            new ShowToastEvent({
                title,
                message,
                variant
            })
        );
    }

    closeQuickAction() {
        this.dispatchEvent(new CloseActionScreenEvent());
    }

    refreshTab() {
        eval("$A.get('e.force:refreshView').fire();");
    }

}