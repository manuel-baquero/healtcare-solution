import { LightningElement, api, wire } from 'lwc';
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import { getRecord, getFieldValue } from 'lightning/uiRecordApi';
import { CloseActionScreenEvent } from 'lightning/actions';
import sendQuotationWithAttachedPDF from '@salesforce/apex/PreviewPresupuestoPDF_Ctrl.sendQuotationWithAttachedPDF';

const OPP_ACC_PERSONID_FIELD = 'Opportunity.Account.PersonContactId';
const RECORD_FIELDS = [OPP_ACC_PERSONID_FIELD];
const DEFAULT_VF = 'PresupuestoPDF_Vfp';

export default class Als_previewPresupuestoPDF extends LightningElement {

    @api recordId;
    _record = {};
    _vfName = DEFAULT_VF;
    _isProcessing = false;

    get vfAddress() {
        if(this._vfName) {
            return '/apex/' + this._vfName + '?Id=' + this.recordId;
        }
        return null;
    }

    @wire(getRecord, {recordId: '$recordId', fields: RECORD_FIELDS})
    handleGetRecord({error, data}) {
        if(error) {
            this.showToast('', 'Ha habido un error al traer los campos del registro', 'error');
        } else if(data) {
            this._record = data;
        }
    }

    saveQuotation() {
        this.processQuotation(false);
    }

    sendQuotation() {
        this.processQuotation(true);
    }

    getPersonContactId() {
        return getFieldValue(this._record, OPP_ACC_PERSONID_FIELD);
    }

    processQuotation(getContactId) {
        this._isProcessing = true;
        const recordId = this.recordId;
        const toWhoId = getContactId ? this.getPersonContactId() : null;

        sendQuotationWithAttachedPDF({recordId, toWhoId})
            .then(result => {
                this.showToast('', `Presupuesto guardado${toWhoId ? ' y enviado' : ''}`, 'success');
                this.refreshTab();
                this.closeQuickAction();
            })
            .catch(error => {
                console.error(JSON.stringify(error, null, 2));
                this.showToast('', `No se ha podido guardar${toWhoId ? ' o enviar ' : ' '}el archivo`, 'error');
            })
            .finally(() => {
                this._isProcessing = false;
            });
    }
    showToast(title, message, variant) {
        this.dispatchEvent(
            new ShowToastEvent({
                title,
                message,
                variant
            })
        );
    }

    closeQuickAction() {
        this.dispatchEvent(new CloseActionScreenEvent());
    }

    refreshTab() {
        eval("$A.get('e.force:refreshView').fire();");
    }

}