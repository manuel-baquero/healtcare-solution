import { LightningElement, api, track, wire } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';

export default class Ahc_homeActionsPage extends NavigationMixin(LightningElement) {

    @api myRecordId;
    @api title;

    navigateAgenda() {
        this[NavigationMixin.Navigate]({
            type: 'standard__navItemPage',
            attributes: {
                apiName: 'Calendar_Management',
            },
        });
    }

    openModal(){
        this.showModal=true;
    }

    closeModal(){
        this.showModal=false;
    }

    navigateForm() {
        this[NavigationMixin.Navigate]({
            type : 'standard__webPage',
            attributes: {
                url : 'https://webinardev-sdodemo-main-166ce2cf6b6-172-17a3ec45739.cs192.force.com/leadForm'
            }
        });
    }

    createAccount() {
        this[NavigationMixin.Navigate]({
            type: 'standard__objectPage',
            attributes: {
                objectApiName: 'Account',
                actionName: 'new'
            }
        });
    }
}