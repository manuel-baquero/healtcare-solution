import { LightningElement, api } from 'lwc';
import getProfilePhoto from '@salesforce/apex/ProfilePhotoUploader_Controller.getProfilePhotoURL';
import { updateRecord } from 'lightning/uiRecordApi';
import PROFILE_PICTURE_FIELD from '@salesforce/schema/ContentVersion.ProfilePicture__c';
import ID_FIELD from '@salesforce/schema/ContentVersion.Id';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class LwcProfilePhotoUploader extends LightningElement {
    @api panelTitle;
    @api uploadLabel;
    @api size;
    @api isRoundCorner;
    @api recordId;
    @api photoUrl;
    @api successMessage;
    @api defaultPhoto;
    loaded = false;
    newPhoto = true;

    get acceptedFormats() {
        return ['.jpg', '.png', '.jpeg'];
    }

    get cssClass() {
        if (this.isRoundCorner) {
            return 'roundImg';
        } else {
            return 'squareImg';
        }
    }

    connectedCallback() {
        this.setUpPhoto();
    }

    setUpPhoto() {
        getProfilePhoto({ recordId: this.recordId })
            .then(result => {
                if (result !== '') {
                    this.photoUrl = result;
                    this.loaded = !this.loaded;
                    this.newPhoto = !this.newPhoto;
                } else {
                    this.resetPhoto();
                }
            })
            .catch(error => {
                this.resetPhoto();
            });
    }


    handleUploadFinished(event) {
        const uploadedFiles = event.detail.files;
        const fields = {};
        fields[ID_FIELD.fieldApiName] = uploadedFiles[0].contentVersionId;
        fields[PROFILE_PICTURE_FIELD.fieldApiName] = true;
        const recordInput = { fields };

        updateRecord(recordInput)
            .then(() => {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: '',
                        message: this.successMessage,
                        variant: 'success'
                    })
                );
                this.photoUrl = '/sfc/servlet.shepherd/version/download/' + uploadedFiles[0].contentVersionId;
                this.newPhoto = !this.newPhoto;
            })
            .catch(error => {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: '',
                        message: error.message,
                        variant: 'error'
                    })
                );
            });
    }

    showNewPhotoUploader() {
        this.newPhoto = !this.newPhoto
    }

    resetPhoto() {
        this.photoUrl = 'https://www.lightningdesignsystem.com/assets/images/avatar2.jpg';
        this.loaded = !this.loaded;
    }
}