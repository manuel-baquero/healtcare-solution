import { LightningElement, track, api, wire } from "lwc";
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import { loadScript, loadStyle } from "lightning/platformResourceLoader";
import { NavigationMixin } from "lightning/navigation";
import FullCalendarJS from "@salesforce/resourceUrl/FullCalendarJs";
import getAppointmentByDate from "@salesforce/apex/ALS_calendarManagement_ctrl.getAppointmentByDate";
import getMainAgenda from "@salesforce/apex/ALS_calendarManagement_ctrl.getMainAgenda";
// import getOtherAgendas from "@salesforce/apex/ALS_calendarManagement_ctrl.getOtherAgendas";
import getCalendarConfiguration from "@salesforce/apex/ALS_calendarManagement_ctrl.getCalendarConfiguration";
import updateCita from "@salesforce/apex/ALS_calendarManagement_ctrl.updateCita";
import ALS_LOGO from '@salesforce/resourceUrl/ALS_LOGO';

const today = new Date();
const date = today.getFullYear() + "-" + ("0" + (today.getMonth() + 1)).slice(-2) + "-" + ("0" + today.getDate()).slice(-2);
const heightWindow = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;

const CALENDAR_FILTERS = 'Filtros del calendario';
const LEGEND = 'Leyenda';
const HEADER_EVENT_INFO = 'Información de la cita';
const HEADER_MODIFY_EVENT = 'Modificación de cita existente';
const BODY_MODIFY_EVENT = '¿Está seguro que desea modificar la cita a la nueva fecha y hora seleccionadas?';
const ACCEPT_BUTTON_LABEL = 'Confirmar';
const CANCEL_BUTTON_LABEL = 'Cancelar';

const CLONE_EVENTS = 'Clonar eventos';
const IS_ACTIVE = 'Activo';
const IS_INACTIVE = 'Inactivo';
const DEFAULT_COLOR_EVENT_BACKGROUND = '#AAAAAA';
const DEFAULT_COLOR_EVENT_TEXT = '#000000';
const DEFAULT_COLOR_BUSINESS_HOURS = 'rgb(20 150 200 / 70%)';
// const ERROR = 'Error';
const ERROR_EVENTS_LOAD = 'Error cargando citas';
const ERROR_EVENTS_WRONG_FILTER = 'Error cargando citas. El filtro para seleccionar las agendas principales no es correcto.';
const ERROR_CALENDAR_INIT = 'Error al inicializar el calendario';
const ERROR_COMPONENT_INIT = 'Error al inicializar el componente';
const ERROR_LIBRARY_LOAD = 'Error al cargar la libreria';
const ERROR_FULLCALENDAR_LOAD = 'Error cargando FullCalendar js';
const ERROR_CONTACT_ADMIN = 'Ha ocurrido un error. Si el error persiste, contacta con un administrador';
const ERROR_ASSOCIATED_EVENT_EDIT = 'No puedes modificar la duración de un evento asociado. Modifica el evento principal'; // "You can't modify the duration of an associated event. Modify the main event.";
// const SUCCESS = 'Éxito';
const SUCCESS_EVENT_CLONE = 'Cita clonada con éxito'; // 'Successfully cloned appointment';
const SUCCESS_EVENT_EDIT = 'Cita modificada con éxito';
const COMPARATORS = {
  EQ: 'Equal',
  NE: 'Not Equal',
  GT: 'Greater than',
  LT: 'Less than',
  GE: 'Equal or greater than',
  LE: 'Equal or less than'
};

export default class Als_calendarByAgenda extends NavigationMixin(LightningElement) {

  // Markup constants
  calendarFilters = CALENDAR_FILTERS;
  legend = LEGEND;
  headerEventInfo = HEADER_EVENT_INFO;
  headerModifyEvent = HEADER_MODIFY_EVENT;
  bodyModifyEvent = BODY_MODIFY_EVENT;
  acceptButtonLabel = ACCEPT_BUTTON_LABEL;
  cancelButtonLabel = CANCEL_BUTTON_LABEL;

  showFilters = true;

  clickedEventId;
  infoData;
  columnLimit = 3;
  @api calendar; //ALS_CalendarConfig__c Contains the calendar info (labels, fields...)
  @track agendaCalendar = {};
  @track agendaEvents = [];
  @track agendaList = [];
  @track chartLegendList = []; //Legend name list
  fields = []; //Fields to display in event modal.
  @track selectables = [];
  resourcesList = [];

  title;
  savebuttonoption = true;
  agendaCalInit = false;
  // NEW:INTXI
  _selectedAgendas; // Listado de las agendas seleccionadas
  disableLegendSection = false;  // Enable/Disble legend section
  showLegend = true; // Boolean for open or minimize legend values
  hideSaveButton = true;  
  isCloneActivated = false;
  pluginLoaded;
  runOnce;
  selectedHour = date;
  endDate = date;
  calendarType;
  calendarPluginType; //For load plugins. Grid/ Timeline
  legendSize = 5; // Used to calculate the number of items per row
  retryCont = 0; //Sometimes the calendar need 2 or 3 retrys to get well the plugins
  logo = ALS_LOGO;
  userSelectedRow; // Agenda a la que se le quiere crear un evento.
  resourceAreaWidth;
  resourceLabelText;
  createEvent = false;
  selectedDate = date;

  //Variables de configuración globales
  filter; // Where section in Primary Agenda field
  slotDuration; //Calendar time slot
  minTime; //Calendar start hour
  maxTime; //Calendar end hour
  numberLegendPerRow;
  disableDay = false;
  disableWeek = false;
  disableMonth = false;
  weekends = false;
  brandingColor = '#001c47'; //Color del branding para utilizarlo dentro del componente.

  // NEW:INTXI
  get selectedAgendas() {
    if(this._selectedAgendas && this._selectedAgendas.length) {
      return this._selectedAgendas;
    } else {
      return this.agendaList;
    }
  }
  set selectedAgendas(value) {
    this._selectedAgendas = value;
  }

  get showLegendSection() {
    return !this.disableLegendSection && this.chartLegendList?.length;
  }
  get filterSize() {
    return this.showLegendSection ? 8 : 12;
  }
  get legendSize() {
    return this.showLegendSection ? 4 : 0;
  }
  get legendIcon() {
    return `utility:${this.showLegend ? 'chevrondown' : 'chevronright'}`;
  }
  get filterIcon() {
    return `utility:${this.showFilters ? 'chevrondown' : 'chevronright'}`;
  }
  get cloneIcon() {
    return `action:${this.isCloneActivated ? 'close' : 'clone'}`;
  }
  get cloneTitle() {
    return `${CLONE_EVENTS} (${this.isCloneActivated ? IS_ACTIVE : IS_INACTIVE})`;
  }
  toggleShowFilters() {
    this.showFilters = !this.showFilters;
  }

  //CONFIGURACIÓN DEL CALENDARIO

  @wire(getCalendarConfiguration, { calendar: '$calendar'})
  wiredGetCalendarConfiguration({ error, data }) {
    //console.log('getCalendarConfiguration >> ' + JSON.stringify(data));
    if (data) {
      //Generic filters
      if(data.Filter) this.filter = data.Filter;
      if(data.SlotDuration) this.slotDuration = data.SlotDuration;
      if(data.StartHour) this.minTime = data.StartHour;
      if(data.EndHour) this.maxTime = data.EndHour;
      if(data.NumberLegendItems) this.numberLegendPerRow = data.NumberLegendItems;
      else this.numberLegendPerRow = 4;
      if(data.DisableDayView) this.disableDay = data.DisableDayView;
      if(data.DisableWeekView) this.disableWeek = data.DisableWeekView;
      if(data.DisableMonthView) this.disableMonth = data.DisableMonthView;
      if(data.ShowWeekends) this.weekends = data.ShowWeekends;
      if(data.Type) this.calendarPluginType = data.Type;
      if(data.ResourceAreaWidth) this.resourceAreaWidth = data.ResourceAreaWidth;
      if(data.SnapDuration) this.snapDuration = data.SnapDuration;
      if(data.ResourceLabelText) this.resourceLabelText = data.ResourceLabelText;
      if(data.BrandingColor) this.brandingColor = data.BrandingColor;
      // NEW:INTXI
      if(data.Picklists) this.selectables = JSON.parse(data.Picklists);

      //Legends
      if(data.Legends){
        for (const legend of data.Legends) {
          var option = {
            id : legend.Label.replaceAll(' ', '_'),
            label : legend.Label,
            color : legend.Color,
            textColor : legend.TextColor,
            conditions : legend.Conditions
          }
          this.chartLegendList.push(option);
        }
        if(this.chartLegendList.length > 0) this.disableLegendSection = false;
        if(this.numberLegendPerRow) this.legendSize = 12 / this.numberLegendPerRow;
      }

      //Fields
      if(data.Fields){
        for(const field of data.Fields){    
          var fieldClass = {
            id : field.SfId,
            type : field.Type,
            apiName : field.ApiName,
            fieldName : field.FieldName,
            iconName : field.IconName,
            keyField : field.KeyField,
            label : field.Label,
            objectName : field.ObjectName,
            order : field.Order,
            placeholder : field.Placeholder,
            required : field.Required,
            size : field.Size
          }

          //Mostramos los campos en la sección correcta
          switch (field.Type) {
            case 'Picklist':
              fieldClass.showPickList = true;
              fieldClass.disabled = true;
              break;
            case 'MultiPicklist':
              fieldClass.showMultiPickList = true;
              break;
            case 'LookUp':
              fieldClass.showLookUp = true;
              break;
            case 'TextArea':
              fieldClass.showTextArea = true;
              break;
            default:
              fieldClass.showText = true;
          }
          
          if(field.Options){
            fieldClass.disabled = false;
            var optionList = [];
            for(const fieldOption of field.Options){
              var option = {
                order : fieldOption.Order
              }

              if(field.Type === 'MultiPicklist'){
                option.key = fieldOption.Value;
                option.value = fieldOption.Label;
              }else{
                option.value = fieldOption.Value;
                option.label = fieldOption.Label;
              }
  
              optionList.push(option);
            }
            if(optionList.length == 1){
                fieldClass.value = optionList[0].value;
            }
            fieldClass.options = optionList; 
          }
          
          this.fields.push(fieldClass);
        }
      }

      //Filter
      getMainAgenda({ filter: this.filter })
      .then((result) => {
        const agendaList = [];
        for (const item of result) {
          var businessHourList = [];
          if (item.businessHour) {
            for (const bh of item.businessHour) {
              const aux = {
                startTime: bh.startTime,
                endTime: bh.endTime,
                startDate: bh.startDate,
                endDate: bh.endDate,
                days: bh.days
              };
              businessHourList = [...businessHourList, aux];
            }
          }
          const option = {
            value: item.Name,
            key: item.Id,
            owner: item.Owner,
            businessHour: businessHourList,
            selected: false
          };
          // this.agendaList = [...this.agendaList, option];
          agendaList.push(option);
        }
        this.agendaList = agendaList;
        this.selectedAgendas = [];
      })
      .catch((error) => {
        console.error(error);
        this.dispatchEvent(
          new ShowToastEvent({
            title: ERROR_EVENTS_WRONG_FILTER,
            message: error,
            variant: 'error'
          })
        );
      });

      this.dataInitialized = true;
      this.loadCalendarPlugins();
      
    }else if (error) {
      console.error(error);
    }
  }

  renderedCallback() {
    //Print Legend color
    if(this.showLegend){
      try{
        for (var legend of this.chartLegendList) {
          var element = this.template.querySelector("lightning-badge."+ legend.id);
          if(element){
            element.style.background = legend.color;
            element.style.color = legend.textColor;
          }
        }
      }catch(error){
        console.error(error);
      }
    } 

    // Color del Branding de crear eventos
    if(this.template.querySelector("div.eventTitle")){
      this.template.querySelector("div.eventTitle").style.background = this.brandingColor; 
    }

    //Pinta el calendario despues de actualizarse los plugins
    if(!this.runOnce && this.pluginLoaded){
      this.showSpinner();
      this.initializeFullCalendarByAgenda();
      this.runOnce = true;
    }
  }

  loadCalendarPlugins(){
    if(this.calendarPluginType == 'Grid'){
      Promise.all([
        loadStyle(this, FullCalendarJS + "/packages/core/main.css"),
        loadScript(this, FullCalendarJS + "/packages/core/main.js"),
        loadScript(this, FullCalendarJS + "/packages/core/locales/es.js")
      ])
        .then(() => {
          // Second step: Load the plugins in a new promise
          Promise.all([
              loadStyle(this, FullCalendarJS + "/packages/daygrid/main.css"),
              loadScript(this, FullCalendarJS + "/packages/daygrid/main.js"),
              loadScript(this, FullCalendarJS + "/packages/interaction/main.js"),
              loadScript(this, FullCalendarJS + "/packages/list/main.js"),
              loadScript(this, FullCalendarJS + "/packages-premium/resource-common/main.js")
          ])
            .then(() => {
                loadStyle(this, FullCalendarJS + "/packages/timegrid/main.css"),
                loadScript(this, FullCalendarJS + "/packages/timegrid/main.js"),
                loadScript(this, FullCalendarJS + "/packages-premium/resource-daygrid/main.js")
                .then(() => {
                  Promise.all([loadScript(this, FullCalendarJS + "/packages-premium/resource-timegrid/main.js")])
                    .then(() => {
                      //Fourth step: calls your calendar builder once the plugins have been also loaded
                      this.pluginLoaded = true;
                    })
                    .catch((error) => {
                      if (this.retryCont < 3){
                        this.retryCont++;
                        this.loadCalendarPlugins();                   
                      }else{
                        console.error(error);
                        this.dispatchEvent(
                          new ShowToastEvent({
                            title: "ERROR",
                            message: ERROR_CALENDAR_INIT,
                            variant: "error"
                          })
                        );
                      }                    
                    });
                })
                .catch((error) => {
                  if (this.retryCont < 3){
                    this.retryCont++;
                    this.loadCalendarPlugins();
                  }else{
                    console.error(`${ERROR_LIBRARY_LOAD}: ${error}`);
                    this.dispatchEvent(
                      new ShowToastEvent({
                        title: ERROR_FULLCALENDAR_LOAD,
                        message: JSON.stringify(error),
                        variant: "error"
                      })
                    );
                  }                     
                });
            })
            .catch((error) => {
              if (this.retryCont < 3){
                this.retryCont++;
                this.loadCalendarPlugins();
              }else{
                console.error(`${ERROR_LIBRARY_LOAD}: ${error}`);
                this.dispatchEvent(
                  new ShowToastEvent({
                    title: ERROR_FULLCALENDAR_LOAD,
                    message: JSON.stringify(error),
                    variant: "error"
                  })
                );
              }                   
            });
        })
        .catch((error) => {
          if (this.retryCont < 3){
            this.retryCont++;
            this.loadCalendarPlugins();
          }else{
            console.error(`${ERROR_LIBRARY_LOAD}: ${error}`);
            this.dispatchEvent(
              new ShowToastEvent({
                title: ERROR_FULLCALENDAR_LOAD,
                message: JSON.stringify(error),
                variant: "error"
              })
            );
          } 
        });  
    }else{
      Promise.all([
        loadStyle(this, FullCalendarJS + '/packages/core/main.css'),
        loadScript(this, FullCalendarJS + '/packages/core/main.js')
      ])
        .then(() => {
          console.log('Entro a pintar el componente : ');
          Promise.all([
            loadStyle(this, FullCalendarJS + '/packages/daygrid/main.css'),
            loadScript(this, FullCalendarJS + '/packages/daygrid/main.js'),
            loadScript(this, FullCalendarJS + '/packages/interaction/main.js'),
            loadScript(this, FullCalendarJS + '/packages/list/main.js'),
            loadStyle(this, FullCalendarJS + '/packages/timegrid/main.css'),
            loadScript(this, FullCalendarJS + '/packages/timegrid/main.js'),
            loadStyle(this, FullCalendarJS + '/packages-premium/timeline/main.css'),
            loadScript(this, FullCalendarJS + '/packages-premium/timeline/main.js'),
            loadScript(this, FullCalendarJS + '/packages/core/locales/es.js'),
            loadStyle(this, FullCalendarJS + '/packages/bootstrap/main.css'),
            loadScript(this, FullCalendarJS + '/packages/bootstrap/main.js'),
            loadScript(this, FullCalendarJS + '/packages-premium/resource-common/main.js')
          ]).then(() => {
            Promise.all([
              loadScript(this, FullCalendarJS + '/packages-premium/resource-daygrid/main.js'),
              loadScript(this, FullCalendarJS + '/packages-premium/resource-timegrid/main.js'),
              loadStyle(this, FullCalendarJS + '/packages-premium/resource-timeline/main.css'),
              loadScript(this, FullCalendarJS + '/packages-premium/resource-timeline/main.js')
            ]).then(() => {
              this.pluginLoaded = true;
            })
              .catch(error => {
                console.error(`${ERROR_COMPONENT_INIT}: ${error}`);
                this.dispatchEvent(
                  new ShowToastEvent({
                    title: ERROR_CALENDAR_INIT,
                    message: JSON.stringify(error),
                    variant: 'error'
                  })
                );
              })

          })
            .catch(error => {
              console.error(`${ERROR_COMPONENT_INIT} 2: ${error}`);
              this.dispatchEvent(
                new ShowToastEvent({
                  title: ERROR_CALENDAR_INIT,
                  message: JSON.stringify(error),
                  variant: 'error'
                })
              );
            })
          this.renderedCallback();
        })
        .catch(error => {
          console.error(`${ERROR_LIBRARY_LOAD}: ${error}`);
          this.dispatchEvent(
            new ShowToastEvent({
              title: ERROR_FULLCALENDAR_LOAD,
              message: JSON.stringify(error),
              variant: 'error'
            })
          );
        })
    }    
  }

  //MOSTRAR/OCULTAR SECCIONES DEL CALENDARIO

  showLegendMethod(){
    this.showLegend = !this.showLegend;    
  }

  showSaveButton(event) {
    this.hideSaveButton = event.detail;
  }

  showCreateEvent() {
    //if (this.selectedMainAgenda) {
      this.createEvent = true;
      this.hideSaveButton = true;
    /*} else {
      this.dispatchEvent(
        new ShowToastEvent({
          title: "Error al crear una cita",
          message: "Se debe seleccionar un doctor antes de crear una cita",
          variant: "error"
        })
      );
    }*/
  }

  hideCreateEvent() {
    try {
      this.createEvent = false;
      this.hideSaveButton = true;
      this.handleEvents(this.selectedDate);
    } catch (error) {
      console.error(error);
    }
  }

  dateChanged(event) {
    this.selectedDate = event.target.value;
    this.handleEvents(this.selectedDate);
  }  

  //METODOS PRINCIPALES

  initializeFullCalendarByAgenda() {
    const ele = this.template.querySelector("div.fillAgendas");

    let resourcesList = [];
    if((!this.selectedAgendas) || (this.selectedAgendas?.length === 0)){
      resourcesList = [{
        id: 1,
        title: "DOCTOR"
      }];
    } else{
      for (var i = 0; i < this.selectedAgendas.length && i < this.columnLimit; i++) {
        const resource = {
          id: i + 1,
          title: this.selectedAgendas === undefined ? " " : this.selectedAgendas[i].value,
          owner: this.selectedAgendas[i]?.owner,
          agendaId: this.selectedAgendas[i].key
        };
        // resourcesList = [...resourcesList, resource];
        resourcesList.push(resource);
      }
    }
    this.resourcesList = resourcesList;

    var rightHeader = '';
    if(!this.disableDay) rightHeader += "customDay,";
    if(!this.disableWeek) rightHeader += "customWeek,";
    if(!this.disableMonth) rightHeader += "customMonth,";

    if(this.calendarPluginType == 'Grid'){
      if(!this.calendarType && !this.disableDay) this.calendarType = "resourceTimeGridDay";
      else if(!this.calendarType && !this.disableDay) this.calendarType = "timeGridWeek";
      else if(!this.calendarType && !this.disableDay) this.calendarType = "dayGridMonth";
    }else{
      if(!this.calendarType && !this.disableDay) this.calendarType = "resourceTimelineDay";
      else if(!this.calendarType && !this.disableDay) this.calendarType = "resourceTimelineWeek";
      else if(!this.calendarType && !this.disableDay) this.calendarType = "resourceTimelineMonth";
    }

    if(rightHeader !== '') rightHeader = rightHeader.slice(0, -1);

    this.calendar3 = new FullCalendar.Calendar(ele, {
      selectable: true,
      selectConstraint: (this.calendarType.includes('resource') ? 'available' : null),
      eventConstraint: (this.calendarType.includes('resource') ? 'available' : null),
      defaultDate: this.selectedDate,
      nowIndicator: true,
      defaultView: this.calendarType,
      aspectRatio: 4,
      displayEventEnd: true,
      allDaySlot: false,
      slotEventOverlap: true,
      slotDuration: this.slotDuration,
      progressiveEventRendering: true,
      height: heightWindow * 0.62,
      minTime: this.minTime,
      maxTime: this.maxTime,
      expandRows: true,
      weekends: this.weekends,
      datesAboveResources: true,
      locale: "es",
      //TimeLine vars.
      dayMaxEvents: true, // allow "more" link when too many events
      schedulerLicenseKey: 'GPL-My-Project-Is-Open-Source',
      editable: true,
      dragScroll: true,
      resourceAreaWidth: (this.resourceAreaWidth ? this.resourceAreaWidth : '15') +'%',
      eventOverlap: (stillEvent, movingEvent) => {
        return stillEvent.rendering == 'background';
      },
      slotWidth: "28", //Controla que no se redimensionen las columnas
      snapDuration: this.slotDuration, //Minimo movimiento de tiempo al arrastrar un evento      
      resourceLabelText: this.resourceLabelText,
      plugins: this.calendarPluginType == 'Grid' ? ["dayGridPlugin","resourceDayGrid","resourceTimeGrid","interaction","list"] :['resourceTimeline', 'timeline', 'interaction', 'resourceTimeGrid', 'resourceDayGrid', 'bootstrap'],
      header: {
        left: "customPrev,customNext,customToday",
        center: "title",
        right: rightHeader
      },
      eventDrop: (info) => {
        this.infoData = info;
        // this.confirmScreen = true;
        const modalComp = this.template.querySelector('c-als_calendar-event-info.move');
        modalComp.show();
        // this.handleEventDrop(info);
      },
      eventResize: (info)  => {
        this.infoData = info;
        // this.confirmScreen = true;
        const modalComp = this.template.querySelector('c-als_calendar-event-info.move');
        modalComp.show();
        // this.handleEventResize(info);
      },
      eventClick: (info) => {
        if (info.event.rendering !== "background" && info.event.id) {
          const modalComp = this.template.querySelector('c-als_calendar-event-info.info');
          this.clickedEventId = info.event.id;
          modalComp.show();
        }
      },
      // dateClick: (info) => {
      //   var aux = (info.resource.extendedProps.idSf != undefined? info.resource.extendedProps.idSf: info.resource.extendedProps.agendaId);
      //   if(aux){
      //     const selectedRowData = {
      //       label: info.resource.title,
      //       value: aux
      //     };
      //     this.userSelectedRow = selectedRowData;
      //   }        
      //   this.selectedHour = info.date;
      //   this.showCreateEvent();
      // },
      select: (info) => {
        if(info.resource) {
          var aux = (info.resource.extendedProps.idSf != undefined ? info.resource.extendedProps.idSf: info.resource.extendedProps.agendaId);
          if(aux){
            const selectedRowData = {
              label: info.resource.title,
              value: aux
            };
            this.userSelectedRow = selectedRowData;
          }        
        }
        this.selectedHour = info.start;
        this.endDate = info.end;
        this.showCreateEvent();
      },
      customButtons: {
        customPrev: {
          text: "<",
          click: () => {
            var yesterday = new Date(this.selectedDate);
            if (this.calendarType == "resourceTimeGridDay" || this.calendarType == "resourceTimelineDay"){
              yesterday.setDate(yesterday.getDate() - 1);
              //Si no se muestran los fin de semana, la fecha se regula diferente.
              if(!this.weekends){
                if(yesterday.getDay() == 0) yesterday.setDate(yesterday.getDate() - 2);
                else if(yesterday.getDay() == 6) yesterday.setDate(yesterday.getDate() - 1);
              }
            }
            else if (this.calendarType == "timeGridWeek" || this.calendarType == "resourceTimelineWeek") yesterday.setDate(yesterday.getDate() - 7);
            else yesterday.setMonth(yesterday.getMonth() - 1);
            this.selectedDate = yesterday.getFullYear() + "-" + ("0" + (yesterday.getMonth() + 1)).slice(-2) + "-" + ("0" + yesterday.getDate()).slice(-2);
            this.agendaCalendar.calendar3.prev();
            this.showSpinner();
            this.handleEvents(this.selectedDate);
          }
        },
        customNext: {
          text: ">",
          click: () => {
            var tomorrow = new Date(this.selectedDate);
            if (this.calendarType == "resourceTimeGridDay" || this.calendarType == "resourceTimelineDay"){
              tomorrow.setDate(tomorrow.getDate() + 1);
              //Si no se muestran los fin de semana, la fecha se regula diferente.    
              if(!this.weekends){
                if(tomorrow.getDay() == 0) tomorrow.setDate(tomorrow.getDate() + 1);
                else if(tomorrow.getDay() == 6) tomorrow.setDate(tomorrow.getDate() + 2);
              }
            }
            else if (this.calendarType == "timeGridWeek" || this.calendarType == "resourceTimelineWeek") tomorrow.setDate(tomorrow.getDate() + 7);
            else tomorrow.setMonth(tomorrow.getMonth() + 1);
            this.selectedDate = tomorrow.getFullYear() + "-" + ("0" + (tomorrow.getMonth() + 1)).slice(-2) + "-" + ("0" + tomorrow.getDate()).slice(-2);
            this.agendaCalendar.calendar3.next();
            this.showSpinner();
            this.handleEvents(this.selectedDate);
          }
        },
        customToday: {
          text: "Hoy",
          click: () => {
            if (this.selectedDate !== date) {
              this.selectedDate = date;
              this.showSpinner();
              this.handleEvents(this.selectedDate);
            }
          }
        },
        customDay: {
          text: "Día",
          click: () => {
            if ((this.calendarType != "resourceTimeGridDay" && this.calendarPluginType == 'Grid') || (this.calendarType != "resourceTimelineDay" && this.calendarPluginType == 'Timeline')) {
              if(this.agendaList.length > 0) this.showOtherAgenda = true;
              if(this.calendarPluginType == 'Grid'){
                this.calendarType = "resourceTimeGridDay";
                this.agendaCalendar.calendar3.changeView("resourceTimeGridDay");
              } else if(this.calendarPluginType == 'Timeline'){
                this.calendarType = "resourceTimelineDay";
                this.agendaCalendar.calendar3.changeView("resourceTimelineDay");
              }             
              this.showSpinner();
              this.handleEvents(this.selectedDate);
            }
          }
        },
        customWeek: {
          text: "Semana",
          click: () => {
            if ((this.calendarType != "timeGridWeek" && this.calendarPluginType == 'Grid') || (this.calendarType != "resourceTimelineWeek" && this.calendarPluginType == 'Timeline')){ 
              try {
                this.selectedDate = this.startOfWeek(this.selectedDate);
              } catch (error) {
                console.error(error);
              }

              if(this.calendarPluginType == 'Grid'){
                this.showOtherAgenda = false;
                this.selectedAgendas = undefined;
                this.calendarType = "timeGridWeek";
                this.agendaCalendar.calendar3.changeView("timeGridWeek");
              } else if(this.calendarPluginType == 'Timeline'){
                this.calendarType = "resourceTimelineWeek";
                this.agendaCalendar.calendar3.changeView("resourceTimelineWeek");
              } 
              this.showSpinner();
              this.handleEvents(this.selectedDate);
            }
          }
        },
        customMonth: {
          text: "Mes",
          click: () => {
            if ((this.calendarType != "dayGridMonth" && this.calendarPluginType == 'Grid') || (this.calendarType != "resourceTimelineMonth" && this.calendarPluginType == 'Timeline')){ 
              try {
                this.selectedDate = this.startOfMonth(this.selectedDate);
              } catch (error) {
                console.error(error);
              }
              if(this.calendarPluginType == 'Grid'){
                this.showOtherAgenda = false;
                this.selectedAgendas = undefined;
                this.calendarType = "dayGridMonth";
                this.agendaCalendar.calendar3.changeView("dayGridMonth");
              } else if(this.calendarPluginType == 'Timeline'){
                this.calendarType = "resourceTimelineMonth";
                this.agendaCalendar.calendar3.changeView("resourceTimelineMonth");
              } 
              this.showSpinner();
              this.handleEvents(this.selectedDate);
            }
          }
        }
      },
      loading: (isLoading) => {
        if (isLoading) this.showSpinner();
        else this.hideSpinner();
      },
      resources: this.resourcesList,
      events: this.agendaEvents
    });
    this.agendaCalInit = true;
    this.agendaCalendar.calendar3 = this.calendar3;
    this.calendar3.render();
    if(!this.agendaCalInit && !this.agendaEvents?.length) {
      this.handleEvents(this.selectedDate);
    }
    this.hideSpinner();
  }

  handleEventDrop() {
    this.showSpinner();
    const info = this.infoData;
    const resourcesList = this.resourcesList;
    var ownerId = resourcesList[info.event._def.resourceIds[0] -1 ].owner;
    var agendaId = resourcesList[info.event._def.resourceIds[0] -1 ].agendaId;
    var startDate = info.event.start;
    var endDate = info.event.end;
    var startDateFormated = startDate.getFullYear() + "-" + ("0" + (startDate.getMonth() + 1)).slice(-2) + "-" + ("0" + startDate.getDate()).slice(-2)+ " " + startDate.getHours() + ":" + startDate.getMinutes() + ":00";
    var endDateFormated = endDate.getFullYear() + "-" + ("0" + (endDate.getMonth() + 1)).slice(-2) + "-" + ("0" + endDate.getDate()).slice(-2)+ " " + endDate.getHours() + ":" + endDate.getMinutes() + ":00";
    var oldStartDate = info.oldEvent?.start;
    var oldStartDateFormated = '';
    if(oldStartDate) {
      oldStartDateFormated = oldStartDate.getFullYear() + "-" + ("0" + (oldStartDate.getMonth() + 1)).slice(-2) + "-" + ("0" + oldStartDate.getDate()).slice(-2)+ " " + oldStartDate.getHours() + ":" + oldStartDate.getMinutes() + ":00";
    }

    if(this.isCloneActivated){
      // if (!confirm("Are you sure you want to clone this appointment?")) {
      //   info.revert();
      // }else{
        updateCita({recordId: info.event.id, startDate: startDateFormated, endDate: '', ownerId: ownerId, agendaId: agendaId, isClone: this.isCloneActivated })
          .then((result) => {
            self.dispatchEvent(
              new ShowToastEvent({
                title: "",
                message: SUCCESS_EVENT_CLONE,
                variant: "success"
              })
            );
            this.template.querySelector('c-als_calendar-event-info.move').hide();
            this.handleEvents(this.selectedDate);
          })
          .catch((error) => {
            console.error(error);
            self.dispatchEvent(
              new ShowToastEvent({
                title: "",
                message: ERROR_CONTACT_ADMIN,
                variant: "error"
              })
            );
            info.revert();
          })
          .finally(() => {
            this.hideSpinner();
          });
      // }
    }
    else{
      var isAssociatedEvent = info.event._def.extendedProps.isAssociatedEvent;
      if((isAssociatedEvent) && (startDateFormated != oldStartDateFormated)){
        self.dispatchEvent(
          new ShowToastEvent({
            title: "",
            message: ERROR_ASSOCIATED_EVENT_EDIT,
            variant: "error"
          })
        );
        info.revert();
      }else{
        // if (!confirm("Do you want to change event?")) {
        //   info.revert();
        // }else{
          updateCita({recordId: info.event.id, startDate: startDateFormated, endDate: endDateFormated, ownerId: ownerId, agendaId: agendaId, isClone: this.isCloneActivated })
          .then((result) => {
            self.dispatchEvent(
              new ShowToastEvent({
                title: "",
                message: SUCCESS_EVENT_EDIT,
                variant: "success"
              })
            );
            this.template.querySelector('c-als_calendar-event-info.move').hide();
            this.handleEvents(this.selectedDate);
          })
          .catch((error) => {
            console.error(error);
            self.dispatchEvent(
              new ShowToastEvent({
                title: "",
                message: ERROR_CONTACT_ADMIN,
                variant: "error"
              })
            );
            info.revert();
          })
          .finally(() => {
            this.hideSpinner();
          });
        // }
      }
    }
  }

  cancelReschedule() {
    this.infoData.revert();
    this.template.querySelector('c-als_calendar-event-info.move').hide();
  }

/*
  handleEventResize() {
    const info = this.infoData;
    const resourcesList = this.resourcesList;
    var isAssociatedEvent = info.event._def.extendedProps.isAssociatedEvent; 
    if(isAssociatedEvent){
      self.dispatchEvent(
        new ShowToastEvent({
          title: "Error",
          message: "You can't modify the duration of an associated event. Modify the main event.",
          variant: "error"
        })
      );
      info.revert();
    }else{
      // if (!confirm("Do you want to change event?")) {
      //   info.revert();
      // }else{
        var startDate = info.event.start;
        var endDate = info.event.end;
        var startDateFormated = startDate.getFullYear() + "-" + ("0" + (startDate.getMonth() + 1)).slice(-2) + "-" + ("0" + startDate.getDate()).slice(-2)+ " " + startDate.getHours() + ":" + startDate.getMinutes() + ":00";
        var endDateFormated = endDate.getFullYear() + "-" + ("0" + (endDate.getMonth() + 1)).slice(-2) + "-" + ("0" + endDate.getDate()).slice(-2)+ " " + endDate.getHours() + ":" + endDate.getMinutes() + ":00";
        var agendaId = resourcesList[info.event._def.resourceIds[0] -1 ].agendaId;

        updateCita({recordId: info.event.id, startDate: startDateFormated, endDate: endDateFormated, ownerId: null, agendaId: agendaId, isClone: false})
        .then((result) => {
          self.dispatchEvent(
            new ShowToastEvent({
              title: "Exito",
              message: "Successfully modified appointment",
              variant: "success"
            })
          );
          this.handleEvents(this.selectedDate);
        })
        .catch((error) => {
          console.error(error);
          self.dispatchEvent(
            new ShowToastEvent({
              title: "Error",
              message: "An error has occurred. Contact the administrator.",
              variant: "error"
            })
          );
          info.revert();
        });
      // }
    }
  }
*/

  handleEvents(selectedDate) {
    if (this.calendar3 && this.calendar3.view) this.calendarType = this.calendar3.view.type;
    var listAgendaId = [];
    if (this.selectedAgendas) {
      for (var i = 0; i < this.selectedAgendas.length; i++) {
        listAgendaId.push(this.selectedAgendas[i].key);
      }
    }

    console.log('this.calendarType >> ' + this.calendarType);
    console.log('this.calendar3.view >> ' + this.calendar3?.view);

    getAppointmentByDate({ agendaDate: selectedDate, agendasId: listAgendaId, calendarType: this.calendarType, lstSelectables: this.selectables /* NEW:INTXI */  })
      .then((result) => {
        
        console.log('Eventos recuperados >> ' + result);
        var evtList = [];
        result.forEach((item, index) => {
          var evtStyle = this.getEventStyle(item); 
          var resourceId = 1;
          if (this.selectedAgendas) {
            for (var i = 0; i < this.selectedAgendas.length; i++) {
              if (this.selectedAgendas[i].key === item.ALS_MainAgenda__c)
                resourceId = i + 1;
            }
          }

          var evt = {
            id: item.ALS_ParentId__c || item.Id,
            start: item.StartDateTime,
            end: item.EndDateTime,
            resourceId: resourceId,
            title: item.Subject,
            color: evtStyle.color,
            textColor: evtStyle.textColor,
            borderColor: "#000000",
            isAssociatedEvent: !!item.ALS_ParentId__c
          };

          evtList.push(evt);
        });

        //No recuperamos las horas de trabajo del calendario mensual
        // if (this.calendarPluginType == 'Grid' && this.calendarType !== "dayGridMonth") {
        if (this.calendarPluginType === 'Grid' && this.calendarType.includes('resource')) {
          var businessHourList = this.getBusinessHourEvents();
          if (businessHourList) {
            for (const evt of businessHourList) {
              evtList.push(evt);
            }
          }
        }

        this.agendaEvents = evtList;

        //Limpiamos el calendario antes de meter los nuevos datos
        if (this.agendaCalInit) {
          this.calendar3.destroy();
        }
        //Renderizamos el calendario
        this.initializeFullCalendarByAgenda();
      })
      .catch((error) => {
        console.error(error);
        this.dispatchEvent(
          new ShowToastEvent({
            title: ERROR_EVENTS_LOAD,
            message: error,
            variant: "error"
          })
        );
      })
      .finally(() => {
        this.hideSpinner();
      });
  }

  //METODOS AUXILIARES

  agendasChanged(event) {
    if(this.agendaCalInit) {
      try {
        console.log(JSON.stringify(event.detail, null, 2));
        // this.yourSelectedValues = [];
        // let self = this;

        // event.detail.forEach(function (eachItem) {
        //   const option = {
        //     value: eachItem.value,
        //     key: eachItem.key,
        //     businessHour: eachItem.businessHour
        //   };
        //   self.yourSelectedValues = [...self.yourSelectedValues, option];
        // });
        // this.yourSelectedValues = event.detail;
        this.selectedAgendas = event.detail; // this.yourSelectedValues;
        this.handleEvents(this.selectedDate);
      } catch (error) {
        console.error(error);
      }
    }
  }

  // NEW:INTXI
  multiPicklistChanged(event) {
    if(this.agendaCalInit) {
      try {
        const selectedItems = event.detail;
        const keys = selectedItems.map(item => item.key);
        const selectables = this.selectables;
        const picklist = selectables.find(item => item.label === event.target.label);
        picklist.selectedValues = keys;
        this.handleEvents(this.selectedDate);
      } catch (error) {
        console.error(error);
      }
    }


    // console.log(JSON.stringify(keys), null, 2);
    // if(this.agendaCalInit) {
    //   try {
    //     this.selectedAgendas = event.detail; // this.yourSelectedValues;
    //     this.handleEvents(this.selectedDate);
    //   } catch (error) {
    //     console.error(error);
    //   }
    // }
  }

  refreshCalendar() {
    this.showSpinner();
    this.handleEvents(this.selectedDate);
  }

  handleClickSave() {
    this.template.querySelector("div.modalSpinner").classList.remove("slds-hide");
    this.template.querySelector("c-als_create-event_-simple").saveEvent();
  }

  //Oculta el spinner cuando guarda
  createdEvent(event) {
    this.template.querySelector("div.modalSpinner").classList.add("slds-hide");
    if(event.detail) this.hideCreateEvent();
  }

  startOfWeek(date) {
    var aux = new Date(date);
    var diff = aux.getDate() - aux.getDay() + (aux.getDay() === 0 ? -6 : 1);
    var result = new Date(aux.setDate(diff));
    return (result.getFullYear() + "-" + ("0" + (result.getMonth() + 1)).slice(-2) + "-" + ("0" + result.getDate()).slice(-2));
  }

  startOfMonth(date) {
    var aux = new Date(date);
    var result = new Date(aux.getFullYear(), aux.getMonth(), 1);
    return (result.getFullYear() + "-" + ("0" + (result.getMonth() + 1)).slice(-2) + "-" + ("0" + result.getDate()).slice(-2));
  }

  getBusinessHourEvents() {
    //Fechas del calendario
    var calendarStartDate;
    var calendarEndDate;
    var evtList = [];

    if (this.calendarType == "resourceTimeGridDay") {
      calendarStartDate = new Date(this.selectedDate);
      calendarEndDate = new Date(calendarStartDate);
      calendarEndDate = new Date(calendarEndDate.setDate(calendarEndDate.getDate() + 1));
    } else if (this.calendarType == "timeGridWeek") {
      calendarStartDate = new Date(this.startOfWeek(this.selectedDate));
      calendarEndDate = new Date(calendarStartDate);
      calendarEndDate = new Date(calendarEndDate.setDate(calendarEndDate.getDate() + 7));
    }

    //Añadimos las horas de trabajo del resto de agendas (Solo para el calendario de día a día)
    if (this.selectedAgendas) {
      for (var i = 0; i < this.selectedAgendas.length; i++) {
        var agenda = this.selectedAgendas[i];
        if (agenda.businessHour && agenda.businessHour.length > 0) {
          for (const bh of agenda.businessHour) {
            var startDateBH = new Date(bh.startDate);
            var endDateBH = new Date(bh.endDate);
            if (bh.days && bh.days.includes(calendarStartDate.getDay()) && calendarStartDate >= startDateBH && calendarStartDate <= endDateBH) {
              var evt = {
                id: 'background',
                groupId: 'available',
                start: this.selectedDate + "T" + bh.startTime,
                end: this.selectedDate + "T" + bh.endTime,
                resourceId: i + 1,
                rendering: 'background',
                backgroundColor: DEFAULT_COLOR_BUSINESS_HOURS,
                overlap: true
              };
              evtList.push(evt);
            }
          }
        }
      }
    }
    return evtList;
  }

  getEventStyle(item){
    var evtColor = DEFAULT_COLOR_EVENT_BACKGROUND;
    var textColor = DEFAULT_COLOR_EVENT_TEXT;

    for(var legend of this.chartLegendList){
      //console.log('legend -------------- ' + legend.label);
      if(!legend.conditions || legend.conditions.size == 0){
        evtColor = legend.color;
        textColor = legend.textColor;
        break;
      }else {
        var isAssigned = true;
        for(var condition of legend.conditions){
          //console.log(item);
          //console.log(condition.FieldApiName);
          //console.log('condition.Value >> ' + condition.Value + ' -- item[condition.FieldApiName] >> ' + item[condition.FieldApiName]);
          if(item[condition.FieldApiName]){
            // if(condition.Operation == 'Equal' && condition.Value !== item[condition.FieldApiName]){
            //   isAssigned = false;
            //   break;
            // }else if(condition.Operation == 'Not Equal' && condition.Value === item[condition.FieldApiName]){
            //   isAssigned = false;
            //   break;
            // }else if(condition.Operation == 'Greater than' && item[condition.FieldApiName] <= condition.Value){
            //   isAssigned = false;
            //   break;
            // }else if(condition.Operation == 'Less than' && item[condition.FieldApiName] >= condition.Value ){
            //   isAssigned = false;
            //   break;
            // }else if(condition.Operation == 'Equal or greater than' && item[condition.FieldApiName] < condition.Value){
            //   isAssigned = false;
            //   break;
            // }else if(condition.Operation == 'Equal or less than' && item[condition.FieldApiName] > condition.Value){
            //   isAssigned = false;
            //   break;
            // }
            // NEW:INTXI
            const value = item[condition.FieldApiName];
            const reference = condition.Value;
            isAssigned = this.compareValues(value, reference, condition.Operation);
          }else{
            isAssigned = false;
            // break;
          }
          // Substitutes break; statements inside conditions
          if(!isAssigned) {
            break;
          }
          
        }
        if(isAssigned){
          evtColor = legend.color;
          textColor = legend.textColor;
          break;
        }
      }
    }

    return {color: evtColor, textColor: textColor};
  }

  compareValues(value, reference, operation) {
    let meets;
    switch (operation) {
      case COMPARATORS.EQ:
        meets = value === reference;
        break;
      case COMPARATORS.NE:
        meets = value !== reference;
        break;
      case COMPARATORS.GT:
        meets = value > reference;
        break;
      case COMPARATORS.LT:
        meets = value < reference;
        break;
      case COMPARATORS.GE:
        meets = value >= reference;
        break;
      case COMPARATORS.LE:
        meets = value <= reference;
        break;
      default:
        meets = false;
        break;
    }
    return meets;
  }

  toggleIsCloneActivated(){
    this.isCloneActivated = !this.isCloneActivated;
  }

  showSpinner() {
    this.template.querySelector("div.spinner").classList.remove("slds-hide");
  }
  hideSpinner() {
    this.template.querySelector("div.spinner").classList.add("slds-hide");
  }

}