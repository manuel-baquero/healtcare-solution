import { LightningElement, api, wire } from 'lwc';
import getProfilePhoto from '@salesforce/apex/ProfilePhotoUploader_Controller.getProfilePhotoURL';
import { ShowToastEvent } from "lightning/platformShowToastEvent";

export default class Ahc_appointmentsTimelineCard extends LightningElement {
    @api photoUrl = 'https://www.lightningdesignsystem.com/assets/images/avatar2.jpg';
    @api patient;
    @api location;
    @api status;
    @api doctor;
    @api pendingTime;
    @api paid;
    @api payment;
    @api appointmentType;
    @api recordId;
    @api appointmentDateTime;
    @api link;
    @api doctorLink;
    @api patientLink;
    @api subject;
    @api isLead;
    @api showModal = false;

    get screenDate (){
        const dt = new Date(this.appointmentDateTime);
        return ("0" + dt.getDate()).slice(-2) + '/' + ("0" + (dt.getMonth() + 1)).slice(-2) + '/' + dt.getFullYear() + '  ' + ("0" + dt.getHours()).slice(-2) + ':' + ("0" + dt.getMinutes()).slice(-2);
    }

    get paymentDate (){
        const dt = new Date(this.appointmentDateTime);
        return dt.getFullYear() + '-' + ("0" + (dt.getMonth() + 1)).slice(-2) + '-' + ("0" + dt.getDate()).slice(-2);
    }

    @wire(getProfilePhoto, { recordId: '$recordId' })
    getTimelinePhoto({ error, data }) {
        if(data){
            this.photoUrl = data;
        }else if (error) {
            this.photoUrl = 'https://www.lightningdesignsystem.com/assets/images/avatar2.jpg';
            console.error(error);
        } 
    }

    openModal(){
        this.showModal = true;
    }

    closeModal(){
        this.showModal = false;
    }

    handleSuccess(){
        this.dispatchEvent(
            new ShowToastEvent({
              title: "",
              message: "Pago registrado correctamente",
              variant: "success"
            })
          );
        this.paid = true;
        this.showModal = false;
    }
}