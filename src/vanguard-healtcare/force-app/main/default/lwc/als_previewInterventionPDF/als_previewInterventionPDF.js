import { LightningElement, api, wire } from 'lwc';
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import { getRecord, getFieldValue } from 'lightning/uiRecordApi';
import { CloseActionScreenEvent } from 'lightning/actions';
import sendQuotationWithAttachedPDF from '@salesforce/apex/PreviewPresupuestoPDF_Ctrl.sendQuotationWithAttachedPDF';

const RT_DEVNAME_FIELD = 'Intervention__c.RecordType.DeveloperName';
const PATIENT_FIELD = 'Intervention__c.Patient__c';
const PATIENT_NAME_FIELD = 'Intervention__c.Patient__r.Name';
const PATIENT_PCID_FIELD = 'Intervention__c.Patient__r.PersonContactId';
const RECORD_FIELDS = [
    RT_DEVNAME_FIELD,
    PATIENT_FIELD,
    PATIENT_NAME_FIELD,
    PATIENT_PCID_FIELD
];
// const DEFAULT_VF = 'MedicalReportPDF';

export default class als_previewInterventionPDF extends LightningElement {

    @api recordId;
    _record = {};
    get _vfName() {
        return 'InterventionPDF';
    }
    _isProcessing = false;
    _isValidated;

    get wasValidated() {
        return this._isValidated != null;
    }
    get vfAddress() {
        if(this._vfName) {
            return '/apex/' + this._vfName + '?Id=' + this.recordId;
        }
        return null;
    }

    @wire(getRecord, {recordId: '$recordId', fields: RECORD_FIELDS})
    handleGetRecord({error, data}) {
        if(error) {
            this.showToast('', 'Ha habido un error al traer los campos del registro', 'error');
        } else if(data) {
            this._record = data;
            this.handleRecordUpdated();
        }
    }
    
    handleRecordUpdated() {
        const record = this._record;
        if (getFieldValue(record, PATIENT_FIELD)) {
            this._isValidated = true;
        } else {
            this._isValidated = false;
        }
    }

    saveQuotation() {
        this.processQuotation(false);
    }

    sendQuotation() {
        this.processQuotation(true);
    }

    getPersonContactId() {
        return getFieldValue(this._record, PATIENT_PCID_FIELD);
    }

    processQuotation(getContactId) {
        this._isProcessing = true;
        const recordId = this.recordId;
        const toWhoId = getContactId ? this.getPersonContactId() : null;

        sendQuotationWithAttachedPDF({recordId, toWhoId})
            .then(result => {
                this.showToast('', `Archivo guardado${toWhoId ? ' y enviado ' : ' '}con éxito`, 'success');
                this.refreshTab();
                this.closeQuickAction();
            })
            .catch(error => {
                console.error(JSON.stringify(error, null, 2));
                this.showToast('', `No se ha podido guardar${toWhoId ? ' o enviar ' : ' '}el archivo`, 'error');
            })
            .finally(() => {
                this._isProcessing = false;
            });
    }
    showToast(title, message, variant) {
        this.dispatchEvent(
            new ShowToastEvent({
                title,
                message,
                variant
            })
        );
    }

    closeQuickAction() {
        this.dispatchEvent(new CloseActionScreenEvent());
    }

    refreshTab() {
        eval("$A.get('e.force:refreshView').fire();");
    }

}