import { LightningElement, api, track } from 'lwc';

export default class Ahc_paginatorFooter extends LightningElement {

    @api disablePrev;
    @api disableNext;
    @api disableLast;
    @api disableFirst;
    @api pageSize = 5;
    @api totalPages;
    @api page;
    @api recordsSize;

    previousHandler1() {
        this.dispatchEvent(new CustomEvent('previous'));
    }

    nextHandler1() {
        this.dispatchEvent(new CustomEvent('next'));
    }

    FirstPageHandler1(){
        this.dispatchEvent(new CustomEvent('firstpage'));
    }

    LastPageHandler1(){
        this.dispatchEvent(new CustomEvent('lastpage'));
    }

    changeHandler(event){
        event.preventDefault();
        const s_value = event.target.value;
        this.pageSize = s_value;
        const selectedEvent = new CustomEvent('selected', { detail: s_value});
        this.dispatchEvent(selectedEvent);
 
    }
}