import { LightningElement, api } from 'lwc';
import {FlowAttributeChangeEvent} from 'lightning/flowSupport';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class SurveyRichText extends LightningElement {

    @api richTextValue;
    @api label;

    handleClick (event) {
        this.richTextValue = this.template.querySelector('.myInput').value ;
        this.dispatchEvent(new FlowAttributeChangeEvent('richTextValue',this.richTextValue));
        this.showToast('success','Rich text succesfully saved','')
    }

    showToast(variant, title, message) {
        this.dispatchEvent(
            new ShowToastEvent({ variant, title, message })
        );
    }
}