import { LightningElement, api } from 'lwc';

export default class Als_calendarManagement extends LightningElement {
    @api calendar;
    @api variant;
    calendarTab = []; // Configuration for tabs.
    initialize = false;
    activateTabSection = false;

    renderedCallback(){
        if(!this.initialize){        
            if(this.calendar.includes(',')){
                this.activateTabSection = true;
                var cont = 0;
                for(var calendarConfig of this.calendar.split(';')){
                    var aux = calendarConfig.split(',');
                    var configuration = {
                        label: aux[0],
                        iconName : aux[1],
                        id: aux[2].replaceAll(' ',''),
                        key: cont
                    };        
                    this.calendarTab.push(configuration);
                    cont++;
                }
            }   
            this.initialize = true;
        }
    }
}