import { LightningElement, api, track } from 'lwc';

const DEFAULT_COLOR = 'goldenrod';
export default class StarRating extends LightningElement {

    @api color;
    @api required = false;
    @api label = '';
    @api value = null;
    @api messageWhenValueMissing = '';
    @api scoreArray = [];

    @track _starArray;  // copy of incoming scoreArray

    _isValid = true;

    get _starStyle() {
        return `color: ${this.color || DEFAULT_COLOR};`;
    }

    connectedCallback() {
        // Needs to be deep-cloned so it can be modified
        this._starArray = JSON.parse(JSON.stringify(this.scoreArray));
        this.fillStars();
    }

    handleStarClicked(event) {
        const newValue = event.target.getAttribute('data-value');
        this.value = newValue;
        this.fillStars();
    }

    // ★☆
    fillStars() {
        const choices = this._starArray;
        const currentValue = this.value;
        for(let ch of choices) {
            ch.label = (ch.value <= currentValue ? '★' : '☆');
        }
    }

    @api reportValidity() {
        this._isValid = !this.required || (
            this.required && this.value != null && isFinite(this.value)
        );
        return this._isValid;
    }
}