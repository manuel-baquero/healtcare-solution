import { LightningElement, api } from 'lwc';

export default class LwcFlowProgressIndicator extends LightningElement {
    @api currentStep;
    @api steps;
    @api flowSteps;

    handleStepFocus(event) {
        const stepIndex = event.detail.index;
        this.currentStep = this.flowSteps[stepIndex].value; 
    }

    get flowSteps(){
        var stepsList = this.steps.split(',');
        var stepsAssinged = [];
        stepsList.forEach((step,index) => {
            var objStep = {
                label:step, value:'step-'+index
            }
            stepsAssinged.push(objStep);
        });

        return stepsAssinged;
    }
}