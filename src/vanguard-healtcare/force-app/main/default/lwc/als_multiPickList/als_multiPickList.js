import { LightningElement, api, track } from "lwc";
export default class als_multiPickList extends LightningElement {
  @api label = ""; //Name of the dropDown
  @api maxselected = 1; //Max selected item display
  @api limitselection; //Max selected item number
  _options;
  @api
  get options() {
    return this._options;
  }
  set options(value) {
    try {
      this._options = value;
      this.initArray(this);
      this._selectedItems = "Select a value";
    } catch (error) {
      console.error(error);
    }    
  }
  @api showfilterinput = false; //show filterbutton
  @api showrefreshbutton = false; //show the refresh button
  @api showclearbutton = false; //show the clear button
  @api comboplaceholder = "Select a value";
  @api selectedvalues = [];

  @track _initializationCompleted = false;
  @track _selectedItems = "Select a value";
  @track _filterValue;
  @track _mOptions;
  initialized = false;

  constructor() {
    try {
      super();
      this._filterValue = "";
    } catch (error) {
      console.error(error);
    }    
  }

  renderedCallback() {
    try {
      let self = this;
      if (!this._initializationCompleted) {
        this.template.querySelector(".ms-input").addEventListener("click", function (event) {
            //console.log ('multipicklist clicked');
            self.onDropDownClick(event.target);
            if (this._mOptions === undefined) self.updateListItems("");
            event.stopPropagation();
          });
        this.template.addEventListener("click", function (event) {
          //console.log ('multipicklist-1 clicked');
          event.stopPropagation();
        });
        document.addEventListener("click", function (event) {
          //console.log ('document clicked');
          self.closeAllDropDown();
        });
        this._initializationCompleted = true;
        this.setPickListName();
      }
    } catch (error) {
      console.error(error);
    }    
  }

  handleItemSelected(event) {
    try {
      var cont = 0;
      this._mOptions.forEach(function (eachItem) {
        if(eachItem.selected) cont ++;
      });

      if (!this.limitselection || cont < this.limitselection || !event.detail.selected) {
        let self = this;
        this._mOptions.forEach(function (eachItem) {
          if (eachItem.key == event.detail.item.key) {
            eachItem.selected = event.detail.selected;
            return;
          }
        });
        this.setPickListName();
        this.onItemSelected();
      }
    } catch (error) {
      console.error(error);
    }
  }

  filterDropDownValues(event) {
    this._filterValue = event.target.value;
    console.log('Filter: ' + this._filterValue);
    this.updateListItems(this._filterValue);
  }

  closeAllDropDown() {
    Array.from(this.template.querySelectorAll(".ms-picklist-dropdown")).forEach(
      function (node) {
        node.classList.remove("slds-is-open");
      }
    );
  }

  onDropDownClick(dropDownDiv) {
    Array.from(this.template.querySelectorAll(".ms-picklist-dropdown"))
    .forEach(node => {
      node.classList.toggle("slds-is-open");
    });
  }

  onRefreshClick(event) {
    this._filterValue = "";
    this.initArray(this);
    this.updateListItems("");
    this.onItemSelected();
  }

  onClearClick(event) {
    this._filterValue = "";
    this._mOptions.forEach(function (eachItem) {
      if (eachItem.selected) {
        eachItem.selected = false;
      }
    });
    this.updateListItems("");
    this.setPickListName();
    this.onItemSelected();
  }

  connectedCallback() {
    try {
      this.initArray(this); 
    } catch (error) {
      console.error(error);
    }
  }

  initArray(context) {
    try {
      if(!this.initialized || ((context._mOptions) && (context._mOptions.length === 0)) || ((context._mOptions) && (context._mOptions.length !== context.options.length))){
        context._mOptions = new Array();
        if (context.options) {
          context.options.forEach(function (eachItem) {
            context._mOptions.push(JSON.parse(JSON.stringify(eachItem)));
  
            context._mOptions.forEach(function (eachItem) {
              if (context.selectedvalues) {
                context.selectedvalues.forEach(function (aux) {
                  if (aux.key === eachItem.key) {
                    eachItem.selected = true;
                  }
                });
              }
            });
            context.setPickListName();
            context.onItemSelected();
          });
        }
        this.initialized = true;
      }
    } catch (error) {
      console.error(error);
    }    
  }

  updateListItems(inputText) {
    try {
      Array.from(this.template.querySelectorAll("c-als_pick-list-item")).forEach(
        function (node) {
          if (!inputText) {
            node.style.display = "block";
          } else if (node.item.value.toString().toLowerCase().indexOf(inputText.toString().trim().toLowerCase()) != -1) {
            node.style.display = "block";
          } else {
            node.style.display = "none";
          }
        }
      );
      this.setPickListName();
    } catch (error) {
      console.error(error);
    }    
  }

  setPickListName() {
    try {
      let selecedItems = this.getSelectedItems();    
      let selections = "";
      if (selecedItems.length < 1) {
        selections = this.comboplaceholder;
      } else if (selecedItems.length > this.maxselected) {
        selections = selecedItems.length + " Options Selected";
      } else {
        selecedItems.forEach((option) => {
          selections += option.value + ",";
        });
        selections = selections.substring(0, selections.length - 1);
      }
      this._selectedItems = selections;
    } catch (error) {
      console.error(error);
    }    
  }

  @api
  getSelectedItems() {
    try{
      let resArray = new Array();
      this._mOptions.forEach(function (eachItem) {
        if (eachItem.selected) {
          resArray.push(eachItem);
        }
      });
      return resArray;
    }catch(error){
      console.error(error);
    }
    
  }

  @api resetOptions(){
    this.initialized = false;
  }

  onItemSelected() {
    try {
      const evt = new CustomEvent("itemselected", {
        detail: this.getSelectedItems()
      });
      this.dispatchEvent(evt);
    } catch (error) {
      console.error(error);
    }    
  }
}