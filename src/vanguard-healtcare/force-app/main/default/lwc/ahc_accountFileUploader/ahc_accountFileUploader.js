import { LightningElement, api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent'

export default class Ahc_accountFileUploader extends LightningElement {
    @api
    recordId;

    get acceptedFormats() {
        return ['.pdf', '.png', '.doc', '.docx', '.jpeg', '.jpg', '.xlsx'];
    }

    handleUploadFinished(event) {
        this.showToast('Ficheros cargados correctamente','Ya puede visualizarlos desde la ficha de paciente','success');
        const selectedEvent = new CustomEvent("successupload", { });
        this.dispatchEvent(selectedEvent);
    }

    showToast(title,message,type) {
        const event = new ShowToastEvent({
            title: title,
            message: message,
            variant: type
        });
        this.dispatchEvent(event);
    }
    
}