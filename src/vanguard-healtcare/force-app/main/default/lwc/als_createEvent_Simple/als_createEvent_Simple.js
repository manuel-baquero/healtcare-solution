import { LightningElement, api} from "lwc";
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import createEvent from "@salesforce/apex/ALS_calendarManagement_ctrl.createEvent";
import getEventRecordType from "@salesforce/apex/ALS_calendarManagement_ctrl.getEventRecordType";

export default class Als_createEvent_Simple extends LightningElement {
  @api startDate;
  @api endDate;
  @api mainAgenda;
  @api fields = [];
  @api recordId;
  @api leadRecord;
  __fields = []; //Necesario ya que el array de fields lo obtenemos de un @wire que no deja editar los valores.
  subject;
  description;
  relatedRecord;
  isRecordTypeSelected = false;
  recordTypeSelected;
  recordTypeSelectedLabel;
  startHour;
  endHour;
  subject;
  listRT;
  doInit = false;
  noSlots = false;
  rtMap;

  get recordTypeOptions() {   
    if(!this.doInit){
      this.listRT = [];  
      getEventRecordType({})
        .then((result) => {   
          this.doInit = true;  
          console.log(result); 
          this.rtMap = new Map ();
          result.forEach((item, index) => {
            const rt = {
              label: item.Name,
              value: item.Id            
            };
            this.listRT = [...this.listRT, rt];
            this.rtMap.set(rt.value,rt.label);
          });     
          console.log(this.rtMap);   
          if(this.listRT.length === 0) this.isRecordTypeSelected = true;
          else if(this.listRT.length === 1){
            this.recordTypeSelected = this.listRT[0];
            this.isRecordTypeSelected = true;
          } else {
            this.recordTypeSelected = this.listRT[0];
            this.isRecordTypeSelected = false;
          } 
        })
        .catch((error) => {
          console.error(error);
          this.dispatchEvent( new ShowToastEvent({
              title: "Error al cargar los tipos de evento",
              message: error,
              variant: "error"
          }));
        });
    }
    return this.listRT;
  }

  //Solo se ejecuta una vez
  renderedCallback(){
    if(!this.doInit){
      //Clonamos el listado de campos para poder modificarlo con el valor que marque el usuario
      for(var field of this.fields){
        this.__fields.push({...field});  
      }
      var auxDate = new Date(this.startDate);
      this.startHour = (auxDate.getHours() < 10 ? "0" : "") + (auxDate.getHours() < 8 ? "8" : auxDate.getHours()) + ":" + (auxDate.getMinutes() < 10 ? "0" : "") + auxDate.getMinutes();
      this.startDate = auxDate.getFullYear() + "-" + ("0" + (auxDate.getMonth() + 1)).slice(-2) + "-" + ("0" + auxDate.getDate()).slice(-2);
      if(this.endDate) {
        let endDate = new Date(this.endDate);
        this.endHour   = (endDate.getHours() < 10 ? "0" : "") + (endDate.getHours() < 8 ? "8" : endDate.getHours()) + ":" + (endDate.getMinutes() < 10 ? "0" : "") + endDate.getMinutes();
        this.endDate   = endDate.getFullYear() + "-" + ("0" + (endDate.getMonth() + 1)).slice(-2) + "-" + ("0" + endDate.getDate()).slice(-2);
      }
      this.doInit = true;
    } 
  }

  //For dynamic fields
  handleFieldChange = (event) => {
    if(this.doInit){
      try {
        for(var field of this.__fields){
          //El Split es necesario ya que al ser dinámico, salesforce le añade un número aleatorio al id.
          if((event.target) && (event.target.id) && (event.target.id.includes('-')) && (field.apiName === event.target.id.split('-')[0])){
              if((event.detail) && (field.showLookUp)) field.value = event.detail.id;
              else if((event.target) && (event.target.value) && (event.target.value != '')) field.value = event.target.value;
              else if((event.detail) && (field.showMultiPickList)){
                field.value = '';
                event.detail.forEach(function (eachItem) {
                  field.value += eachItem.key + ';';
                });
                if(field.value.length > 1) field.value.slice(0, -1);
              }
          }
          this.saveButtonController();
        }    
      } catch (error) {
        console.error(error);
      }      
    }    
  };

  handleDateChange(event) {
    this.startDate = event.target.value;
    this.saveButtonController();
  }

  handleDescriptionChange(event) {
    this.description = event.target.value;
  }

  handleRecordTypeChange(event) {
    this.recordTypeSelected = event.target.value;
    this.recordTypeSelectedLabel = 'Tipo de cita: ' + this.rtMap.get(event.target.value);
    this.isRecordTypeSelected = true;
  }

  handleStartHourChange(event) {
    this.startHour = event.target.value;
    this.saveButtonController();
  }

  handleSubjectChange(event) {
    this.subject = event.target.value;
    this.saveButtonController();
  }

  handleEndHourChange(event) {
    this.endHour = event.target.value;
    this.saveButtonController();
  }

  handleSubjectChange(event){
    this.subject = event.target.value;
  }

  handleAgendaChange(event) {
    this.mainAgenda = event.detail;
    this.saveButtonController();
  }

  handleAccountChange(event) {
    this.recordId = event.detail;
    this.saveButtonController();
  }

  handleRelatedRecord(event) {
    this.relatedRecord = event.detail;
    this.saveButtonController();
  }

  saveButtonController() {
    var aux = true; //true: hide, false: show
    if ((this.startDate && this.startHour && this.endHour && this.mainAgenda) || (this.startDate && this.startHour && this.endHour && this.recordId)) {
      //Comprobamos que los campos dynamicos obligatorios estén informados.
      var fieldRequiredError = false;      
      for(var field of this.__fields){
        if((field.required) && ((!field.value) || (field.value == ''))){
          //console.log('field.apiName >> ', field.apiName, 'field.required >> ', field.required, '   ------  field.value >> ' + field.value);
          fieldRequiredError = true;
        }
      }
      aux = fieldRequiredError;
    }
    const selectedEvent = new CustomEvent("completemodal", { detail: aux });
    this.dispatchEvent(selectedEvent);
  }

  @api
  saveEvent() {
    try {
      console.log("saveEvent");

      //Calculate duration of the event
      var dt1 = new Date(1995, 11, 17, this.startHour.split(":")[0], this.startHour.split(":")[1]);
      var dt2 = new Date(1995, 11, 17, this.endHour.split(":")[0], this.endHour.split(":")[1]);
      var diff =(dt2.getTime() - dt1.getTime()) / 1000;
      diff /= 60;
      var duration =  Math.abs(Math.round(diff));
      let eventFields = {};

      if(this.recordId){
        eventFields = {
          subject: this.subject,
          description: this.description,
          recordType: this.recordTypeSelected,
          recordId: this.recordId.id,
          startDate: this.startDate,
          startHour: this.startHour,
          leadRecord: this.leadRecord,
          duration: duration,
          fields: '{"data":' + JSON.stringify(this.__fields) + '}'
        }
      }else if (this.mainAgenda){
        eventFields = {
          subject: this.subject,
          description: this.description,
          recordType: this.recordTypeSelected,
          mainAgenda: this.mainAgenda.id,
          startDate: this.startDate,
          startHour: this.startHour,
          duration: duration,
          fields: '{"data":' + JSON.stringify(this.__fields) + '}'
        }
      }

      console.log(JSON.stringify(eventFields));
      createEvent(eventFields)
        .then((result) => {
          const selectedEvent = new CustomEvent("successfullycreated", { detail: result});
          this.dispatchEvent(selectedEvent);
          this.dispatchEvent(
            new ShowToastEvent({
              title: "Exito",
              message: "La cita se ha agendado con éxito en el calendario del centro",
              variant: "success"
            })
          );
        })
        .catch((error) => {
          console.error(error);
          this.dispatchEvent(
            new ShowToastEvent({
              title: "Error al crear el evento",
              message: error,
              variant: "error"
            })
          );
        });
    } catch (error) {
      console.error(error);
      this.dispatchEvent(
        new ShowToastEvent({
          title: "Error al crear el evento",
          message: error,
          variant: "error"
        })
      );
    }
  }
}