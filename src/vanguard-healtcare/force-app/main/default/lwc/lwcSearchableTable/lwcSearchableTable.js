import { LightningElement, wire, api, track} from 'lwc';
import { refreshApex } from '@salesforce/apex';
import {FlowAttributeChangeEvent} from 'lightning/flowSupport';
import _fetchLookUpValues from '@salesforce/apex/ReusableLookup_Controller.fetchLookUpValues';

// const columns = [{
//         label: 'Name',
//         fieldName: 'Name',
//         type: 'text',
//         sortable: true
//     },
//     {
//         label: 'Phone',
//         fieldName: 'Phone',
//         sortable: true
//     },
//     {
//         label: 'Email',
//         fieldName: 'PersonEmail',
//         sortable: false
//     },
//     {
//         label: 'Birth Date',
//         fieldName: 'PersonBirthdate',
//         sortable: true
//     }
// ];
export default class LwcSearchableTable extends LightningElement {
    
    
    @api searchKey = '';
    @api title;
    result;

    @api objectName;
    @api fieldName;
    @api placeHolder;
    @api iconName;
    @api whereClause = '';
    @api fields;
    @api keyField = 'Id';
    
    @api page = 1; 
    items = []; 
    @track data;
    columns;
    error;
    @api startingRecord = 1;
    @api endingRecord = 0; 
    @api pageSize = 5; 
    @api totalRecountCount = 0;
    @api totalPage = 1;
    @api nextAvailable = false;
    @api displayRecordsLabel;
    @api
    get selectedDataRows() {
        return this.addNew(this.fixedRows, this.temporaryRows);
    }
    fixedRows = [];
    temporaryRows = [];
    
    get nextUnavailable() {
        return !this.nextAvailable;
    }

    //clicking on previous button this method will be called
    previousHandler() {
        this.persistRows();
        if (this.page > 1) {
            this.page = this.page - 1; //decrease page by 1
            this.displayRecordPerPage(this.page);
        }
    }

    //clicking on next button this method will be called
    nextHandler() {
        this.persistRows();
        if((this.page<this.totalPage) && this.page !== this.totalPage){
            this.page = this.page + 1; //increase page by 1
            this.displayRecordPerPage(this.page);            
        }             
    }

    //this method displays records page by page
    displayRecordPerPage(page){

        this.startingRecord = ((page -1) * this.pageSize) ;
        this.endingRecord = (this.pageSize * page);

        this.endingRecord = (this.endingRecord > this.totalRecountCount) 
                            ? this.totalRecountCount : this.endingRecord; 

        this.data = [...this.data, this.items.slice(this.startingRecord, this.endingRecord)];

        this.startingRecord = this.startingRecord + 1;
    }

    handleKeyUp(event) {
        this.persistRows();
        this.searchKey = event.target.value;

            if (this.searchKey.length >= 3) {
                // setTimeout(() => {
                    return refreshApex(this.refreshLWCTable);
                // }, 2000);
            } else{
                this.data = [];
                this.totalRecountCount = 0;
            }
    }

    refreshLWCTable;
    @wire(_fetchLookUpValues, {searchKey: '$searchKey', fieldName: '$fieldName', fields: '$fields', objectName: '$objectName', keyField: '$keyField', whereClause: '$whereClause' })
    fetchLookUpValues(response) {
        this.refreshLWCTable = response;
        let { data, error } = response;
        if (data) {
            this.items = data;
            this.totalRecountCount = data.length; 
            this.totalPage = Math.ceil(this.totalRecountCount / this.pageSize); 
            if(this.totalPage > 1){
                this.nextAvailable = true;
            }
            this.data = this.items.slice(0,this.pageSize); 
            this.endingRecord = this.pageSize;
            if(!this.columns) {
                this.setColumns();
            }
            this.error = undefined;
        }else {
            this.error = error;
            this.data = undefined;
        }
    }

    setColumns() {
        let fields = this.fields?.replaceAll(' ', '').split(',');
        fields = [this.fieldName, ...fields];
        this.columns = fields?.map(elem => {
            return {
                label: elem?.replace('__c', '').replaceAll('_', ' '),
                fieldName: elem,
                sortable: false
            }
        });
    }

    persistRows() {
        this.addNew(this.fixedRows, this.temporaryRows);
    }

    addNew(oldArray, newArray) {
        for(let row of newArray) {
            if(!oldArray.find(elem => elem.Id === row.Id)) {
                oldArray.push(row);
            }
        }
        return oldArray;
    }

    selectedRowsHandler (event){
        const rowData = event.detail.selectedRows;
        this.temporaryRows = rowData;
        this.dispatchEvent(new FlowAttributeChangeEvent('selectedDataRows',this.selectedDataRows));
    }
}