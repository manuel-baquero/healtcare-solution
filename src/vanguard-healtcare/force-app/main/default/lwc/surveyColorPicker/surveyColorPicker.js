import { LightningElement, api, track } from 'lwc';
import {FlowAttributeChangeEvent} from 'lightning/flowSupport';

export default class SurveyColorPicker extends LightningElement {
    @api currentColor;
    @api selectedColor;
    @api selectorLabel;

    changeColor(event) {

        const color = event.detail.value;
        this.selectedColor = color;
        this.dispatchEvent(new FlowAttributeChangeEvent('selectedColor',this.selectedColor));
    }
}