({
    doInit: function(component, event, helper){
        var myPageRef = component.get("v.pageReference");
        var parentRecordId = myPageRef.state.c__parentRecordId;
        component.set("v.parentId", parentRecordId);
    },

    saveRecord : function(component, event, helper) {
        var parentId = component.get("v.parentId");
        var label = component.find("propLabel").get("v.value");
        var color = component.find("propColor").get("v.value");
        var action = component.get('c.createCalendarLegend'); 

        if(label == '' || color == ''){
            var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "type":"error",
                    "title": "Error",
                    "message": "All fields are required."
                });
                toastEvent.fire();
        }else{
            action.setParams({
                "label" : label,
                "color" : color,
                "calendar" : parentId,
            });
            action.setCallback(this, function(a){
                var state = a.getState();
                if(state == 'SUCCESS') {
                    var recordEvt = $A.get("e.force:navigateToSObject");
                    recordEvt.setParams({
                        "recordId": a.getReturnValue(),
                        "slideDevName": "related"
                    });
                    recordEvt.fire();
                }
            });
            $A.enqueueAction(action);
        }        
    },

    saveAndNewRecord : function(component, event, helper) {
        var parentId = component.get("v.parentId");
        var label = component.find("propLabel").get("v.value");
        var color = component.find("propColor").get("v.value");
        var action = component.get('c.createCalendarLegend'); 

        if(label == '' || color == ''){
            var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "type":"error",
                    "title": "Error",
                    "message": "All fields are required."
                });
                toastEvent.fire();
        }else{
            action.setParams({
                "label" : component.find("propLabel").get("v.value"),
                "color" : component.find("propColor").get("v.value"),
                "calendar" : parentId,
            });
            action.setCallback(this, function(a){
                var state = a.getState();
                if(state == 'SUCCESS') {
                    component.find("propLabel").set("v.value", "");
                    component.find("propColor").set("v.value", "");

                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "type":"success",
                        "title": "Success",
                        "message": "The record has been create successfully."
                    });
                    toastEvent.fire();
                }
            });
            $A.enqueueAction(action);
        }
    },

    cancelDialog : function(component, helper) {
        var parentId = component.get("v.parentId");
        var recordEvt = $A.get("e.force:navigateToSObject");
        recordEvt.setParams({
            "recordId": parentId,
            "slideDevName": "related"
        });
        recordEvt.fire();
    }
})